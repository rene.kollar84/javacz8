package cz.sda.java8;

import cz.sda.java8.rle.EncoderDecoder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestEncoderDecoder {
    EncoderDecoder encoder1 = new EncoderDecoder();

    @Test
    public void testEncodeInput() {
        String input = "askdfjsd";
        String encode = encoder1.encode(input);
        Assertions.assertEquals("a1s1k1d1f1j1s1d1", encode);

    }

    @Test
    public void testDecodeInput() {
        String input = "a1s1k1d1f1j1s1d1";
        String encode = encoder1.decode(input);
        Assertions.assertEquals("askdfjsd", encode);

    }

    @Test
    public void testDecodeEmptyInput() {
        String input = "";
        String encode = encoder1.decode(input);
        Assertions.assertEquals("", encode);

    }

    @Test
    public void testEmptyInput() {
        String input = "";
        String encode = encoder1.encode(input);
        Assertions.assertEquals("", encode);

    }

    @Test
    public void testEncodeInputWithNumber() {
        String input = "askdfjsd5";
        String encode = encoder1.encode(input);
        Assertions.assertEquals("a1s1k1d1f1j1s1d1#51", encode);

    }

    @Test
    public void testDecodeInputWithNumber() {
        String input = "a1s1k1d1f1j1s1d1#51";
        String encode = encoder1.decode(input);
        Assertions.assertEquals("askdfjsd5", encode);

    }
    @Test
    public void testEncodeInputWithEscape() {
        String input = "askdfjsd#";
        String encode = encoder1.encode(input);
        Assertions.assertEquals("a1s1k1d1f1j1s1d1##1", encode);
    }


}
