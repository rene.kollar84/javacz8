package cz.sda.java8;

import cz.sda.java8.advancedf.exercises.task3.Circle;
import cz.sda.java8.advancedf.exercises.task6.Movable;
import cz.sda.java8.advancedf.exercises.task6.MovableCircle;
import cz.sda.java8.advancedf.exercises.task6.MovablePoint;
import cz.sda.java8.advancedf.exercises.task6.OutOfScreenException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestMoveable {

    @Test
    public void throwExceptionWhenXOnEnd() {
        MovablePoint movablePoint = new MovablePoint(1024, 3);
        Assertions.assertThrows(OutOfScreenException.class, () -> {
            movablePoint.moveRight();
        });
    }

    @Test
    public void throwExceptionWhenXOnTop() {
        MovablePoint movablePoint = new MovablePoint(22, 0);
        Assertions.assertThrows(OutOfScreenException.class, () -> {
            movablePoint.moveUp();
        });
    }

    @Test
    public void testMoveUp() throws OutOfScreenException {
        MovablePoint movablePoint = new MovablePoint(22, 100, 4, 4);
        movablePoint.moveUp();
        Assertions.assertEquals(96, movablePoint.getY());
    }

    @Test
    public void testThrowExceptionMoveCircleRight(){
        MovableCircle movableCircle = new MovableCircle(10, 1014, 40);
        //testujeme ze urcity kod vyhodi vyjimku uvedenou v 1. argumentu
        Assertions.assertThrows(OutOfScreenException.class,()->{
            movableCircle.moveRight();
        });


    }
}
