package cz.sda.java8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestBinaryTree {

    @Test
    public void testSearchInTree(){
        int[] inputArray = {5, 6, 3, 2, 4, 9, 7,0};
        BinaryTree fromArray = BinaryTree.getFromArray(inputArray);
        for(int i:inputArray){
            BinaryTree.Node node = fromArray.binarySearch(i);
            Assertions.assertEquals(i,node.value);
        }
    }
}
