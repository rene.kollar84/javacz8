package cz.sda.java8;

import cz.sda.java8.advancedf.exercises.task1c.UserValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestEmail {
    UserValidator userValidator;
    @BeforeEach
    public void init () {
         userValidator = new UserValidator();
    }
    @Test
    public void testValidator(){
        String s = userValidator.validateEmail("tom@s.cz");
        Assertions.assertEquals("tom@s.cz",s);
    }

    @Test
    public void testNotValidate(){
        String s = userValidator.validateEmail("toms.cz");
        Assertions.assertEquals("unknown",s);
    }
}
