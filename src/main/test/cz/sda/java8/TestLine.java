package cz.sda.java8;

import cz.sda.java8.advancedf.exercises.task1.Point2D;
import cz.sda.java8.advancedf.exercises.task5.BadArguments;
import cz.sda.java8.advancedf.exercises.task5.Line;
import cz.sda.java8.advancedf.exercises.task5.NoEnoughtData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestLine {

    @Test
    public void testMiddlePoint1() throws NoEnoughtData {
        Line line = new Line(-2, -1, 2, 2);
        Point2D expe = new Point2D(0, 0.5f);
        Assertions.assertEquals(expe, line.getMiddlePoint());

    }

    @Test
    public void testMiddlePoint2() throws NoEnoughtData {
        Line line = new Line(2, 2, -2, -1);
        Point2D expe = new Point2D(0, 0.5f);
        Assertions.assertEquals(expe, line.getMiddlePoint());
    }

    @Test
    public void testMiddlePoint3() throws NoEnoughtData {
        Line line = new Line(0, 0, 0, 0);
        Point2D expe = new Point2D(0, 0f);
        Assertions.assertEquals(expe, line.getMiddlePoint());
    }

    @Test
    public void testMiddlePoint4() {

        try {
            Line line = new Line(new Point2D(1, 1), null);
            Point2D expe = new Point2D(0, 0f);
            Point2D middlePoint = line.getMiddlePoint();
        } catch (Throwable e) {
            System.out.println(e.getMessage());
        }finally {
            System.out.println("Finally executed");
        }

    }

    @Test
    public void testMiddlePoint5() {

        Assertions.assertThrows(BadArguments.class, () -> {
            Line line = new Line(new Point2D(1, 1), null);
        });

    }

}
