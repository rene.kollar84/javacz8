package cz.sda.java8;


import cz.sda.java8.advancedf.exercises.task3.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestShapes {

    @Test
    public void testCreateCircleWithEmptyConstructor(){
        Circle circle = new Circle();
        Assertions.assertEquals(Colour.UNKNOWN,circle.getColour());
        Assertions.assertEquals(1,circle.getRadius());
        Assertions.assertEquals(Math.PI,circle.getArea());
    }

    @Test
    public void testCreateCircleWithNoEmptyConstructor(){
        Circle circle = new Circle(Colour.WHITE,true,2);
        Assertions.assertEquals(Colour.WHITE,circle.getColour());
        Assertions.assertEquals(2,circle.getRadius());
        Assertions.assertEquals(Math.PI*4,circle.getArea());
    }
    @Test
    public void testSquereWithEmptyCons(){
        Square square = new Square();
        Assertions.assertEquals(4,square.getPerimetr());
    }

    @Test
    public void testSquereWithNoEmptyCons(){

        Square square = new Square();
        square.setLenght(2);
        square.setWidth(1);
        Assertions.assertEquals(4,square.getPerimetr());
    }

    @Test
    public void onlyShowInheriance(){
        Shape[] shapes=new Shape[]{
                new Circle(),
                new Rectangle(Colour.WHITE,true,2,4),
                new Square(){
                    @Override
                    public String toString() {
                        return super.toString()+"Anonymous";
                    }
                },
                new Shape() {
                    @Override
                    public double getArea() {
                        return Double.NaN;
                    }

                    @Override
                    public double getPerimetr() {
                        return 0;
                    }
                }
        };
        for(Shape sh:shapes){
            System.out.println(sh + "Has area:" + sh.getArea());
        }
    }

    @Test
    public void testCreateSquareFromStatic(){
        Square fromArea = Square.getFromArea(4);
        Assertions.assertEquals(2,fromArea.getWidth());
    }
}
