package cz.sda.java8;

import cz.sda.java8.advancedf.exercises.task7.ResizableCircle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestResizibleCircle {
    @Test
    public void testThrowExceptionBadArgument(){
        ResizableCircle resizableCircle = new ResizableCircle(1);
        try {
            resizableCircle.resize(-20);
        }catch (IllegalArgumentException e){
            Assertions.assertTrue(true);
            return;
        }
        Assertions.assertTrue(false);
    }
}
