package cz.sda.java8;

import cz.sda.java8.designepatterns.bestpractise.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PersonEqualsTest {
    @Test
    public void testEquals1(){
        Person karel = new Person("karel", 1900);
        Person honza = new Person("honza", 1950);
        boolean equals = karel.equals(honza);
        Assertions.assertEquals(false,equals);
    }

    @Test
    public void testEquals2(){
        Person karel = new Person("karel", 1900);
        boolean equals = karel.equals("honza");
        Assertions.assertEquals(false,equals);
    }

    @Test
    public void testEquals3(){
        Person karel = new Person("karel", 1900);

        boolean equals = karel.equals(karel);
        Assertions.assertEquals(true,equals);
    }

    @Test
    public void testEquals4(){
        Person karel = new Person("karel", 1900);
        Person karel2 = new Person(null, 1900);
        boolean equals = karel.equals(karel2);
        Assertions.assertEquals(false,equals);
    }

    @Test
    public void testEquals5(){
        Person karel = new Person("karel", 1900);
        Person karel2 = new Person(null, 1900);
        boolean equals = karel2.equals(karel);
        Assertions.assertEquals(false,equals);
    }
}
