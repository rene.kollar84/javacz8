package cz.sda.java8;

import cz.sda.java8.advancedf.exercises.task2e.Book;
import cz.sda.java8.advancedf.exercises.task2e.BookRepository;
import cz.sda.java8.advancedf.exercises.task2e.NoBookFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BookRepositoryTest {
    BookRepository repository;

    @BeforeEach
    public void init() {
        repository = new BookRepository();
    }

    @Test
    public void addBookTest() {
        Book book = new Book(12, "Novy Zeland", "Jakub Novak", 2005);
        repository.addBook(book);
        Book novy_zeland = repository.findByName("Novy Zeland");
        Assertions.assertEquals(book, novy_zeland);
    }
    @Test
    public void testThrowExepcion(){
        Assertions.assertThrows(NoBookFoundException.class,()->{
            repository.findByName("Novy Zeland");
        });
    }
}
