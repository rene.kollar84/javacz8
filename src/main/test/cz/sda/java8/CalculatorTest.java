package cz.sda.java8;

import cz.sda.java8.tdd.Calculator;
import cz.sda.java8.tdd.MyArithmeticException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    static Calculator calc;

    @BeforeAll
    public static void init(){
        calc = new Calculator();
    }

    @Test
    public void testAdd(){
        int add = calc.add(1, 1);
        Assertions.assertEquals(2,add);
    }
    @Test
    public void testSub(){
        int add = calc.sub(1, 1);
        Assertions.assertEquals(0,add);
    }

    @Test
    public void testDiv(){
        float add = calc.div(1, 2);
        Assertions.assertEquals(0.5,add);
    }

    @Test
    public void testDivZero(){
        Assertions.assertThrows(MyArithmeticException.class,()->{
            float add = calc.div(1, 0);
        });
    }

    @Test
    public void testMax(){
        int add = calc.max(100, -1001);
        Assertions.assertEquals(100,add);
    }

}
