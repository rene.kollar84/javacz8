package cz.sda.java8.coding_advancef.e25;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BasketTest {

    @Test
    void addToBasket() {

        try {
            cz.sda.java8.coding_advancef.e25.Basket basket = new cz.sda.java8.coding_advancef.e25.Basket();
            for (int i = 0; i < 11; i++) {
                basket.addToBasket();
            }
            assertFalse(false);
        }catch(BasketFullException e){
            assertTrue(true);
        }

    }

    @Test
    void removeFromBasket() {
        try {
            new Basket().removeFromBasket();
            assertFalse(false);
        } catch (BasketEmptyException e) {
           assertTrue(true);
        }
    }
}