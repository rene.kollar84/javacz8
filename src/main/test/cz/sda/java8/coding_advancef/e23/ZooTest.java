package cz.sda.java8.coding_advancef.e23;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ZooTest {
    Zoo zoo;

    @BeforeEach
    void init() {
        zoo = new Zoo();
        zoo.addAnimal("Lev",1);
        zoo.addAnimal("Lev", 1);
        zoo.addAnimal("Lvice", 3);
        zoo.addAnimal("Albatros",1);
    }

    @Test
    void addAnimal() {
//        Assertions.assertEquals();
    }

    @Test
    void getNumberOfAnimals() {
        Assertions.assertEquals(6,zoo.getNumberOfAnimals());
    }

    @Test
    void testSort() {
        Map<String, Integer> animalsCountSorted = zoo.getAnimalsCountSorted();
        Assertions.assertEquals("Lvice",animalsCountSorted.keySet().iterator().next());
    }
}