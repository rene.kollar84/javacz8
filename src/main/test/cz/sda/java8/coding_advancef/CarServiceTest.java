package cz.sda.java8.coding_advancef;

import cz.sda.java8.coding_advancef.e12_13.Car;
import cz.sda.java8.coding_advancef.e12_13.CarService;
import cz.sda.java8.coding_advancef.e12_13.Manufacture;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class CarServiceTest {
    CarService carService;
    Car c1;
    Manufacture bmw;

    @BeforeEach
    public void inIt() {
        carService = new CarService();
        bmw = new Manufacture("BMW", 1968, "Germany");
        Manufacture skoda = new Manufacture("Skoda", 1963, "Czech Republic");
        c1 = new Car("a", "Trabant", 2800, 2002, Car.Engine.V12, bmw);
        carService.addCar(c1);
        carService.addCar(new Car("b", "Trabant23", 3000, 1999, Car.Engine.V12, bmw));
        carService.addCar(new Car("c", "Trabant65", 4000, 1998, Car.Engine.V6, bmw));
        carService.addCar(new Car("d", "Skoda120", 4020, 2010, Car.Engine.V4, skoda));

    }

    @Test
    public void removeCarTest() {
        carService.removeCar(c1);
        Assertions.assertEquals(3, carService.getAllCar().size());
    }

    @Test
    public void getAllCarTest() {
        Assertions.assertEquals(4, carService.getAllCar().size());
    }

    @Test
    public void getAllV12Test() {
        Assertions.assertEquals(2, carService.getAllV12Engine().size());
    }

    @Test
    public void getAll1999Test() {
        Assertions.assertEquals(1, carService.getAllBefore1999().size());
    }

    @Test
    public void getMostExpansiveCarTest() {
        Assertions.assertEquals(carService.getAllCar().get(3), carService.getMostExpansiveCar());
    }

    @Test
    public void getCheapestCarTest() {
        Assertions.assertEquals(2800, carService.getCheapestCar().getPrice());
    }

    @Test
    public void getManufactureOfCarTest() {
        Assertions.assertEquals("BMW", carService.getManufactureOfCar().get(0).getName());
    }

    @Test
    public void sortedCarTest() {
        List<Car> cars = carService.sortedCar(true);
        Assertions.assertEquals("a", cars.get(0).getName());
        Assertions.assertEquals("b", cars.get(1).getName());
        Assertions.assertEquals("c", cars.get(2).getName());
        Assertions.assertEquals("d", cars.get(3).getName());

    }

    @Test
    public void sortedCarTestDes() {
        List<Car> cars = carService.sortedCar(false);
        Assertions.assertEquals("a", cars.get(3).getName());
        Assertions.assertEquals("b", cars.get(2).getName());
        Assertions.assertEquals("c", cars.get(1).getName());
        Assertions.assertEquals("d", cars.get(0).getName());

    }

    @Test
    public void containCarTest() {
        Assertions.assertEquals(true, carService.containCar(c1));
    }

    @Test
    public void getByManufactureTest() {
        Assertions.assertEquals(3, carService.getByManufacture(bmw).size());
    }

    @Test
    public void getByManufactureYearTest() {
        Assertions.assertEquals(0, carService.getByManufactureYear(2020, CarService.Operator.EQ).size());
        Assertions.assertEquals(4, carService.getByManufactureYear(2020, CarService.Operator.LT).size());
        Assertions.assertEquals(4, carService.getByManufactureYear(1, CarService.Operator.GRT).size());
        Assertions.assertEquals(1, carService.getByManufactureYear(2010, CarService.Operator.EGRT).size());
        Assertions.assertEquals(4, carService.getByManufactureYear(2020, CarService.Operator.ELT).size());
        Assertions.assertEquals(4, carService.getByManufactureYear(2020, CarService.Operator.NEQ).size());
        Assertions.assertEquals(0, carService.getByManufactureYear(2020, null).size());
    }
}
