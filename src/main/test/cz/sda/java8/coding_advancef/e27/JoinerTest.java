package cz.sda.java8.coding_advancef.e27;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JoinerTest {

    @Test
    void testToString() {
        Joiner<Integer> integerJoiner = new Joiner<>();
        integerJoiner.separator("-");
        integerJoiner.join(1,2,3,4);
        assertEquals("1-2-3-4",integerJoiner.toString());
    }
}