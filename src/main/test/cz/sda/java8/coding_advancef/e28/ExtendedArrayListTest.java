package cz.sda.java8.coding_advancef.e28;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ExtendedArrayListTest {
    static ExtendedArrayList<Integer> list = new ExtendedArrayList<>();

    @BeforeAll
    static void init(){
        list.addAll(List.of(0,1,2,3,4,5,6,7,8,9));
    }

    @Test
    void testSkip(){
        List<Integer> everyNthElement = list.getEveryNthElement(2, 2);
        //2,5,8
        assertEquals(3, everyNthElement.size());
        assertEquals(2,everyNthElement.get(0));
        assertEquals(5,everyNthElement.get(1));
        assertEquals(8,everyNthElement.get(2));
    }

}