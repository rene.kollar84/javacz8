package cz.sda.java8.coding_advancef.e9;

import cz.sda.java8.coding_advancef.e10.MoveDirection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CircleTest {


    Point2D center;
    Point2D anyPoint;
    Circle circle;

    @BeforeEach
    public void init(){
         center = new Point2D(0,0);
         anyPoint = new Point2D(1,0);
         circle = new Circle(center, anyPoint);
    }
    @Test
    public void testComputeRadius(){
        Assertions.assertEquals(1, circle.getRadius());
    }

    @Test
    public void testGetArea() {
        Assertions.assertEquals(Math.PI, circle.getArea());
    }

    @Test
    public void testGetPerimeter() {
        Assertions.assertEquals(2 * Math.PI, circle.getPerimeter());
    }

    @Test
    public void testComputAfterMove() {
        circle.move(new MoveDirection(1, 1));
        Assertions.assertEquals(2 * Math.PI, circle.getPerimeter());
        Assertions.assertEquals(Math.PI, circle.getArea());
        Assertions.assertEquals(1, circle.getRadius());
    }

    @Test
    public void testComputAfterMoveOnePoint() {
        anyPoint.move(new MoveDirection(1, 0));
        Circle circle1 = new Circle(center, anyPoint);
        Assertions.assertEquals(2, circle1.getRadius());
        Assertions.assertEquals(2 * Math.PI * 2, circle1.getPerimeter());
        Assertions.assertEquals(Math.PI * 4, circle1.getArea());

    }

    @Test
    public void testResizable() {
        circle.resize(2d);
        Assertions.assertEquals(2,circle.getRadius());
    }
}
