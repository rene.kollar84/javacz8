package cz.sda.java8.coding_advancef.e20_21_22;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TriangleTest {

    @Test
    void createRectangle() {
        Assertions.assertThrows(RuntimeException.class,()->new Triangle(2.01,1,1));
    }

    @Test
    void calculatePerimeter() {
        Triangle t=new Triangle(1,1,1);
        Assertions.assertEquals(3,t.calculatePerimeter());
    }

    @Test
    void calculateArea() {
        Triangle t=new Triangle(3,4,5);
        Assertions.assertEquals(6,t.calculateArea());
    }
}