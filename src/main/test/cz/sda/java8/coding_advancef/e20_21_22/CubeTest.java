package cz.sda.java8.coding_advancef.e20_21_22;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CubeTest {

    @Test
    void calculatePerimeter() {
        Assertions.assertThrows(UnsupportedOperationException.class,()->new Cube(1).calculatePerimeter());
    }

    @Test
    void calculateArea() {
        Cube c = new Cube(1);
        assertEquals(6, c.calculateArea());
    }

    @Test
    void calculateVolume() {
        Cube c = new Cube(1);
        assertEquals(1, c.calculateVolume());
    }

    @Test
    public void testFillCubeAndCone() {
        Shape3D cube = new Cube(1);
        Shape3D cone = new Cone(1,1);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream newOut = new PrintStream(byteArrayOutputStream);

        System.setOut(newOut);
        cube.fill(1);
        cube.fill(0);
        cube.fill(2);

        cone.fill(1.0471975511965976);
        cone.fill(0);
        cone.fill(30000);
        String output = new String(byteArrayOutputStream.toByteArray());
        assertEquals("akorat\nmalo\nmoc\nakorat\nmalo\nmoc\n",output);
        //
//        Shape3D cube =  Mockito.spy(new Cube(1));
//        ArgumentCaptor<String> ac = ArgumentCaptor.forClass(String.class);
//        Mockito.when(cube.print(ac.capture()));
//        cube.fill(1);
//        cube.fill(0);
//        cube.fill(2);
//        Mockito.or
    }
    @Test
    public void testFillCubeMithMock() {

        Shape3D cube =  new Cube(1);
        PrintStream oldOut = System.out;

        PrintStream spy = Mockito.spy(System.out);
        System.setOut(spy);

        cube.fill(1);
        cube.fill(0);
        cube.fill(2);
        Mockito.verify(spy).println("akorat");
        Mockito.verify(spy).println("moc");
        Mockito.verify(spy).println("malo");

        System.setOut(oldOut);

    }
}