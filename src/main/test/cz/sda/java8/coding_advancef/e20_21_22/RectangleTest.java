package cz.sda.java8.coding_advancef.e20_21_22;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RectangleTest {

    @Test
    void calculatePerimeter() {
       Shape rectangle=new Rectangle(5,4);
        Assertions.assertEquals(18,rectangle.calculatePerimeter());
    }

    @Test
    void calculateArea() {
        Shape rectangle=new Rectangle(5,4);
        Assertions.assertEquals(20,rectangle.calculateArea());
    }
}