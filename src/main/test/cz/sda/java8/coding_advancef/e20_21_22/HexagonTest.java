package cz.sda.java8.coding_advancef.e20_21_22;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class HexagonTest {

    @Test
    void calculatePerimeter() {
        Hexagon h=new Hexagon(1);
        Assertions.assertEquals(6,h.calculatePerimeter());
    }

    @Test
    void calculateArea() {
        Hexagon h=new Hexagon(1);
        Assertions.assertEquals(3*Math.sqrt(3)*1*1/2,h.calculateArea());

    }
}

