package cz.sda.java8.coding_advancef.e20_21_22;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ConeTest {

    @Test
    void calculateVolume() {
        Cone c=new Cone(1,1);
        Assertions.assertEquals(10471,Math.floor(10000*c.calculateVolume()));
    }

    @Test
    void calculatePerimeter() {
        Assertions.assertThrows(UnsupportedOperationException.class, ()->new Cone(1,1).calculatePerimeter());
    }

    @Test
    void calculateArea() {
        Cone c=new Cone(1,1);
        Assertions.assertEquals(7584,Math.floor(1000*c.calculateArea()));
    }
}
