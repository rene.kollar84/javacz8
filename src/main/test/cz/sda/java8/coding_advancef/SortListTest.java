package cz.sda.java8.coding_advancef;

import cz.sda.java8.coding_advancef.e1.SortList;
import cz.sda.java8.coding_advancef.e1.SortListCaseInsensitive;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.List;

public class SortListTest {

    @Test
    public void testSort(){
        List<String> input = List.of("Z","a");
        List<String> strings = SortList.sortDesc(input);
        Assertions.assertEquals("a",strings.get(0));
        Assertions.assertEquals("Z",strings.get(1));

    }
    @Test
    public void testSortCaseins(){
        List<String> input = List.of("Z","a","b");
        List<String> strings = SortListCaseInsensitive.sortDescSensitive(input);
        Assertions.assertEquals("Z",strings.get(0));
        Assertions.assertEquals("b",strings.get(1));
        Assertions.assertEquals("a",strings.get(2));

    }
}
