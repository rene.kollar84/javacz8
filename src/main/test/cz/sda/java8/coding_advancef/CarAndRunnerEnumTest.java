package cz.sda.java8.coding_advancef;

import cz.sda.java8.coding_advancef.e15.CarEnum;
import cz.sda.java8.coding_advancef.e16.Runner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CarAndRunnerEnumTest {
    @Test
    public void testCarEnum() {

        Assertions.assertEquals(true, CarEnum.TOYOTA.isPremium());
        Assertions.assertEquals(false, CarEnum.TOYOTA.isRegular());
        Assertions.assertEquals(true, CarEnum.FERRARI.isFasterThan(CarEnum.OPEL));
    }

    @Test
    public void testRunnerEnum() {
        Assertions.assertEquals(Runner.BEGINNER, Runner.getFitnessLevel(100));
        Assertions.assertEquals(Runner.ADVANCED, Runner.getFitnessLevel(1));
        Assertions.assertEquals(Runner.INTERMEDIATE, Runner.getFitnessLevel(4));
    }
}
