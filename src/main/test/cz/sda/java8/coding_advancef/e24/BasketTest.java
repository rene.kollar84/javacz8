package cz.sda.java8.coding_advancef.e24;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BasketTest {

    @Test
    void addToBasket() {
        assertThrows(BasketFullExceptionRuntime.class,()->{
            Basket basket = new Basket();
            for (int i = 0; i < 11; i++) {
                basket.addToBasket();
            }
        });
    }

    @Test
    void removeFromBasket() {
        assertThrows(BasketEmptyExceptionRuntime.class,()->new Basket().removeFromBasket());
    }
}