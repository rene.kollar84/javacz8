package cz.sda.java8.coding_advancef.e18_19;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComputerTest {
    @Test
    public void testComputerEqual(){
        Computer computer = new Computer("i7", 32, GraphicsCard.NVIDIA, "HP", "305");
        Computer computer1 = new Computer("i7", 32, GraphicsCard.NVIDIA, "HP", "305");
        List<Computer> computers = List.of(computer);
        //Assertions.assertEquals(true, computer.equals(computer1));
        Assertions.assertEquals(true, computers.contains(computer1));
    }
    @Test
    public void testComputerToString() {
        Computer computer = new Computer("i7", 32, GraphicsCard.NVIDIA, "HP", "305");

        //Assertions.assertEquals(true, computer.equals(computer1));
        Assertions.assertEquals("Computer{processor='i7', ram=32, graphicsCard=NVIDIA, company='HP', model='305'}", computer.toString());
    }
    @Test
    public void testComputerHashcode() {
        Computer computer = new Computer("i7", 32, GraphicsCard.NVIDIA, "HP", "305");
        Computer computer1 = new Computer("i7", 32, GraphicsCard.NVIDIA, "HP", "305");
        Map<Computer, Integer> computerCounts = new HashMap<>();
        computerCounts.put(computer, 1);
        Assertions.assertNotNull(computerCounts.get (computer1));
    }
    @Test
    public void testLaptopEqual(){
        Computer computer = new Laptop("i7", 32, GraphicsCard.NVIDIA, "HP", "305",
                20000);
        Computer computer1 = new Laptop ("i7", 32, GraphicsCard.NVIDIA, "HP", "305",
                20000);
        List<Computer> computers = List.of(computer);
        //Assertions.assertEquals(true, computer.equals(computer1));
        Assertions.assertEquals(true, computers.contains(computer1));
    }

    @Test
    public void testLaptopHashcode() {
        Computer computer = new Laptop("i7", 32, GraphicsCard.NVIDIA, "HP", "305",
                20000);
        Computer computer1 = new Laptop("i7", 32, GraphicsCard.NVIDIA, "HP", "305",
                25000);
        Map<Computer, Integer> computerCounts = new HashMap<>();
        computerCounts.put(computer, 1);
        Assertions.assertNull(computerCounts.get(computer1));
    }
}
