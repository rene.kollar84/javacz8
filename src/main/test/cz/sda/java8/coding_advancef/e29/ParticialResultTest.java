package cz.sda.java8.coding_advancef.e29;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ParticialResultTest {

    @Test
    void partOf() {
        List<String> input = List.of("ahoj", "Mak", "se", "Mame");

        // ahoj, Mak, se, Mame

        // polozka.startWithLowerLetter
        //ahoj, se

        //polozka.startWith M
        assertEquals(100, ParticialResult.partOf(e -> true, input));
        assertEquals(0, ParticialResult.partOf(e -> false, input));
        assertEquals(50, ParticialResult.partOf(e -> e.startsWith("M"), input));
        assertEquals(25, ParticialResult.partOf(e -> e.equals("ahoj"), input));
    }
}