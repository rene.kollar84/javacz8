package cz.sda.java8.coding_advancef.e5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SDAHashSetTest {
     HashSet<String> set;

    @BeforeEach
    public  void init() {
        set = new SDAHashSet<>();
    }

    @Test
    void add() {
        boolean added = set.add("Ahoj");
        Assertions.assertEquals(true,added);
        added = set.add("Ahoj");
        assertEquals(false,added);

        assertEquals(1,set.size());
    }

    @Test
    void remove() {
        boolean contains = set.remove("Ahoj");
        assertEquals(false,contains);
        boolean ahoj = set.add("Ahoj");
        contains = set.remove("Ahoj");
        assertEquals(true,contains);
    }

    @Test
    void size() {
        assertEquals(0,set.size());
        set.add("Ahoj");
        set.add("ne");
        assertEquals(2,set.size());
    }

    @Test
    void contains() {
        assertEquals(false,set.contains("Ahoj"));
        set.add("Ahoj");
        assertEquals(true,set.contains("Ahoj"));
    }

    @Test
    void clear() {
        set.add("Ahoj");
        set.add("ne");
        set.clear();
        assertEquals(0,set.size());
    }
}