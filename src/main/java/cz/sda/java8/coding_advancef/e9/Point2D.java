package cz.sda.java8.coding_advancef.e9;

import cz.sda.java8.coding_advancef.e10.Movable;
import cz.sda.java8.coding_advancef.e10.MoveDirection;

public class Point2D implements Movable {

    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getY() {
        return y;
    }

    @Override
    public void move(MoveDirection moveDirection) {
        setX(getX() + moveDirection.getX());
        setY(getY() + moveDirection.getY());
    }
}
