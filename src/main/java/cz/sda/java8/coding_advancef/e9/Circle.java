package cz.sda.java8.coding_advancef.e9;

import cz.sda.java8.coding_advancef.e10.Movable;
import cz.sda.java8.coding_advancef.e10.MoveDirection;
import cz.sda.java8.coding_advancef.e11.Resizable;

public class Circle implements Movable, Resizable {
    private Point2D center;
    private Point2D anyPoint;

    public Circle(Point2D center, Point2D anyPoint) {
        this.center = center;
        this.anyPoint = anyPoint;
    }

    public double getRadius() {
        return Math.sqrt(Math.pow(Math.abs(center.getX() - anyPoint.getX()), 2)
                + Math.pow(Math.abs(center.getY() - anyPoint.getY()), 2));
    }

    public double getPerimeter() {
        return (2 * Math.PI * getRadius());
    }

    public double getArea() {
        return (Math.PI * Math.pow(getRadius(), 2));
    }

    @Override
    public void move(MoveDirection moveDirection) {
        center.move(moveDirection);
        anyPoint.move(moveDirection);
    }

    @Override
    public void resize(double factor) {
        anyPoint.setX(center.getX() + getRadius() * factor);
        anyPoint.setY(center.getY());
    }

//    public Point2D[] upperRight(){
//        Point2D upperRight = null;
//        if (anyPoint.getY()>=center.getY()){
//            if (anyPoint.getX()>= center.getX()){
//                upperRight = anyPoint;
//            } else {
//                upperRight.setY(anyPoint.getY());
//                upperRight.setX(anyPoint.getX()+2*(center.getX()-anyPoint.getX()));
//            }
//        } else {
//            if (anyPoint.getX()>= center.getX()){
//                upperRight.setX(anyPoint.getX());
//                upperRight.setY(anyPoint.getY()+2*(center.getY()-anyPoint.getY()));
//            } else {
//                upperRight.setX(anyPoint.getX()+2*(center.getX()-anyPoint.getX()));
//                upperRight.setY(anyPoint.getY()+2*(center.getY()-anyPoint.getY()));
//            }
//
//        }
//        return upperRight;
//    }


    //Create a Point2D class with fields double x, double y, getters, setters and constructor. Then create a Circle
    //class that will have a constructor:
    //Circle(Point2D center, Point2D point)
    //The first parameter specifies the center of the circle, the second is any point on the circle. Based on these points, the Circle class is to determine:
    //• circle radius when calling double getRadius() method
    //• circle circumference when calling double getPerimeter() method
    //• circle area when calling double getArea() method
    //• * (challenging) three points on the circle every 90 degrees from the point given when calling the List<Point2D> getSlicePoints() method
}


