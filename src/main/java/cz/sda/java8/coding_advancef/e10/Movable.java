package cz.sda.java8.coding_advancef.e10;

public interface Movable {
    void move(MoveDirection moveDirection);
}
