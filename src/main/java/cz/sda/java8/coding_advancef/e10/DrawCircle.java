package cz.sda.java8.coding_advancef.e10;

import cz.sda.java8.coding_advancef.e9.Circle;
import cz.sda.java8.coding_advancef.e9.Point2D;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;

public class DrawCircle extends JPanel {

    Circle circle;
    Point2D center;
    Point2D anyPoint;
    public DrawCircle() {
        center=new Point2D(50,50);
        anyPoint=new Point2D(100,50);
        circle = new Circle(center,anyPoint);
    }



    public void paintComponent(Graphics g) {
        int width = getWidth();
        int height = getHeight();
        g.setColor(Color.black);
        int radius= (int) circle.getRadius();
        g.drawOval((int) center.getX(), (int) center.getY(), radius,radius);
    }

    private void move(){
        circle.move(new MoveDirection(10,10));
        SwingUtilities.updateComponentTreeUI(this);
    }

    private void resize(double factor){
        circle.resize(factor);
        SwingUtilities.updateComponentTreeUI(this);
    }

    public static void main(String args[]) throws InterruptedException {
        JFrame frame = new JFrame("Oval Sample");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        DrawCircle dc = new DrawCircle();
        frame.add(dc);
        frame.setSize(300, 200);
        frame.setVisible(true);
        for(int i=0;i<10;i++){
            dc.move();
            Thread.sleep(1000);
        }
        dc.resize(2);
    }
}