package cz.sda.java8.coding_advancef.e23;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Zoo {
    private Map<String, Integer> animals = new HashMap<>();

    public void addAnimal(String name, int count) {
        if (animals.get(name) == null) {
            animals.put(name, count);
        } else {
            int actualCount = animals.get(name);
            animals.remove(name);
            animals.put(name, actualCount + count);
        }
    }

    public int getNumberOfAnimals() {
        Optional<Integer> reduce = animals.values().stream().reduce((n1, n2) -> n1 + n2);
        return reduce.isPresent() ? reduce.get() : 0;
    }

    public Map<String, Integer> getAnimalsCount() {
        return animals;
    }

    public Map<String, Integer> getAnimalsCountSorted() {
        return animals.entrySet().stream().sorted((e1, e2) -> e1.getValue()-e2.getValue())
                .collect(Collectors.toMap(e -> e.getKey(), v -> v.getValue()));
    }

}
