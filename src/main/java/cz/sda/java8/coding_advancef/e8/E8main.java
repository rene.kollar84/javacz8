package cz.sda.java8.coding_advancef.e8;

public class E8main {

    public static void main(String[] args) {
        Parcel pack = new Parcel.ParcelBuilder()
                .isExpress(true)
                .weight(16)
                .xLenght(30)
                .yLenght(30)
                .zLenght(10)
                .build();
        System.out.println(pack.validate(pack));
    }
}
