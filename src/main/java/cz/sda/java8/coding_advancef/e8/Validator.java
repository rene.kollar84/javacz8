package cz.sda.java8.coding_advancef.e8;

public interface Validator {

    boolean validate(Parcel box);
}
