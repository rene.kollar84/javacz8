package cz.sda.java8.coding_advancef.e8;

public class Parcel implements Validator {
    int xLength;
    int yLength;
    int zLength;
    float weight;
    boolean isExpress;


    @Override
    public boolean validate(Parcel box) {

        return validateSize(box) & validateWeight(box);
    }

    private boolean validateSize(Parcel box) {
        if ((box.xLength >= 30) && (box.yLength >= 30) && (box.zLength >= 30)
                && ((box.xLength + box.yLength + box.zLength) <= 300)) {
            return true;
        }
        System.out.println("Dimensional conditions for transport not fulfilled");
        return false;
    }

    private boolean validateWeight(Parcel box) {
        boolean b = box.isExpress ? box.weight < 15 : box.weight < 30;
        if (!b) {
            System.out.println("Too heavy for picked transport method");
        }
        return b;
    }

    private Parcel(int xLength, int yLength, int zLength, float weight, boolean isExpress) {
        this.xLength = xLength;
        this.yLength = yLength;
        this.zLength = zLength;
        this.weight = weight;
        this.isExpress = isExpress;
    }

    public static class ParcelBuilder {
        private int xLength;
        private int yLength;
        private int zLength;
        private float weight;
        private boolean isExpress;

        public ParcelBuilder xLenght(int x) {
            this.xLength = x;
            return this;
        }

        public ParcelBuilder yLenght(int y) {
            this.yLength = y;
            return this;
        }

        public ParcelBuilder zLenght(int z) {
            this.zLength = z;
            return this;
        }

        public ParcelBuilder weight(float w) {
            this.weight = w;
            return this;
        }

        public ParcelBuilder isExpress(boolean express) {
            this.isExpress = express;
            return this;
        }

        public Parcel build() {
            return new Parcel(xLength, yLength, zLength, weight, isExpress);
        }
    }
}
