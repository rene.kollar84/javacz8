package cz.sda.java8.coding_advancef.e30;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RevertFileSaver {
    //Write a program that will read any file and save it in the same folder.
    // The content and title of the new file should
    //be reversed (from the back).

    public static void saveRevert(String fileName) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Files.copy(Path.of(fileName), out);
        byte[] bytes = out.toByteArray();

        byte[] bytes1 = new byte[bytes.length];
        for (int i = 0; i < bytes1.length; i++) {
            bytes1[i] = bytes[bytes.length - i - 1];
        }

        FileOutputStream fileOutputStream = new FileOutputStream(fileName + "reverted");
        fileOutputStream.write(bytes1, 0, bytes.length);
        fileOutputStream.close();
    }

    public static void main(String[] args) throws IOException {
        saveRevert("/Users/rkollar/IdeaProjects/javacz8/filetorevert.txt");
    }
}
