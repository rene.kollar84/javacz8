package cz.sda.java8.coding_advancef.e33;

import java.io.File;
import java.util.Arrays;

//Write a program that will display all photos (.png, .jpg) in a given directory and subdirectories.
public class Main {
    public static void main(String[] args) {
        //explanation of shorted evaluation
        int i = 10;
        boolean result = i < 10 || i++ < 10;
        //result is true and i is still 10
        boolean resultFull = i < 10 | i++ < 10;
        //resullt is still true but i=11

      //  URL d = Main.class.getResource("/cz/java5/p33/1");
        printAllPhotos(new File("/Users/rkollar/IdeaProjects/javacz8/src/main/resources/cz.java8.p33"));
    }

    public static void printAllPhotos(File filePath) {

        //check if given file is directory
        if (filePath.isDirectory()) {
            File[] files = filePath.listFiles(pathname -> pathname.isFile() && (pathname.getName().endsWith(".jpg")
                    || pathname.getName().endsWith(".png")));
            Arrays.stream(files).forEach(System.out::println);

            Arrays.stream(filePath.listFiles(pathname -> pathname.isDirectory()))
                    .forEach(Main::printAllPhotos);
            //it is same as
             // .forEach(f->printAllPhotos(f));
        }
    }
}
