package cz.sda.java8.coding_advancef.e31;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordCouter {
    //Write a program that will count the occurrences of each word
    // in a text file and then save the results in the form
    //of a table in a new file
    public static void main(String[] args) throws IOException {
        printWordCount("/Users/rkollar/IdeaProjects/javacz8/words.txt");
    }

    private static void printWordCount(String s) throws IOException {
        List<String> strings = Files.readAllLines(Path.of(s));

        //content of words.txt is
        //ahoj jak jak jak jak se
        //se se se se se se mame mame

        //we need to get list of words
        Map<String, List<String>> collect = strings.stream()
                .flatMap(line -> Stream.of(line.split(" ")))//map list of lines of words to stream of words
                .collect(Collectors.groupingBy(word -> word));//group each word to unique pair of word-List of thes word
        //now collect contains
        //ahoj - {ahoj}
        // jak - {jak,jak,jak}
        //...

        StringBuilder ret = new StringBuilder();
        collect.forEach((key, value) -> {
            ret.append(key + "-" + value.size() + "\n");
        });

        Files.writeString(Path.of(s + "count"), ret, StandardOpenOption.CREATE_NEW);
    }
}
