package cz.sda.java8.coding_advancef.e17;

//Create a ConversionType enum class with the constants METERS_TO_YARDS, YARDS_TO_METERS,
//CENTIMETERS_TO_ICHES, INCHES_TO_CENTIMETERS, KILOMETERS_TO_MILES, MILES_TO_KILOMETERS.
//Enum should have a ConverterType parameter used to perform calculations for a given type.
//Then create a MeasurementConverter class that will have the convert(int value, ConvertionType
//conversionType) method and based on the value and type of conversion, used the Converter of the given
//type and returned the result.

public enum ConversionType {
    METERS_TO_YARDS, YARDS_TO_METERS, CENTIMETERS_TO_INCHES, INCHES_TO_CENTIMETERS, KILOMETERS_TO_MILES, MILES_TO_KILOMETERS;

//    public double getConversion(ConversionType cv, double x, double y) {
//        switch (cv) {
//            case METERS_TO_YARDS: //1 meter = 1.0936133 yards
//                return y * x;
//            case YARDS_TO_METERS:
//                return y / x;
//            case CENTIMETERS_TO_INCHES: //1 centimeter = 0.393700787 inches
//                return y * x;
//            case INCHES_TO_CENTIMETERS:
//                return y / x;
//            case KILOMETERS_TO_MILES: //1 kilometer = 0.621371192 miles
//                return y * x;
//            case MILES_TO_KILOMETERS:
//                return y / x;
//        }
//
//        return x;
//    }


//    METERS_TO_YARDS {
//        private double convertMetersToYards(double meter, double yard) {
//            return meter * yard;
//        }
//    },
//    YARDS_TO_METERS {
//        private double convertYardsToMeter(double yard, double meter) {
//            return yard / meter;
//        }
//    },
//    CENTIMETERS_TO_INCHES {
//        private double convertCentimetersToInches(double centimeter, double inch) {
//            return centimeter / inch;
//        }
//    },
//    INCHES_TO_CENTIMETERS {
//        private double convertInchesToCentimeter(double inch, double centimeter) {
//            return inch * centimeter;
//        }
//    },
//    KILOMETERS_TO_MILES {
//        private double convertKilometersToMiles(double kilometer, double mile) {
//            return kilometer * mile;
//        }
//    },
//    MILES_TO_KILOMETERS {
//        private double convertMilesToKilometer(double mile, double kilometer) {
//            return mile / kilometer;
//        }
//    },
//    ;

}
