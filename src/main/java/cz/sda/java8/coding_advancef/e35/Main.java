package cz.sda.java8.coding_advancef.e35;

public class Main {
    public static void main(String[] args) {
        MyRunnable myRunnable = new MyRunnable();
        Thread myThread = new Thread(myRunnable);
        myThread.start();
        Thread myThread1 = new Thread(()-> System.out.println("Ahoj"));
        myThread1.start();
    }
}
