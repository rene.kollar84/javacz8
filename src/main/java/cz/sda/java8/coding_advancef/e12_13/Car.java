package cz.sda.java8.coding_advancef.e12_13;

public class Car {
    private String name;
    private String model;
    private int price;
    private int yearOfManufacture;
    private Engine engine;
    private Manufacture manufacture;

    public Car(String name, String model, int price, int yearOfManufacture, Engine engine, Manufacture manufacture) {
        this.name = name;
        this.model = model;
        this.price = price;
        this.yearOfManufacture = yearOfManufacture;
        this.engine = engine;
        this.manufacture = manufacture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setYearOfManufacture(int yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Manufacture getManufacture() {
        return manufacture;
    }

    public void setManufacture(Manufacture manufacture) {
        this.manufacture = manufacture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (price != car.price) return false;
        if (yearOfManufacture != car.yearOfManufacture) return false;
        if (!name.equals(car.name)) return false;
        if (!model.equals(car.model)) return false;
        if (engine != car.engine) return false;
        return manufacture.equals(car.manufacture);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + model.hashCode();
        result = 31 * result + price;
        result = 31 * result + yearOfManufacture;
        result = 31 * result + engine.hashCode();
        result = 31 * result + manufacture.hashCode();
        return result;
    }

    public enum Engine {
        V12, V8, V6, V4
    }
}
