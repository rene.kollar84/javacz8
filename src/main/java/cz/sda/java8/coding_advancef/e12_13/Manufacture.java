package cz.sda.java8.coding_advancef.e12_13;

public class Manufacture {
    private String name;
    private int yearOfEstablishment;
    private String country;

    public Manufacture(String name, int yearOfEstablishment, String country) {
        this.name = name;
        this.yearOfEstablishment = yearOfEstablishment;
        this.country = country;

    }

    public String getName() {
        return name;
    }

    public int getYearOfEstablishment() {
        return yearOfEstablishment;
    }

    public String getCountry() {
        return country;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYearOfEstablishment(int yearOfEstablishment) {
        this.yearOfEstablishment = yearOfEstablishment;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Manufacture that = (Manufacture) o;

        if (yearOfEstablishment != that.yearOfEstablishment) {
            return false;
        }
        if (!name.equals(that.name)) {
            return false;
        }
        return country.equals(that.country);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + yearOfEstablishment;
        result = 31 * result + country.hashCode();
        return result;
    }
}
