package cz.sda.java8.coding_advancef.e12_13;

import java.util.*;
import java.util.stream.Collectors;

//1. adding cars to the list,
//2. removing a car from the list,
//3. returning a list of all cars,
//4. returning cars with a V12 engine,
//5. returning cars produced before 1999,
//6. returning the most expensive car,
//7. returning the cheapest car,
//8. returning the car with at least 3 manufacturers,
//9. returning a list of all cars sorted according to the passed parameter: ascending / descending,
//10. checking if a specific car is on the list,
//11. returning a list of cars manufactured by a specific manufacturer,
//12. returning the list of cars manufactured by the manufacturer with the year of establishment <,>, <=,> =,
//=,! = from the given.
public class CarService {
    List<Car> cars = new ArrayList<>();

    public void addCar(Car car) {
        cars.add(car);
    }

    public void removeCar(Car car) {
        cars.remove(car);
    }

    public List<Car> getAllCar() {
        return Collections.unmodifiableList(cars);
    }

    public List<Car> getAllV12Engine() {
        return cars.stream().filter(p -> p.getEngine() == Car.Engine.V12).collect(Collectors.toList());
    }

    public List<Car> getAllBefore1999() {
        return cars.stream().filter(c -> c.getYearOfManufacture() < 1999).collect(Collectors.toList());
    }

    public Car getMostExpansiveCar() {
        return cars.stream().max(Comparator.comparingInt(Car::getPrice)).get();
        // return cars.stream().max((o1, o2) -> o1.getPrice() - o2.getPrice()).get();
    }

    public Car getCheapestCar() {
        return cars.stream().min(Comparator.comparingInt(Car::getPrice)).get();
    }

    public List<Manufacture> getManufactureOfCar() {
        Map<Manufacture, List<Car>> collect = cars.stream().collect(Collectors.groupingBy(c -> c.getManufacture()));
        return collect.entrySet().stream().filter(e -> e.getValue().size() > 2).map(e -> e.getKey()).collect(Collectors.toList());
    }

    public List<Car> sortedCar(boolean asc) {
        return cars.stream().sorted((c1, c2) -> asc ? c1.getName().compareToIgnoreCase(c2.getName()) : c2.getName().compareToIgnoreCase(c1.getName())).collect(Collectors.toList());
    }

    public boolean containCar(Car car) {
        return cars.contains(car);
    }

    public List<Car> getByManufacture(Manufacture manufacture) {
        return cars.stream().filter(c -> c.getManufacture().equals(manufacture)).collect(Collectors.toList());
    }

    public List<Car> getByManufactureYear(int year, Operator op) {
        return op == null ? Collections.emptyList() : cars.stream().filter(c ->
                switch (op) {
                    case EQ -> c.getYearOfManufacture() == year;
                    case LT -> c.getYearOfManufacture() < year;
                    case GRT -> c.getYearOfManufacture() > year;
                    case EGRT -> c.getYearOfManufacture() >= year;
                    case ELT -> c.getYearOfManufacture() <= year;
                    case NEQ -> c.getYearOfManufacture() != year;

                    default -> throw new RuntimeException("Never thrown");

                }
        ).collect(Collectors.toList());
    }

    public enum Operator {
        EQ, NEQ, GRT, LT, EGRT, ELT
    }
}
