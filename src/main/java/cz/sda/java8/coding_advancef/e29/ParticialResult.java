package cz.sda.java8.coding_advancef.e29;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ParticialResult {

    public static <T> double partOf(Predicate<T> predicate, List<T> input){
        // ahoj, Mak, se, Mame

        // polozka.startWithLowerLetter
        //ahoj, se

        //polozka.startWith M
        int filteredSize = input.stream().filter(predicate).collect(Collectors.toList()).size();
        return 100.0 * filteredSize / input.size() ;
    }
}
