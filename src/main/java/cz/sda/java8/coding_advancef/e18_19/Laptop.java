package cz.sda.java8.coding_advancef.e18_19;

import java.util.Objects;

public class Laptop extends Computer{
    private int batteryCapacity;

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "batteryCapacity=" + batteryCapacity +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Laptop laptop = (Laptop) o;
        return batteryCapacity == laptop.batteryCapacity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), batteryCapacity);
    }

    public Laptop(String processor, int ram, GraphicsCard graphicsCard, String company, String model, int batteryCapacity) {
        super(processor, ram, graphicsCard, company, model);
        this.batteryCapacity = batteryCapacity;

    }

    //Create a Laptop class extending the Computer class from the previous task. The Laptop class should
    //additionally contain the battery parameter.
    //Implement additional getters, setters, constructor and overwrite the toString(), equals() and hashcode() methods accordingly.
    //Use a reference to parent class methods.
}
