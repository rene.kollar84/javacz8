package cz.sda.java8.coding_advancef.e18_19;

public enum GraphicsCard {
    NVIDIA(16), GFORCE(32), NVIDIA_32(32);
    int memSize;

    GraphicsCard(int memSize) {
        this.memSize = memSize;
    }

    public int getMemSize() {
        return memSize;
    }
}
