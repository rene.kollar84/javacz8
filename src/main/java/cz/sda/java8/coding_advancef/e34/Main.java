package cz.sda.java8.coding_advancef.e34;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        MyThread myThread = new MyThread();
        myThread.setName("Jakub");
        myThread.setPriority(Thread.MAX_PRIORITY);
        MyThread myThread1 = new MyThread();
        myThread1.setPriority(Thread.MIN_PRIORITY);

        myThread.start();
        myThread.join();
        myThread1.start();
    }
}
