package cz.sda.java8.coding_advancef.e20_21_22;

public abstract class Shape3D extends Shape implements Fillable{

    public abstract double calculateVolume();

    @Override
    public void fill(double amount) {
        double volume = calculateVolume();
        if (volume == amount){
            System.out.println("akorat");
        }else if(volume<amount){
            System.out.println("moc");
        }else{
            System.out.println("malo");
        }
    }


}

//Add an additional
//method calculateVolume().
//Create Cone and Qube classes by extending the 3DShape class, properly implementing abstract methods.
//Verify the solution correctness.
