package cz.sda.java8.coding_advancef.e20_21_22;

public class Cone extends Shape3D{

    double r;
    double v;

    public Cone(double r, double v) {
        this.r = r;
        this.v = v;
    }

    @Override
    public double calculateVolume() {
        return Math.PI*r*r*v/3;
    }

    @Override
    public double calculatePerimeter() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double calculateArea() {
        return Math.PI*r*r+Math.PI*r*Math.sqrt(r*r+v*v);
    }
}

//Add an additional
//method calculateVolume().
//Create Cone and Qube classes by extending the 3DShape class, properly implementing abstract methods.
//Verify the solution correctness.
