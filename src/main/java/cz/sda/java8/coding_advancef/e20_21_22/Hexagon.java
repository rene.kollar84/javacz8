package cz.sda.java8.coding_advancef.e20_21_22;

public class Hexagon extends Shape{
    private double a;

    public Hexagon(double a) {
        this.a = a;
    }

    @Override
    public double calculatePerimeter() {
        return 6*a;
    }

    @Override
    public double calculateArea() {
        return (3*Math.sqrt(3)*a*a)/2;
    }
}
