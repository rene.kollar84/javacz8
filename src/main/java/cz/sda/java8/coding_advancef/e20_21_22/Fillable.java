package cz.sda.java8.coding_advancef.e20_21_22;

public interface Fillable {
    void fill(double amount);
}
