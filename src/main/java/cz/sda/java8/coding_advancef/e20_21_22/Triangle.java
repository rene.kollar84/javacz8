package cz.sda.java8.coding_advancef.e20_21_22;

public class Triangle extends Shape {
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) {
        if ((a+b>c) && (a+c>b) && (b+c>a)) {
            this.a = a;
            this.b = b;
            this.c = c;
        } else {
          throw new RuntimeException("nejde skonstruovat trojuholnik");
        }
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    @Override
    public double calculatePerimeter() {
        return a+b+c;
    }

    @Override
    public double calculateArea() {
        double s=(a+b+c)/2;

        return Math.sqrt(s*(s-a)*(s-b)*(s-c));
    }
}
//   Create Rectangle, Triangle, Hexagon classes, extending the Shape class, and implementing abstract
//methods accordingly. Verify the solution correctness.