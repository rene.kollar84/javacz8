package cz.sda.java8.coding_advancef.e20_21_22;

public class Cube extends Shape3D{
    double a;

    public Cube(double a) {
        this.a = a;
    }


    @Override
    public double calculatePerimeter() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double calculateArea() {
        return 6*a*a;
    }

    @Override
    public double calculateVolume() {
        return a*a*a;
    }
}
