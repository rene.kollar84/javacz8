package cz.sda.java8.coding_advancef.p26;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Manufacturer {
    public String name;
    public int yearOfCreation;
    List<Model> models;
}
