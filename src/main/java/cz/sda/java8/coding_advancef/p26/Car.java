package cz.sda.java8.coding_advancef.p26;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Car {
    public String name;
    public String description;
    public CarType carType;
}
