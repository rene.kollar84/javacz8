package cz.sda.java8.coding_advancef.p26;


public enum CarType {
    COUPE, CABRIO, SEDAN, HATCHBACK
}
