package cz.sda.java8.coding_advancef.p26;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    Manufacturer toyota;
    Manufacturer vw;
    Manufacturer kia;

    List<Manufacturer> manufacturers;

    public static void main(String[] args) {
        Main main = new Main();
        //6. list of all years of starting production of models,
//        main.manufacturers.stream().forEach(System.out::println);
//        main.manufacturers.stream().flatMap(m -> m.models.stream()).forEach(System.out::println);
//        main.manufacturers.stream().flatMap(m -> m.models.stream()).map(Model::getProductionStartYear).forEach(System.out::println);
        main.listAllModels();
        main.listAllCars();
        main.listAllManufactureNames();
        main.f4();
        main.listAllModelsName();
        main.all();
    }

    public Main() {
        Car c1 = Car.builder()
                .carType(CarType.HATCHBACK)
                .name("Corolla")
                .description("skvele auto")
                .build();
        Car c2 = Car.builder()
                .carType(CarType.CABRIO)
                .name("VW")
                .description("skvele auto")
                .build();
        Car c3 = Car.builder()
                .carType(CarType.SEDAN)
                .name("Kia")
                .description("skvele auto")
                .build();

        Model model1 = new Model("2019", 2019, List.of(c1));
        Model model2 = new Model("Polo", 2020, List.of(c2));
        Model model3 = new Model("Ceed", 2019, List.of(c3));

        toyota = new Manufacturer("Toyota", 1951, List.of(model1));
        vw = new Manufacturer("Vw", 1930, List.of(model2));
        kia = new Manufacturer("Kia", 1980, List.of(model3));
        manufacturers = List.of(toyota, vw, kia);
    }

    //1. a list of all Models,
//2. a list of all cars,
//3. list of all manufacturer names,
//4. list of all manufacturers' establishment years,
    void f4() {
        manufacturers.stream().map(Manufacturer::getYearOfCreation).forEach(System.out::println);
    }

    //5. list of all model names,
    void listAllModelsName() {
        manufacturers.stream().flatMap(m -> m.models.stream()).map(Model::getName).forEach(System.out::println);
    }

    void all() {
        //6. list of all years of starting production of models,
        manufacturers.stream().flatMap(m -> m.models.stream()).map(Model::getProductionStartYear).forEach(System.out::println);
//7. list of all car names,
        manufacturers.stream().flatMap(m -> m.getModels().stream()).flatMap(m -> m.cars.stream())
                .map(c -> c.getName()).forEach(System.out::println);
//8. list of all car descriptions,
        manufacturers.stream().flatMap(m -> m.getModels().stream()).flatMap(m -> m.cars.stream())
                .map(Car::getDescription).forEach(System.out::println);
//9. only models with an even year of production start,
        manufacturers.stream().filter(m -> m.yearOfCreation % 2 == 0).flatMap(m -> m.getModels().stream()).flatMap(m -> m.cars.stream())
                .forEach(System.out::println);
//10. only cars from manufacturers with an even year of foundation,
        manufacturers.stream().filter(m -> m.yearOfCreation % 2 != 0).flatMap(m -> m.getModels().stream()).flatMap(m -> m.cars.stream())
                .forEach(System.out::println);
//11. only cars with an even year of starting production of the model and an odd year of establishing the
//manufacturer,
        manufacturers.stream().filter(m -> m.yearOfCreation % 2 != 0).flatMap(m -> m.getModels().stream())
                .filter(m->m.getProductionStartYear() % 2 == 0)
                .flatMap(m -> m.cars.stream())
                .forEach(System.out::println);
//12. only CABRIO cars with an odd year of starting model production and an even year of establishing the
//manufacturer,
        manufacturers.stream().filter(m -> m.yearOfCreation % 2 == 0).flatMap(m -> m.getModels().stream())
                .filter(m->m.getProductionStartYear() % 2 != 0)
                .flatMap(m -> m.cars.stream())
                .filter(c->c.carType==CarType.CABRIO)
                .forEach(System.out::println);
//13. only cars of the SEDAN type from a model newer than 2019 and the manufacturer's founding year less than
//1919
        manufacturers.stream().filter(m -> m.yearOfCreation < 2019).flatMap(m -> m.getModels().stream())
                .filter(m->m.getProductionStartYear() > 2019)
                .flatMap(m -> m.cars.stream())
                .filter(c->c.carType==CarType.SEDAN)
                .forEach(System.out::println);
    }

    void listAllModels() {
        manufacturers.stream().flatMap(m -> m.models.stream()).forEach(System.out::println);
    }

    void listAllCars() {
        manufacturers.stream()
                .flatMap(m -> m.models.stream())
                .flatMap(m -> m.cars.stream()).forEach(System.out::println);
    }

    void listAllManufactureNames() {
        manufacturers.stream().map(Manufacturer::getName).forEach(System.out::println);
    }
}
