package cz.sda.java8.coding_advancef.p26;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Model {
    public String name;
    public int productionStartYear;
    List<Car> cars;
}
