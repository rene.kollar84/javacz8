package cz.sda.java8.coding_advancef.e38;

public class CofeeMaker {
    //Write an application that will simulate a coffee making machine.
    // In the event that any coffee brewing service
    //finds an empty water tank, the thread should stop. When the water
    // is refilled in the machine, the thread
    //should be excited.

    private int tank;

    private int coffeCount;

    public CofeeMaker(int tank) {
        this.tank = tank;
    }

    public synchronized void prepareCoffe(int size) throws InterruptedException {
        if (size % 10 != 0) {
            throw new RuntimeException("size must be dividedable 10");
        }
        System.out.println("Preparing coffe:" + (++coffeCount));

        for (int i = 0; i < size; i += 10) {
            while(tank - 10 < 0){
                notify();
                wait();
            }
            tank -= 10;
            System.out.print(i+" ");
            delay(200);
        }
        System.out.println("\nCoffe maked in tank remains "+tank);
    }

    public synchronized void fillTank(int amount) throws InterruptedException {
        System.out.println("Prepared to fill tank");
        delay(500);
        tank += amount;
        System.out.println("tank filled");
        notify();
        wait();
    }


    private void delay(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {

        }
    }
}
