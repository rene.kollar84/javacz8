package cz.sda.java8.coding_advancef.e38;

public class Kitchen {
    public static void main(String[] args) {


        CofeeMaker cofeeMaker = new CofeeMaker(150);

        new Thread(() -> {
            try {
                for (int i = 0; i < 2; i++) {
                    cofeeMaker.prepareCoffe(100);
                }

            } catch (InterruptedException e) {

            }

        }).start();

        Thread fillingThread = new Thread(() -> {
            while (true) {
                try {
                    cofeeMaker.fillTank(10);
                } catch (InterruptedException e) {

                }
            }
        });
        fillingThread.setDaemon(true);
        fillingThread.start();

    }
}
