package cz.sda.java8.coding_advancef.e24;

public class Basket {
    private final int CAPACITY = 10;
    private int itemsInBasket;

    public void addToBasket(){
        if(itemsInBasket+1==CAPACITY){
            throw new BasketFullExceptionRuntime();
        }
        itemsInBasket++;
    }
    public void removeFromBasket(){
        if(itemsInBasket-1<0){
            throw new BasketEmptyExceptionRuntime();
        }
        itemsInBasket--;
    }
}
