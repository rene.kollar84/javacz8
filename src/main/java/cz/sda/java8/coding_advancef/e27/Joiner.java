package cz.sda.java8.coding_advancef.e27;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Joiner<T> {
    private List<T> parameters;
    private String separator;
    //Design the Joiner<T> class, which in the constructor will take a separator (string)
    // and have a join() method
    //that allows you to specify any number of T-type objects.
    // This method will return a string joining all passed
    //elements by calling their toString() method and separating them with a separator.
    //eg. for Integers and separator "-" it should return: 1-2-3-4 ...
    public void join(T ... params) {
        parameters = List.of(params);

    }

    public void separator(String separator){
        this.separator = separator;
    }

    @Override
    public String toString() {
        Optional<String> reduce = parameters.stream().map(p -> p.toString()).reduce((p1, p2) -> p1 + separator + p2);
        return reduce.isPresent()?reduce.get():"";
    }
}
