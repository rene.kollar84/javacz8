package cz.sda.java8.coding_advancef.e6;

import java.util.Map;
import java.util.TreeMap;
//Create a method that accepts TreeMap and prints the first and last EntrySet in the console.
public class E6 {
    public static void main(String[] args) {
        TreeMap<String, Integer> map = new TreeMap<>();
        map.put("1",1);
        map.put("2",2);
        map.put("3",3);
        map.put("1",1);
        printTree(map);
    }

    static void printTree(TreeMap tree){
        Map.Entry firstEntry = tree.firstEntry();
        Map.Entry last = tree.lastEntry();
        System.out.println(firstEntry);
        System.out.println(last);
    }
}
