package cz.sda.java8.coding_advancef.p14;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//Implement the following functionalities based on 100,000 element arrays with randomly selected values:
//        1. return a list of unique items,
//        2. return a list of elements that have been repeated at least once in the generated array,
//        3. return a list of the 25 most frequently recurring items.
//        Implement a method that deduplicates items in the list. If a duplicate is found, it replaces it with a new
//        random value that did not occur before. Check if the method worked correctly by calling method number 2.
public class Main {

    public static void main(String[] args) {
        Integer[] randomA = generateRandomArray(100000);
//1.
        Set<Integer> sI = new HashSet<>();
        sI.addAll(Arrays.asList(randomA));
        System.out.println(sI.size());
//{1,1,2,1}

        Map<Integer, List<Integer>> collect = Stream.of(randomA).collect(Collectors.groupingBy(Integer::intValue));
        List<Integer> collect1 = collect.keySet().stream().collect(Collectors.toList());
        System.out.println(collect1.size());
        //{1:[1,1,1], 2:[2]

        //2.
        List<Integer> repeatedElements = Stream.of(randomA).collect(Collectors.groupingBy(Integer::intValue))
                .entrySet().stream()
                .filter(es -> es.getValue().size() > 1)
                .map(m -> m.getKey()).collect(Collectors.toList());

        //3
        List<Integer> top25 = Stream.of(randomA).collect(Collectors.groupingBy(Integer::intValue))
                .entrySet().stream()
                .sorted((ke1, ke2) -> ke1.getValue().size() - ke2.getValue().size())
                    .limit(25)
                .map(m -> m.getKey()).collect(Collectors.toList());
        System.out.println(top25);

        deduplicate(randomA);
    }

    private static Integer[] generateRandomArray(int count) {
        Integer[] randomA = new Integer[count];
        Random random = new Random();
        for (int i = 0; i < randomA.length; i++) {
            int rN = Math.abs(random.nextInt()) % count;
            randomA[i] = rN;
        }
        return randomA;
    }

    static void deduplicate(Integer[] randomA) {
        Object[] o = new Object[] {1, 2};
        Random random = new Random();
        Set<Integer> checkDuplicasSet = new TreeSet<>();
        int lastIndex = 0;
        int lastSize = checkDuplicasSet.size();
        while (checkDuplicasSet.size() < randomA.length) {

            Integer savedRandom = randomA[lastIndex];
            checkDuplicasSet.add(savedRandom);

            if (lastSize != checkDuplicasSet.size()) {
                lastSize = checkDuplicasSet.size();
                lastIndex++;
            } else {
                int rN = Math.abs(random.nextInt()) % randomA.length;
                checkDuplicasSet.add(rN);
                randomA[lastIndex] = rN;
                if (lastSize != checkDuplicasSet.size()) {
                    lastSize = checkDuplicasSet.size();
                    lastIndex++;
                }
            }

        }
        //check if all items in randomA is unique size of sI must be 100000
        Set<Integer> sI = new HashSet<>();
        sI.addAll(Arrays.asList(randomA));
        System.out.println(sI.size());

    }
}
