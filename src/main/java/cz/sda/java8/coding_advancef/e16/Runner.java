package cz.sda.java8.coding_advancef.e16;

public enum Runner {
    BEGINNER(5,Double.MAX_VALUE),
    INTERMEDIATE(3.3,5),
    ADVANCED(0,3.3);
    ;
    double minimumTimeMaraton;
    double maximumTimeMaraton;

    Runner(double minimumTimeMaraton, double maximumTimeMaraton) {
        this.minimumTimeMaraton = minimumTimeMaraton;
        this.maximumTimeMaraton = maximumTimeMaraton;
    }

    public static Runner getFitnessLevel(double time){
        if(time<3.3){
            return Runner.ADVANCED;
        }
        if(time<5){
            return Runner.INTERMEDIATE;
        }

        return BEGINNER;
    }
}
