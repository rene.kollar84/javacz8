package cz.sda.java8.coding_advancef.e4;

import java.util.*;

//Create a Storage class that will have a private Map field, a public constructor, and methods:
//        addToStorage(String key, String value) → adding elements to the storage
//        printValues(String key) → displaying all elements under a given key
//        findValues(String value) → displaying all keys that have a given value
//        The Storage class should allow you to store multiple values under one key.
public class Storage {
    private Map<String, List<String>> map = new HashMap<>();

    public void printValues(String inputKey) {
//        Set<String> strings = map.keySet();
//        int count = 0;
//        for (String key : strings) {
//            if (!key.equals(inputKey)){
//                continue;
//            }
//            String value = map.get(key);
//            String endLine = count++ < strings.size() - 1 ? "," : ".";
//            System.out.println("Key: " + key + "," + "Value: " + value + endLine);
//        }
        map.entrySet().stream()
                .filter(v -> v.getKey().equals(inputKey))
                .forEach((v) -> {
                    v.getValue().forEach(s -> System.out.println(v.getKey() + " " + s));
                });
    }

    public void addToStorage(String key, String value) {
        List<String> listOfValues = map.get(key);
        if (listOfValues == null){
            listOfValues = new ArrayList<>();
            map.put(key,listOfValues);
        }
        listOfValues.add(value);
    }

    public void findValues(String value){
        map.entrySet().stream().filter(k->k.getValue().contains(value)).forEach(System.out::println);
    }

    public static void main(String[] args) {
        Storage storage = new Storage();
        storage.addToStorage("Auto","Toyota");
        storage.addToStorage("Auto","Ford");
        storage.addToStorage("Auto","BWW");
        storage.addToStorage("Jidlo","Hruska");
        storage.addToStorage("Ovoce","Hruska");
        storage.findValues("Hruska");
        storage.printValues("Auto");
    }

}
