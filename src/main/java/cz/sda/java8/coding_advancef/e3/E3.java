package cz.sda.java8.coding_advancef.e3;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

//Create a method that takes the map as a parameter, where the key is string and the value number, and then
//prints each map element to the console in the format: Key: <k>, Value: <v>. There should be a comma at the
//end of every line except the last, and a period at the last.
//Example:
//Key: Java, Value: 18,
//Key: Python, Value: 1,
//…
//Key: PHP, Value: 0.
public class E3 {
    public static void printMapElements(Map<String, Integer> inputMap) {
        Set<String> strings = inputMap.keySet();
        int count = 0;
        for (String key : strings) {
            Integer value = inputMap.get(key);
            String endLine = count++ < strings.size() - 1 ? "," : ".";
            System.out.println("Key: " + key + "," + "Value: " + value + endLine);
        }
    }

    public static void main(String[] args) {

        HashMap<String, Integer> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("Java", 18);
        objectObjectHashMap.put("Python", 1);
        objectObjectHashMap.put("PHP", 0);
        Integer java = objectObjectHashMap.get("Java");
        printMapElements(objectObjectHashMap);
    }

}
