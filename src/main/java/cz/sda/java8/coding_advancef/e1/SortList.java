package cz.sda.java8.coding_advancef.e1;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortList {
    //Create a method that takes a string list as a parameter, then returns the list sorted alphabetically from Z to A.
    public static List<String> sortDesc(List<String> input) {
        //A....Z...a..z
        //a b c d -> dcba
        //Z, a -> a, Z

        return input.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    }
}
