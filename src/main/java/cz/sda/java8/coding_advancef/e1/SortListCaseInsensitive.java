package cz.sda.java8.coding_advancef.e1;

import java.util.List;
import java.util.stream.Collectors;

//Create a method that takes a string list as a parameter, then returns that list sorted alphabetically from Z to
//A case-insensitive.
public class SortListCaseInsensitive {
    public static List<String> sortDescSensitive(List<String> input) {

        //A....Z...a..z
        //a b c d -> dcba
        //Z, a -> Z, a

        //aaa,abs,kdkw
        // a,b,c,A
        // 62,63,64
        //ignoreCase
        //(upper)A, (upper)z -> A,Z -> A-Z
        // a -> A
        // Z -> Z
        // X -> X
        //62-63
        int b = "a".compareTo("b"); // return -1 if a<b , 0 if a==b, 1 if a>b


        return input.stream().sorted((s1, s2) -> s2.compareToIgnoreCase(s1)).collect(Collectors.toList());
    }
}
