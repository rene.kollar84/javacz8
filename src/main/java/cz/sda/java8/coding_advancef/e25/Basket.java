package cz.sda.java8.coding_advancef.e25;

public class Basket {
    private final int CAPACITY = 10;
    private int itemsInBasket;

    public void addToBasket() throws BasketFullException {
        if(itemsInBasket+1==CAPACITY){
            throw new BasketFullException();
        }
        itemsInBasket++;
    }
    public void removeFromBasket() throws BasketEmptyException {
        if(itemsInBasket-1<0){
            throw new BasketEmptyException();
        }
        itemsInBasket--;
    }
}
