package cz.sda.java8.coding_advancef.e32;

import cz.sda.java8.jf.oop.Car;

import java.io.*;
import java.util.List;

public class CarSaver {
    //Write a program that will be able to save the list of items (e.g. cars)
    // to a file in such a format that it can also read
    //this file and create a new list of items and write it to the console.
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //String registrationNumber, String manufacturer, String color, int yearOfConstruction, int kmDriven) {
        List<Car> cars = List.of(new Car("1", "toyota", "cervena", 2020), new Car("1", "bmw", "cerna", 2020));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
       // FileOutputStream out = new FileOutputStream("any.file");
        ObjectOutputStream dataOutputStream = new ObjectOutputStream(out);

        dataOutputStream.writeObject(cars);
        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
       // FileInputStream in = new FileInputStream("anyfile");

        ObjectInputStream objectInputStream = new ObjectInputStream(in);
        List<Car> carsReaded = (List<Car>) objectInputStream.readObject();
        carsReaded.forEach(System.out::println);


    }
}
