package cz.sda.java8.coding_advancef.e37;

import cz.sda.java8.coding_advancef.e36.ThreadPlaygroundRunnable;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    //Create a class containing the standard static method main and a variable of type Executor and using the
    //factory method newFixedThreadPool of the Executors class to create a pool of 2 executors.
    //In iteration, add 10 ThreadPlaygroundRunnable objects from the previous task to the executor. Use any
    //string and current iteration number as the name.
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; i++) {
            ThreadPlaygroundRunnable threadPlaygroundRunnable = new ThreadPlaygroundRunnable(String.valueOf(i));
            executorService.submit(threadPlaygroundRunnable);

        }

    }
}
