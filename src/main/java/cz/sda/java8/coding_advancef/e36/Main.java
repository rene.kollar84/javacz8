package cz.sda.java8.coding_advancef.e36;

public class Main {
    public static void main(String[] args) {
        Thread myThread = new Thread(new ThreadPlaygroundRunnable("Jakub"));
        Thread myThread1 = new Thread(new ThreadPlaygroundRunnable("Pavel"));
        Thread myThread2 = new Thread(new ThreadPlaygroundRunnable("Jan"));
        myThread.start();
        myThread1.start();
        myThread2.start();
    }
}
