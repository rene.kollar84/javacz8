package cz.sda.java8.coding_advancef.e28;

import java.util.ArrayList;
import java.util.List;

public class ExtendedArrayList<E> extends ArrayList<E> {

    public List<E> getEveryNthElement(int startIndex, int skip){
        ArrayList<E> result = new ArrayList<>();
        for (int i = startIndex; i < this.size(); i += skip +1) {
            result.add(this.get(i));
        }
        return result;
    }

}
///Extend the ArrayList<E> class by implementing the getEveryNthElement() method. This method returns a list
//        of elements and takes two parameters: the element index from which it starts and a number specifying which
//        element to choose.
//        For the list: [a, b, c, d, e, f, g] and parameters: startIndex = 2 and skip = 3 it should return [c, g]
