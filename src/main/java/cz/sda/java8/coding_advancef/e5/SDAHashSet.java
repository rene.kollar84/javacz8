package cz.sda.java8.coding_advancef.e5;

public class SDAHashSet<E> implements HashSet<E>{

    java.util.HashSet<E> inner = new java.util.HashSet();
    @Override
    public boolean add(E item) {
        return inner.add(item);
    }

    @Override
    public boolean remove(E item) {
        return inner.remove(item);
    }

    @Override
    public int size() {
        return inner.size();
    }

    @Override
    public boolean contains(E item) {
        return inner.contains(item);
    }

    @Override
    public void clear() {
        inner.clear();
    }
}
