package cz.sda.java8.coding_advancef.e5;

public interface HashSet<E> {
    boolean add(E item);

    boolean remove(E item);

    int size();

    boolean contains(E item);

    void clear();

}
