package cz.sda.java8.coding_advancef.e7;

public class FullMagExeption extends RuntimeException{

    public FullMagExeption(String message) {
        super(message);
    }
}
