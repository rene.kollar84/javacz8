package cz.sda.java8.coding_advancef.e7;

public class Magazine {

    private int size;
    private int ammount;

    public Magazine(int size) {
        this.size = size;
    }

    public void loadBullet() {
        if (ammount < size) {
            this.ammount++;
        } else {
            throw new FullMagExeption("Vkladas " + (ammount + 1) + " naboj do zasobnika o kapacite " + size);
        }
    }

    public boolean isLoaded() {
        return ammount > 0;
    }

    public void shot() {
        if (ammount > 0) {
            ammount--;
            System.out.println("BOOOM");
        } else {
            System.out.println("Cvak");
        }
    }
}
