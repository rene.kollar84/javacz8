package cz.sda.java8.coding_advancef.e7;

public class Weapon {

    Magazine magazine = new Magazine(17);

    public void addBullet() {
        magazine.loadBullet();
    }
    public void shoot() {
        magazine.shot();
    }

    public static void main(String[] args) {

        Weapon glock = new Weapon();
        glock.shoot();
        glock.addBullet();
        glock.shoot();
        try{
            for (int i = 0; i <18 ; i++) {
                glock.addBullet();
            }


        }catch(FullMagExeption e){
            System.out.println(e.getMessage());
        }
    }

}
