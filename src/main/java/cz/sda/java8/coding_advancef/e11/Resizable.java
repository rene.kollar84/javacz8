package cz.sda.java8.coding_advancef.e11;

public interface Resizable {

    void resize(double factor);
}
