package cz.sda.java8.coding_advancef.e15;

//Create a Car enum class with FERRARI, PORSCHE, MERCEDES, BMW, OPEL, FIAT, TOYOTA etc. constants.
//Each vehicle has its own parameters, e.g. price, power, etc. Enum should contain boolean isPremium() and
//boolean isRegular() methods. The isPremium() method should return the opposite result to the call of the
//isRegular() method.
//In addition, the boolean isFasterThan() method should be declared and implemented as part of the enum
//class. This method should accept the Car type object and display information that the indicated vehicle is
//faster or not than the vehicle provided in the argument. To do this, use the compareTo() method.
public enum CarEnum {
    FERRARI(true,3000000,600),
    PORSHE(true,2000000,400),
    OPEL(false,400000,90),
    TOYOTA(true,700000,130);

    private final boolean premium;
    private final double price;
    private final double power;

    CarEnum(boolean premium, double price, double power) {
        this.premium = premium;
        this.price = price;
        this.power = power;
    }

    public boolean isPremium(){
        return premium;
    }

    public boolean isRegular(){
        return !premium;
    }

    public boolean isFasterThan(CarEnum ce){
        return power > ce.power;
    }
}
