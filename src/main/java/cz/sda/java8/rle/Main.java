package cz.sda.java8.rle;

public class Main {
    public static void main(String[] args) {
        EncoderDecoder encoderDecoder = new EncoderDecoder();
        String input = encoderDecoder.encode("aaaaaaaaaaaaaaaahbbbstaaa");
        System.out.println(input);
        System.out.println(encoderDecoder.decode(input));

    }
}
