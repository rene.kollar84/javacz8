package cz.sda.java8.rle;

public class EncoderDecoder {
    public String encode(String in) {
        // aaab5ccc
        //# je escape
        if(in.equals("")){
            return "";
        }
        String ret = "";
        char prev = in.charAt(0);
        int count = 1;
        for (int i = 1; i < in.length(); i++) {
            char c = in.charAt(i);
            if (prev == c) {
                count++;
                if (i < in.length() - 1) {
                    continue;
                }
            }
            if(Character.isDigit(prev) || prev=='#'){
                ret+='#';
            }
            ret += prev;
            ret += count;
            prev = c;
            count = 1;
            if(i==in.length()-1){
                if(Character.isDigit(c) || c=='#'){
                    ret+='#';
                }
                ret+=c;
                ret+=1;
            }

        }
        return ret;
    }

    public String decode(String in) {
        if(in.equals("")){
            return "";
        }
        String ret = "";
        char prev = in.charAt(0);
        String count = "0";
        boolean isEscapeMode = false;
        for (int i = 1; i < in.length(); i++) {
            char c = in.charAt(i);
            if(c=='#'){
                isEscapeMode=true;
                continue;
            }
            if (Character.isDigit(c) && !isEscapeMode) {
                count += c;
                if (i < in.length() - 1) {
                    continue;
                }
            }
            int countInt = Integer.parseInt(count);
            for (int j = 0; j < countInt; j++) {
                ret += prev;
            }
            prev = c;
            count = "0";
            isEscapeMode=false;
        }
        return ret;
    }
}
