package cz.sda.java8.jf.oop;

public class CarMain {
    public static void main(String[] args) {
        //3 new objects of Car
        Car car1 = new Car("8B2 1241", "Audi", "Blue", 1999, 123456);
        Car car2 = new Car("4A5 6325", "Skoda", "White", 2021, 64545);
        Car car3 = new Car("4T2 8476", "BWM", "Yellow", 2022);

        //Calling addKmDriven() method
        car1.addKmDriven(5000);
        car2.addKmDriven(15000);
        car3.addKmDriven(66460);

        //Calling static method from CarUtil - does not require instance (object) of CarUtil
        CarUtil.printCarAge(car1, 2022);

        //Refresh:
        //Cars and carsThroughIndex have the same values/references to cars1, cars2, cars3
        Car[] cars = new Car[]{car1, car2, car3};

        float avgAge = CarUtil.avgCarAge(cars);
        System.out.println("Average age of my cars is: " + avgAge);
        System.out.println();

        CarUtil.printCarInformation(cars);
        CarUtil.printCarInformation(car1, car2, car3);

        //Different way to create the same Array as
        //Car[] cars = new Car[]{car1, car2, car3};
        Car[] carsThroughIndex = new Car[3];
        carsThroughIndex[0] = car1;
        carsThroughIndex[1] = car2;
        carsThroughIndex[2] = car3;

        //Both will print the same values - they are equivalent
        System.out.println( cars[0].getManufacturer() );
        System.out.println( car1.getManufacturer() );

        //Both will print the same values - they are equivalent
        System.out.println( cars[1].getManufacturer() );
        System.out.println( car2.getManufacturer() );

        //Both will print the same values - they are equivalent
        System.out.println( cars[2].getManufacturer() );
        System.out.println( car3.getManufacturer() );

    }
}
