package cz.sda.java8.jf.oop;

public class Teacher {
    //Teacher can hold information about all its students
    private Student[] students;

    public Teacher(Student[] students) {
        this.students = students;
    }
}
