package cz.sda.java8.jf.oop;

import java.io.Serializable;

public class Car implements Serializable {
    //Fields
    private String registrationNumber;
    private String manufacturer;
    private String color;
    private int yearOfConstruction;
    private int kmDriven;

    //Constructor that sets values for all fields
    public Car(String registrationNumber, String manufacturer, String color, int yearOfConstruction, int kmDriven) {
        this.registrationNumber = registrationNumber;
        this.manufacturer = manufacturer;
        this.color = color;
        this.yearOfConstruction = yearOfConstruction;
        this.kmDriven = kmDriven;
    }

    //Constructor calling the other constructor and giving kmDriven as 0 every time
    public Car(String registrationNumber, String manufacturer, String color, int yearOfConstruction) {
        this(registrationNumber, manufacturer, color, yearOfConstruction, 0);
    }

    //Getters
    public String getManufacturer() {
        return manufacturer;
    }

    public int getYearOfConstruction() {
        return yearOfConstruction;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public String getColor() {
        return color;
    }

    //Currently unused getters - should not be included
    public int getKmDriven() {
        return kmDriven;
    }

    /**
     * Adds kilometers driven to the car
     * @param kmDriven how many kilometers should be added
     */
    public void addKmDriven(int kmDriven){
        this.kmDriven += kmDriven;
    }

    @Override
    public String toString() {
        return "Car{" +
                "registrationNumber='" + registrationNumber + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", color='" + color + '\'' +
                ", yearOfConstruction=" + yearOfConstruction +
                ", kmDriven=" + kmDriven +
                '}';
    }
}
