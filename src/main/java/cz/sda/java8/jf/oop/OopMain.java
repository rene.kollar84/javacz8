package cz.sda.java8.jf.oop;

import java.util.Arrays;

public class OopMain {
    public static void main(String[] args) {
        //String object saved in variable with name "day"
        //Use literals but this is just showcase
        String day = new String("Monday");

        //2 new OBJECTS student1 and student2
        Student student1 = new Student("Jiri", "Novotny", 1987);
        Student student2 = new Student("Petr", "Novak", 1985);

        //calling method getName from student2 and saving it in String variable student2Name
        String student2Name = student2.getName();
        //Changing student2 name to "Honza"
        student2.setName("Honza");

        //Calling static methods from Math class - no need to create new object
        int a = Math.max(5, 6);
        float b = Math.nextUp(7);

        //Calling static methods from Arrays class
        Arrays.asList(1,2,3,4,5);
    }
}
