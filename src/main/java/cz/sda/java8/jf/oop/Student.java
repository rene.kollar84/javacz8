package cz.sda.java8.jf.oop;

public class Student {
    //Fields
    private String name;
    private String surname;
    private int yearBorn;


    //Constructor
    public Student(String name, String surname, int yearBorn) {
        this.name = name;
        this.surname = surname;
        this.yearBorn = yearBorn;
    }


    //Getters
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYearBorn() {
        return yearBorn;
    }


    //Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYearBorn(int yearBorn) {
        this.yearBorn = yearBorn;
    }
}
