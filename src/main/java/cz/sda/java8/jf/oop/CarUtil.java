package cz.sda.java8.jf.oop;

public class CarUtil {

    /**
     * Method that takes car and year and returns how old was the car in the set year (it expects current year in this
     * case but any year can be given
     * @param car object of Car
     * @param currentYear any year but current year expected
     */
    public static void printCarAge(Car car, int currentYear){
        int age = currentYear - car.getYearOfConstruction();
        System.out.println(age);
    }

    /**
     * Method that counts average age of provided cars
     * @param cars array of Car objects
     * @return average year
     */
    public static float avgCarAge(Car[] cars){
        float avgAge;
        float ageSum = 0f;

        for (Car car : cars) {
            ageSum += 2022 - car.getYearOfConstruction();
        }

        avgAge = ageSum / cars.length;

        return avgAge;
    }

    /**
     * Prints information about provided cars
     * @param cars Either vararg of Cars or array
     */
    public static void printCarInformation(Car... cars){
        for (Car car : cars) {
            System.out.println(car.getColor() + " " + car.getManufacturer() +
                    " from year " + car.getYearOfConstruction() + ", " +
                    car.getRegistrationNumber());
        }
    }


}
