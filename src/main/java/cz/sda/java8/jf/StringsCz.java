package cz.sda.java8.jf;

import java.util.Scanner;

public class StringsCz {
    public static void main(String[] args) {

        //Two ways to initialize String
        String name = "Jakub"; //literal ... uses String pool
        String nameAlt = new String(name); //creates new value in memory

        //Side note: similar use of constructor that you know
        Scanner scanner = new Scanner(System.in);

        //---------------------------------------------------------------------------------

        //Concat of strings - 3 different examples
        String name2 = "Jakub";
        name2 = name2 + " Kobelar";
        name2 += " says";
        name2 = name2.concat(" hi!");

        //---------------------------------------------------------------------------------

        System.out.println("Different comparisons:");
        //Literal != Constructor with the same value
        String stringLiteral = "Jakub";
        String stringFromConstructor = new String(stringLiteral);

        System.out.println(stringFromConstructor == stringLiteral); //FALSE
        System.out.println(stringFromConstructor.equals(stringLiteral)); //TRUE

        //Literal == literal with the same value
        String lit1 = "Jakub";
        String lit2 = "Jakub";

        System.out.println(lit1 == lit2); //TRUE
        System.out.println(lit1.equals(lit2)); //TRUE

        //Interning
        String internedString = new String("This String is interned").intern();
        String alsoInternedString = new String(internedString).intern();

        System.out.println(internedString == alsoInternedString); //TRUE
        System.out.println(internedString.equals(alsoInternedString)); //TRUE
        System.out.println();

        //---------------------------------------------------------------------------------
        //2 different things:
        System.out.println("Is empty:");
        String nullString; //IS NULL!
        String emptyString = ""; //Is just empty!
        System.out.println(emptyString.isEmpty());
        System.out.println();

        //Lower and upper case
        System.out.println("Play with lower and upper case:");
        String shorterString = "A Java array is a special type of object variable that serves as a container to hold structured data of one type.";
        System.out.println("Everything normal:");
        System.out.println(shorterString);
        System.out.println("Everything lower case:");
        System.out.println(shorterString.toLowerCase());
        System.out.println("Everything upper case:");
        System.out.println(shorterString.toUpperCase());

        System.out.println("Split String by spaces:");
        String[] myWords = shorterString.split(" ");

        for (String word : myWords) {
            System.out.println(word);
        }
        System.out.println();
        System.out.println();

        //Character at index 2 is 'J'
        System.out.println(shorterString.charAt(2));
        //Omits "A Java"
        System.out.println(shorterString.substring(8));
        System.out.println(shorterString.contains("Java")); //TRUE
        //Replace all 'a' characters with '#'
        System.out.println(shorterString.replace('a', '#'));

        //Example of trim() - removes white characters
        String withWhiteChars = "      Hello!     ";
        System.out.println(withWhiteChars);
        System.out.println(withWhiteChars.trim());

        System.out.println();

        String ourString = "A Java array is a special type of object variable that serves as a container to hold structured data of one type. \n" +
                "We can refer to its individual elements with index.\n" +
                "For example, if we wanted to store 1000 names in our application, we wouldn't have to declare 1000 variables of the String type, but only declare one 1000-element array variable that stores strings of the String type.\n" +
                "To declare an array, you need to specify the type of data stored in the array and specify its size. \n" +
                "The size of the array should be constant, so when declaring an array, the size must be specified or the elements must be explicitly declared so that the compiler can calculate the length of the array itself.";

        System.out.println(ourString);
        //Splits lines by /n and puts them in Stream (usefull, no need to understand yet)
        System.out.println();

        ourString.lines().forEach(line -> System.out.print(line + "ENDOFTHELINE"));
        System.out.println();
        System.out.println();


        //---------------------------------------------------------------------------------

        //Problematic characters - '"' and '\' require additional '\' in front:
        String example = "\" \\";

        //---------------------------------------------------------------------------------

        //Scanner examples
        Scanner userInput = new Scanner(System.in);

        System.out.println("Input String:");
        String stringInput = userInput.nextLine();
        System.out.println("Your value: " + stringInput);

        System.out.println("Input boolean:");
        boolean booleanInput = userInput.nextBoolean();
        System.out.println("Your value: " + booleanInput);

        System.out.println("Input int:");
        int intInput = userInput.nextInt();
        System.out.println("Your value: " + intInput);

        System.out.println("Input float:");
        float floatInput = userInput.nextFloat();
        System.out.println("Your value: " + floatInput);

    }
}
