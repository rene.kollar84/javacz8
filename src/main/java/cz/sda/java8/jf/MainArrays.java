package cz.sda.java8.jf;

public class MainArrays {
    public static void main(String[] args) {
        //Different types
        int[] myNumbers = new int[4];
        boolean[] myBooleans = new boolean[4];

        //Declaration array size 5
        String[] myNames = new String[5]; //Values will be null!!!
        String[] names = new String[]{"Jiri Princ", "Antonin Barta", "Zdenek Jandura", "Josef Kotyk", "Radek Lacina"};

        //Shows access to the values through index
        int[] numbers = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println(numbers[5]);
        System.out.println(names[3]);
        System.out.println();

        for (int index = 0; index < names.length; index++) {
            System.out.println(names[index]);
        }
        System.out.println();

        for (String tempName : names) {
            System.out.println(tempName);
        }


        //Shows more complicated work with indexes
        String name = "Jakub";
        String city = "Brno";

        char[] myChars = new char[name.length() + city.length()];

        for (int i = 0; i < name.length(); i++) {
            myChars[i] = name.toCharArray()[i];
        }
        for (int i = name.length(); i < name.length() + city.length(); i++) {
            myChars[i] = city.toCharArray()[i - name.length()];
        }
        for (char c : myChars) {
            System.out.println(c);
        }

        //Two dimensional array with values
        int[] numbers1 = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] numbers2 = new int[]{0, 10, 20, 30, 40, 50, 60, 70, 80, 90};
        int[] numbers3 = new int[]{10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
        int[] numbers4 = new int[]{50, 51, 52, 53, 54, 55, 56, 57, 58, 59};
        int[][] table = new int[4][];
        table[0] = numbers1;
        table[1] = numbers2;
        table[2] = numbers3;
        table[3] = numbers4;

        //Accessing value at index 1;9 -> numbers2:lastValue
        System.out.println(table[1][9]);

        //Iterating throw fori
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }

        //Iterating throw foreach
        for (int[] line : table) {
            for (int number : line) {
                System.out.println(number);
            }
        }

        //All values are their default - primitive data types -> zeros
        int[][] tableWithZeroes = new int[8][6];
        int[][][] threeDSpace = new int[100][100][100];

        System.out.println();

        //Small example
        loginGenerationExample();


        //Play with indexes
        String[] seasons = new String[]{"Spring", "Summer", "Automn", "Winter"};
        for (int i = 0; i < 50; i++) {
//            System.out.println(seasons[i%4]);
        }


    }

    /**
     * Shows an example where several different logins are generated - imagine having input(namesForLogins) with millions of values
     */
    private static void loginGenerationExample() {
        String[] namesForLogins = new String[]{
                "Jiri Princ", "Antonin Barta", "Zdenek Jandura", "Josef Kotyk", "Radek Lacina",
                "Lukas Dulik", "Miroslav Kroupa", "Milan Chrastek", "Karel Stoural", "Pavel Novotny",
                "Vladimir Jasek", "Bruno Kubacka", "Dominik Vasek", "Jan Blaha", "Martin Nikodem",
                "Daniel Noha", "Rostislav Hakr", "Vilem Stach", "Volodymyr Benko", "Michal Abraham", "Jakub Kobelar",
                "Andrej Mazak", "Nikola Zvonarova"};

        String[] logins = new String[namesForLogins.length];

        for (int index = 0; index < namesForLogins.length; index++) {
            String login = "";

            String[] nameAndSurname = namesForLogins[index].split(" ");

            String personName = nameAndSurname[0];
            login += personName.toLowerCase().charAt(0);

            String surname = nameAndSurname[1];
            login += surname.toLowerCase();

            logins[index] = login;
        }

        for (int i = 0; i < logins.length; i++) {
            System.out.println(namesForLogins[i] + " has a login of: " + logins[i]);
        }
    }

}
