package cz.sda.java8.jf;

import java.util.Scanner;

public class MainLoops {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(i + 1 + ".Hello World!");
        }

        for (int i = 9; i >= 0; i--) {
            System.out.println(i + 1 + ".Hello World!");
        }


        String[] mesice = {"Leden", "Unor", "Brezen"};

        for (String m : mesice) {
            System.out.println(m + " ");
        }


        int j = 5;
        while (j < 5) {
            j++;
            System.out.println(j);
        }

        Scanner in = new Scanner(System.in);
        int nInt;
        do {
            System.out.println("Zadejte cislo k testu");
            nInt = in.nextInt();
            if (nInt % 2 == 0) {
                System.out.println("Cislo " + nInt + " je sude");
            } else {
                System.out.println("Cislo " + nInt + " je liche");
            }
        } while (nInt > 0);

        //break example
        for (int i = 0; i < 10000; i++) {
            if (i > 5) {
                break;
            }
            System.out.println(i);
        }

        long start = System.currentTimeMillis();
        while (true) {
            long end = System.currentTimeMillis();
            if (end - start > 1) {
                break;
            }
            System.out.println("any");
        }

        for (int i = 0; i < 20; i++) {
            if (i % 2 != 0) {//jedna se o liche cislo
                continue;
            }
//            .
//            .
//            .
            System.out.println(i);
        }

    }
}
