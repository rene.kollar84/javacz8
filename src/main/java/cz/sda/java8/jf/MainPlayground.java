package cz.sda.java8.jf;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainPlayground {
    public static void main(String[] args) {
        char[] allLetters = generateArrayWithAllLowerCaseLetters();
        int[] generatedNumbers = generateArrayZeroToNumber(100);
        int[] generatedRandomNumbers = generateNNumbersToVValue(100, 1000);

        printAllLetters();
        printAllLettersWithRegexFilter();
        printCharsWithValues();
    }

    /**
     * Method generating random n number of ints between 0 and v
     * @param n number of values
     * @param v values max
     */
    private static int[] generateNNumbersToVValue(int n, int v) {
        int[] randomNumbers = new int[n];
        for (int i = 0; i < randomNumbers.length; i++) {
            Random random = new Random();
            int randomNumber = random.nextInt(v);

            randomNumbers[i] = randomNumber;

//            System.out.print(randomNumber + " ");
        }
        return randomNumbers;
    }

    /**
     * Prints numbers and letters with their Char value showed
     */
    private static void printCharsWithValues() {
        for (char c = 0; c < 1000; c++) {
            if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9')) {
                int cValueRepresentation = (int) c;
                System.out.println(c + " has value of " + cValueRepresentation);
            }
        }
        System.out.println();
    }

    /**
     * Prints all letters and filters unwanted characters with regex
     */
    private static void printAllLettersWithRegexFilter() {
        Pattern pattern = Pattern.compile("[A-Za-z]");
        for(char c = 'A'; c <= 'z'; c++){
            Matcher matcher = pattern.matcher(c + "");
            if (matcher.matches()){
                System.out.print(c + " ");
            }
        }
        System.out.println();
    }

    /**
     * Prints all letters using simple for cycle
     */
    private static void printAllLetters() {
        for(char c = 'A'; c <= 'z'; c++){
            if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) {
                System.out.print(c + " ");
            }
        }
        System.out.println();
    }

    /**
     * Generates numbers from 0 to number given through parameter
     * @param number how many numbers should generate (also size of the array)
     */
    private static int[] generateArrayZeroToNumber(int number) {
        int[] numbers = new int[number];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = i;
        }
        return numbers;
    }

    /**
     * Generates and returns arrays of all lower case letters
     */
    private static char[] generateArrayWithAllLowerCaseLetters() {
        char[] letters = new char[26];

        char currentChar = 'a';
        for (int i = 0; i < letters.length; i++) {
            letters[i] = currentChar++;
        }
        return letters;
    }
}
