package cz.sda.java8.jf;

//Zaznamy:
//https://drive.google.com/drive/folders/1oOvpYWSU4_EguISYBLW6gW79PBnJNYoW?usp=sharing
public class MainCz {

    private static int count = 5;

    private static final int constantVariable = 3;

    private static byte color = 5;

    public static void main(String[] args) {
        if (true) {
            int blockVariable = 10;
        }

        System.out.println("Hello\tworld\n2");
        float a = 999999999999999999999f;
        float b = 0.0000000000000000001f;

        //equivalent
        a = a + 50;
        a += 50;

        int restOfDivision = 7 % 2;
        int division = 7 / 2;
        float aaa = 7f / 2;
        System.out.println(restOfDivision);
        String stringA = "ahoj";
        stringA = "ahoj2";
        System.out.println(stringA);
        System.out.println(a + b);

        // System.out.println(blockVariable); outside of scope
        System.out.println("Hello world");
        System.out.println(count);

        boolean b1 = 6 == 7;
        System.out.println(b1);
        System.out.println(6 != 7);

        System.out.println(6 > 7);//false

        boolean vyznamenani = true;
        boolean mamDostPenez = false;
        boolean muzuKoupitKolo = vyznamenani && mamDostPenez;

        if (vyznamenani && mamDostPenez) {
            //koupim kolo
        } else {
            //koupim rohhlik
        }
        // 00000001
        // 00000010
        // 00000000
        byte aBit = 1 & 2;
        // 00000001
        // 00000100
        byte aShift = 1 << 2;

        int k = 5;
        double l = 3;
        double v = k + l;

        char zn = 'a';
        System.out.println((int)'|');
    }

}
