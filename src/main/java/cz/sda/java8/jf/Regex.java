package cz.sda.java8.jf;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    public static void main(String[] args) {
        //Scanner for user's input into console (System.in)
        Scanner userInput = new Scanner(System.in);

        //EXAMPLE WITH PID
        while (true) {
            System.out.println("Zadejte rodne cislo:");

            String personalIdNumber = userInput.nextLine();
            Pattern personalIdNumberPattern = Pattern.compile("[0-9]{6}/[0-9]{3,4}");
            Matcher matcher = personalIdNumberPattern.matcher(personalIdNumber);

            if (matcher.matches()) {
                System.out.println("Correct format!");
                break;
            } else {
                System.out.println("Incorrect format!");
            }
        }

        //EXAMPLE WITH NAME
        while (true) {
            System.out.println("Zadejte jmeno a prijmeni:");

            String name = userInput.nextLine();
            Pattern personalIdNumberPattern = Pattern.compile("([A-Z][a-z]+ )+([A-Z][a-z]+)");
            Matcher matcher = personalIdNumberPattern.matcher(name);

            if (matcher.matches()) {
                System.out.println("Correct format!");
                break;
            } else {
                System.out.println("Incorrect format!");
            }
        }
    }

}
