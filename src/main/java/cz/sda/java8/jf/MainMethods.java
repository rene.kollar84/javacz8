package cz.sda.java8.jf;

public class MainMethods {

    public static void main(String[] args) {
        //Two different cases of usage of the same method - just printing all String values in String[]
        String[] names = new String[]{"Jakub","Marcel","Martin"};
        stringsPrinter(names);

        String[] carsManufacturers = new String[]{"Mercedes", "BMW", "Ferrari", "Audi"};
        stringsPrinter(carsManufacturers);


        //Two different uses of sumOfTwoNumbers methods. Also shows method overloading
        float sum1 = sumOfTwoNumbers(5, 6);
        System.out.println(sum1);

        float sum2 = sumOfTwoNumbers(4.0f, 5.0f);
        System.out.println(sum2);


        //Shows simple void non-parametric method
        for (int i = 0; i < 5; i++) {
            printAhoj();
        }


        //Shows behavior of methods with multiple returns
        System.out.println(isBiggerThan100(50));
        System.out.println(isBiggerThan100(150));
    }

    /**
     * Sums two number together
     * @param x first value
     * @param y second value
     * @return sum of x and y
     */
    private static float sumOfTwoNumbers(float x, float y) {
        float result = 0;
        result = x + y;

        return result;
    }

    /**
     * Sums two number together
     * @param firstNumber x
     * @param secondNumber y
     * @return sum of x and y
     */
    public static float sumOfTwoNumbers(int firstNumber, int secondNumber){
        int result = 0;
        result = firstNumber + secondNumber;

        return result;
    }

    /**
     * Prints every String in String[]
     * @param strings
     */
    public static void stringsPrinter(String[] strings){
        for(String s : strings){
            System.out.println(s);
        }
    }

    /**
     * Prints "Ahoj"
     */
    public static void printAhoj(){
        System.out.println("Ahoj");
    }

    /**
     * Decides if input number is bigger than 100
     * @param number your number
     * @return if number is bigger than 100
     */
    private static boolean isBiggerThan100(int number){
        if (number > 100){
            return true;
        } else {
            return false;
        }
    }


}
