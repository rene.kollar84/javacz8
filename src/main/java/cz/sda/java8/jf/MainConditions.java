package cz.sda.java8.jf;

public class MainConditions {
    public static void main(String[] args) {
        int znamka = 5;
        if (znamka > 5) {
            System.out.println("Mimo rozzsah klasifikace");
        }

        int points = 99;
        //spatne
//        if (points > 86) {
//            System.out.println("Vyborne");
//        }
//        if (points > 68) {
//            System.out.println("Chvalitebne");
//        }
//        if (points > 51) {
//            System.out.println("Dobre");
//        } else {
//            System.out.println("Nedostatecne");
//        }

        if (points > 86) {
            System.out.println("Vyborne");
        } else if (points > 68) {
            System.out.println("Chvalitebne");
        } else if (points > 51) {
            System.out.println("Dobre");
        } else {
            System.out.println("Nedostatecne");
        }

        boolean autoNaSklade = true;
        boolean mamDostPenez = true;
        if (autoNaSklade && mamDostPenez) {
            System.out.println("kupuju auto");
        }
        //equal code as below
        if (autoNaSklade) {
            if (mamDostPenez) {
                System.out.println("kupuju auto");
            }
        }

        int mouth = 5;
        switch (mouth) {
            case 1:
                System.out.println("Leden");
                break;
            case 2:
                System.out.println("Unor");//cokoliv je to koment
//                .
//                .
//                .
            case 12:
                System.out.println("Prosinec");
            default:
                System.out.println("Meznamy mesic");
        }

        int a = 6;
        if (a % 2 == 0) {
            System.out.printf(" Cislo %d je sude", a);
        } else {
            System.out.printf(" Cislo %d je liche", a);
        }


        String s = "Cislo " + a + " je sude";
    }
}
