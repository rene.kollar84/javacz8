package cz.sda.java8.jf;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TimeAndDateMain {
    public static void main(String[] args) {
        localTime();
        localDate();
        localDateTime();
        //fun(); //Watch out, this method can cause performance issues on your computer while running - don't let it run in the background
    }

    private static void localDateTime() {
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime);

        LocalDateTime customLocalDateTime = LocalDateTime.of(2000, 1, 1, 0, 0);
        System.out.println("Konec sveta mel nastat take uz: " + customLocalDateTime);

        LocalDate dateNow = LocalDate.now();
        LocalTime timeNow = LocalTime.now();

        LocalDateTime connectedDateAndTime = LocalDateTime.of(dateNow, timeNow);
        System.out.println(connectedDateAndTime);
    }

    private static void localDate() {
        LocalDate dateNow = LocalDate.now();
        System.out.println("Dnes mame datum: " + dateNow);

        LocalDate customDate = LocalDate.of(2012, 12, 21);
        System.out.println("Konec sveta mel nastat: " + customDate);

        System.out.println("Tedy v roce: " + customDate.getYear());

        System.out.println("Jiz ubehlo: " + dateNow.getDayOfYear() + " dni v roce " + dateNow.getYear());

        System.out.println("Today is: " + dateNow.getDayOfWeek());

        System.out.println("----------------------------");
        System.out.println("----------------------------");
        System.out.println();
    }

    private static void localTime() {
        LocalTime timeNow = LocalTime.now();
        LocalTime customTime = LocalTime.of(12, 0, 0);

        System.out.println(timeNow);
        System.out.println(customTime);

        System.out.println("Same in hh:mm: " + timeNow.withSecond(0).withNano(0));

        LocalTime shorterTimeNow = LocalTime.now().withSecond(0).withNano(0);
        System.out.println("In an hour it's going to be: " + shorterTimeNow.plusHours(1));
        System.out.println("In 5 hours it's going to be: " + shorterTimeNow.plusHours(5));

        System.out.println("In 15 minutes it's going to be: " + shorterTimeNow.plusMinutes(15));

        //Same exists for minutes and seconds
        System.out.println("----------------------------");
        System.out.println(shorterTimeNow.getHour() + " hours have passed today");
        System.out.println(timeNow.toSecondOfDay() + " seconds have passed today");


        System.out.println("----------------------------");
        LocalTime noon = LocalTime.of(12, 0);

        if (shorterTimeNow.isBefore(noon)) {
            System.out.println("Obed jeste ceka!");
        } else if (shorterTimeNow.isAfter(noon) && shorterTimeNow.isBefore(noon.plusMinutes(30))) {
            System.out.println("Ted je cas na obed!");
        } else {
            System.out.println("Obed uz mas za sebou!");
        }

        System.out.println("----------------------------");
        System.out.println("----------------------------");
        System.out.println();
    }

    /**
     * Function that prints current time every second
     * It also adds :00 when whole minute happens (otherwise it would be omitted)
     */
    private static void fun() {
        LocalTime timeNow = LocalTime.now();
        while (true){
            if(LocalTime.now().withNano(0).isAfter(timeNow)){
                timeNow = LocalTime.now().withNano(0);

                if(timeNow.getSecond() == 0){
                    System.out.println("Prave je: " + timeNow + ":00");
                } else {
                    System.out.println("Prave je: " + timeNow);
                }
            }
        }
    }
}
