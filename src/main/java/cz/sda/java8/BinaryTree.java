package cz.sda.java8;

import java.util.Arrays;
import java.util.function.Predicate;

public class BinaryTree {
    public static void main(String[] args) {

        BinaryTree binaryTree = new BinaryTree();
        binaryTree.root = new Node(8);
        binaryTree.root.left = new Node(3);
        binaryTree.root.right = new Node(10);
        binaryTree.root.left.left = new Node(1);
        binaryTree.root.left.right = new Node(4);

        Node node = binaryTree.binarySearch(4);
        System.out.println(node);
        BinaryTree fromArray = BinaryTree.getFromArray(new int[]{5, 6, 3, 2, 4, 9, 7});
        Node node1 = fromArray.binarySearch(2);
        System.out.println(node1);
        Node node2 = fromArray.binarySearch(9);
        System.out.println(node2);
        Node node3 = fromArray.binarySearch(6);
        System.out.println(node3);
    }

    //DU
    public static BinaryTree getFromArray(int[] array) {
        BinaryTree bt = new BinaryTree();
        bt.root = new Node(array[0]);
        for (int i = 1; i < array.length; i++) {
            //second attempt
            getSuitableNode(array[i], bt.root);


            //first attempt
//            if (bt.root.value < array[i]) {
//                if (lastRightNode.value <= array[i]) {
//                    lastRightNode.right = new Node(array[i], lastRightNode);
//                    lastRightNode = lastRightNode.right;
//                } else {
//                    lastRightNode.left = new Node(array[i], lastRightNode);
//                    lastRightNode = lastRightNode.left;
//                }
//            } else if (lastLeftNode.value <= array[i]) {
//                lastLeftNode.right = new Node(array[i], lastLeftNode);
//                lastLeftNode = lastLeftNode.right;
//            } else {
//                lastLeftNode.left = new Node(array[i], lastLeftNode);
//                lastLeftNode = lastLeftNode.left;
//            }
        }
        return bt;
    }

    public static void getSuitableNode(int value, Node node) {
        if (node.value <= value) {
            if (node.right == null) {
                node.right = new Node(value, node);
            } else {
                getSuitableNode(value, node.right);
            }
        } else if (node.left == null) {
            node.left = new Node(value, node);
        } else {
            getSuitableNode(value, node.left);
        }
    }

    Node binarySearch(int value) {
        return binarySearch(value, root);
    }

    Node binarySearch(int value, Node node) {
        if (node != null) {
            if (node.value == value) {
                return node;
            }
            if (value > node.value) {
                return binarySearch(value, node.right);
            }
            if (value < node.value) {
                return binarySearch(value, node.left);
            }
            //nikdy nenastane
            return null;
        } else {
            return null;
        }
    }

    Node root;

    public static class Node {

        int value;
        Node parent;
        Node left;//mensi nez parent
        Node right;//vetsi nez parent

        public Node(int value) {
            this.value = value;

        }

        public Node(int value, Node parent) {
            this.value = value;
            this.parent = parent;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }
}
