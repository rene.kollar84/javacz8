package cz.sda.java8.designepatterns.singleton;

import java.util.Scanner;

public class OrderMain {
    static boolean run=true;
    public static void main(String[] args) {
        //****** singleton ma private konstruktor, neexistuje zpusob jak vytvorit objekt pouze pomoci
        //staticke metody tridy
//        OrderCounter orderCounter = new OrderCounter();
//        OrderCounter orderCounter1 = new OrderCounter();
//
//        orderCounter1.incrementCount();
//        System.out.println(orderCounter.getOrderCount());
//        OrderCounter instance = OrderCounter.getInstance();
//        OrderCounter instance1 = OrderCounter.getInstance();
//
//        OrderCounter.getInstance().incrementCount();
//        System.out.println(OrderCounter.getInstance().getOrderCount());
//        System.out.println(instance1==instance);
        execCreationOrder();
        execProcessingOrder();
        //pouze cekame na enter, co nactem nas nezajima
        new Scanner(System.in).nextLine();
        run=false;
    }

    private static void execCreationOrder(){
        new Thread(()->{
            OrderCounter instance = OrderCounter.getInstance();
            do {
                instance.incrementCount();
                sleep(1000);
            }while(run);
        }).start();
    }

    private static void execProcessingOrder(){
        new Thread(()->{
            OrderCounter instance = OrderCounter.getInstance();
            do {
                System.out.println(instance.getOrderCount());
                sleep(1000);
            }while (run);
        }).start();
    }

    static void sleep(int time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {

        }
    }
}
