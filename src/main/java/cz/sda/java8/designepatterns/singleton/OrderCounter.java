package cz.sda.java8.designepatterns.singleton;

public class OrderCounter {
    private int orderCount;

    private static OrderCounter instance;

    private OrderCounter() {
        //simulace race condition
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized static OrderCounter getInstance(){
        if(instance==null){

           if(instance==null) {
               instance = new OrderCounter();
           }
        }
        return instance;
    }

    public void incrementCount(){
        orderCount++;
    }

    public int getOrderCount() {
        return orderCount;
    }


}
