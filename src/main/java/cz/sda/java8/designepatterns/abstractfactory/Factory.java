package cz.sda.java8.designepatterns.abstractfactory;

import cz.sda.java8.designepatterns.factorymethod.Game;

public interface Factory {

    Factory create(String gameType);


}
