package cz.sda.java8.designepatterns.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DynamicProxyExample {
    public static void main(String[] args) {
        List proxyInstance = (List) Proxy.newProxyInstance(
                DynamicProxyExample.class.getClassLoader(),
                new Class[]{List.class},
                new InvocationHandler() {

                   ArrayList proxy =  new ArrayList();

                    @Override
                    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
                        long l = System.nanoTime();
                        Object ret= method.invoke(proxy,objects);
                        System.out.println("Invoked method "+ method.getName()
                                +" duration:"+(System.nanoTime()-l)+" ns");
                        return ret;
                    }
                });

        proxyInstance.add("ahoj");
        System.out.println(proxyInstance.get(0));
    }

}
