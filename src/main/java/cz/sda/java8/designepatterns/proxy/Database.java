package cz.sda.java8.designepatterns.proxy;

public interface Database<K, V> {
    V read(K key);

    void write(K key, V value);
}
