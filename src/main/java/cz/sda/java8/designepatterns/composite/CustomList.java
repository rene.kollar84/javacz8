package cz.sda.java8.designepatterns.composite;

import cz.sda.java8.advancedf.oop.Iterator;

public class CustomList<E> {
    private int size = 0;
    private Item<E> actual;

    public void add(E item) {
        size++;
        Item<E> insertingItem = new Item<>(item, null, null);
        if (actual == null) {
            actual = insertingItem;
        } else {
            actual.next = insertingItem;
            insertingItem.prew = actual;
            actual = insertingItem;
        }
    }

    public int getSize() {
        return size;
    }

    public E getNext() {
        if (hasNext()) {
            actual = actual.next;
            return actual.item;
        }
        return null;
    }

    public E get() {
        return actual.item;
    }

    public E getPrew() {
        if (actual.prew != null) {
            actual = actual.prew;
            return actual.item;
        }
        return null;
    }

    public boolean hasNext() {
        return actual.next != null;
    }

    public boolean hasPrew() {
        return actual.prew != null;
    }

    private static class Item<E> {
        E item;
        Item<E> next;
        Item<E> prew;

        public Item(E item, Item<E> next, Item<E> prew) {
            this.item = item;
            this.next = next;
            this.prew = prew;
        }

        @Override
        public String toString() {
            return "Item{" +
                    "item=" + item +
                    '}';
        }
    }

}
