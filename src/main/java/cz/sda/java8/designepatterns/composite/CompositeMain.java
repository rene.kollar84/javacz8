package cz.sda.java8.designepatterns.composite;

public class CompositeMain {
    public static void main(String[] args) {
        CustomList<String> scl = new CustomList<>();
        scl.add("1");
        scl.add("2");
        scl.add("3");
        scl.add("4");

        while (scl.hasNext()){
            System.out.println(scl.get());
            scl.getNext();
        }

        while (scl.hasPrew()){
            System.out.println(scl.get());
            scl.getPrew();
        }
    }
}
