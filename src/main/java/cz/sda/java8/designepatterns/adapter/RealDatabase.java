package cz.sda.java8.designepatterns.adapter;

import java.util.ArrayList;
import java.util.List;

public class RealDatabase {
    List<Object> objects=new ArrayList<>();

    public List<Object> getAll(){
        return objects;
    }

    public void writeObject(Object o){
        objects.add(o);
    }
}
