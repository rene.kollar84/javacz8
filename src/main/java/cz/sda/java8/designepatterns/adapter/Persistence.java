package cz.sda.java8.designepatterns.adapter;

public interface Persistence {
    void save(Entity o);
    Entity get(int id);
}
