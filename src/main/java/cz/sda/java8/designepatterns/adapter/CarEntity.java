package cz.sda.java8.designepatterns.adapter;

public class CarEntity extends Entity{
    private double power;

    public CarEntity(double power) {
        super();
        this.power = power;
    }
}
