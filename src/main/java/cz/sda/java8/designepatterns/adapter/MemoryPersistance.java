package cz.sda.java8.designepatterns.adapter;

import java.util.ArrayList;
import java.util.List;

public class MemoryPersistance implements Persistence{
    List<Entity> entities=new ArrayList<>();

    @Override
    public void save(Entity o) {
        entities.add(o);
    }

    @Override
    public Entity get(int id) {
        return entities.stream().filter(entity -> entity.getId()==id).findFirst().get();
    }
}
