package cz.sda.java8.designepatterns.adapter;

public class DbPersistence implements Persistence{
    RealDatabase db=new RealDatabase();
    @Override
    public void save(Entity o) {
        db.writeObject(o);
    }

    @Override
    public Entity get(int id) {
      return db.getAll().stream()
               .filter(o->o instanceof Entity)
               .map(o -> (Entity)o).filter(e->e.getId()==id).findFirst().get();
    }
}
