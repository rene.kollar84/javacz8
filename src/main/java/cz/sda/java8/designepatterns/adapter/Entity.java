package cz.sda.java8.designepatterns.adapter;

public abstract class Entity {
    private static int count=0;
    int id;

    public Entity() {
        count++;
        setId(count);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
