package cz.sda.java8.designepatterns.adapter;

public class SchoolMain {
    public static void main(String[] args) {
        Persistence p=getPersistance("memory");

        p.save(new StudentEntity("Vojta"));
        p.save(new CarEntity(130));

        System.out.println(p.get(1));
        System.out.println(p.get(2));

        Persistence pdb=getPersistance("db");

        pdb.save(new StudentEntity("Vojta"));
        pdb.save(new CarEntity(130));

        System.out.println(pdb.get(3));
        System.out.println(pdb.get(4));
    }

    private static Persistence getPersistance(String type) {
        if ("memory".equals(type)) {
            return new MemoryPersistance();
        }else{
            return new DbPersistence();
        }

    }
}
