package cz.sda.java8.designepatterns.adapter;

public class StudentEntity extends Entity{
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StudentEntity(String name) {
        super();
        this.name = name;
    }
}
