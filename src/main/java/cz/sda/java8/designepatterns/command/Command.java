package cz.sda.java8.designepatterns.command;

public interface Command {
    void execute();
    void undo();
}
