package cz.sda.java8.designepatterns.command;

public class UpperCommand implements Command{
    TextEditor context;
    String oldString;

    public UpperCommand(TextEditor context) {
        this.context = context;

    }

    @Override
    public void execute() {
         oldString = context.getOutput();
        String newOutput = context.getOutput().toUpperCase();
        context.setOutput(newOutput);
    }

    @Override
    public void undo() {
        context.setOutput(oldString);
    }
}
