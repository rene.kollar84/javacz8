package cz.sda.java8.designepatterns.command;


import java.util.Stack;

public class TextEditor {

    private String output="";

    private Stack<Command> commands=new Stack<>();

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

   public void executeCommand(Command c){
        commands.push(c);
        c.execute();
   }

    public void addInput(String text){
        output += text;
    }

    public void undo(){
        Command pop = commands.pop();
        pop.undo();
    }

    public static void main(String[] args) {
        TextEditor textEditor = new TextEditor();
        textEditor.addInput("ahoj");
        textEditor.addInput(" fajn");
        System.out.println(textEditor.getOutput());
        textEditor.executeCommand(new BoldCommand(textEditor));
        System.out.println(textEditor.getOutput());
        textEditor.executeCommand(new UpperCommand(textEditor));


        System.out.println(textEditor.getOutput());
        textEditor.undo();
        System.out.println(textEditor.getOutput());
        textEditor.undo();
        System.out.println(textEditor.getOutput());

    }
}
