package cz.sda.java8.designepatterns.command;

public class BoldCommand implements Command{
    TextEditor context;


    public BoldCommand(TextEditor context) {
        this.context = context;

    }

    @Override
    public void execute() {
       String newOutput = "["+context.getOutput()+"]";
       context.setOutput(newOutput);
    }

    @Override
    public void undo() {
        context.setOutput(context.getOutput()
                .replace("[","")
                .replace("]",""));
    }
}
