package cz.sda.java8.designepatterns.interpret;

public class ValueExpression extends Exprersion {

    int value;

    public ValueExpression(int value) {
        this.value = value;
    }

    @Override
    public int eval() {
        return value;
    }
}
