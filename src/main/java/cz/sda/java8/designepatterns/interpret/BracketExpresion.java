package cz.sda.java8.designepatterns.interpret;

public class BracketExpresion extends Exprersion {
    Exprersion one;
    String operand;
    Exprersion two;

    public BracketExpresion(Exprersion one) {
        this.one = one;
    }


    @Override
    public int eval() {
        return switch (operand) {
            case "+" -> one.eval() + two.eval();
            case "*" -> one.eval() * two.eval();
            default -> 0;
        };
    }


}
