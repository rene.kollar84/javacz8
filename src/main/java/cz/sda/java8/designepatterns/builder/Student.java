package cz.sda.java8.designepatterns.builder;

import lombok.Builder;

@Builder
public class Student {

    public final static Student NULL=new Student(){
        @Override
        public String toString() {
            return "null student";
        }
    };
    private String name;
    private String surname;
    private String school;
    private int old;
    private int exam;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSchool() {
        return school;
    }

    public int getOld() {
        return old;
    }

    public int getExam() {
        return exam;
    }

    public Student() {
    }

    public Student(String name, String surname, String school, int old, int exam) {
        this.name = name;
        this.surname = surname;
        this.school = school;
        this.old = old;
        this.exam = exam;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", school='" + school + '\'' +
                ", old=" + old +
                ", exam=" + exam +
                '}';
    }
}
