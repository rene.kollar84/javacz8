package cz.sda.java8.designepatterns.builder;

import java.util.Optional;

public class StudenMain {
    public static void main(String[] args) {
        Student student = new Student("jan", "novak", "hight", 30, 2);
        Student student1 = new Student("novotny", "karel", "elementary", 1, 15);

        // ***************************DesignPatern null object
//        Student s=null;
        //null is unsave, may throw NullPointerException
//        s.toString();

        Student s=Student.NULL;
        s.toString();


        Optional<Student> st = Optional.empty();
        if(st.isPresent()){
            //....
        }
        st.ifPresent(notNullStudent->{
            //in here is save use all methods
            notNullStudent.toString();
        });
        // ***************************DesignPatern null object


        //v pripade ze muzem pouzit lombock, nemusime psat builder rucne
        Student hight = Student.builder().name("jan")
                .old(30)
                .exam(3)
                .school("hight")
                .surname("novak").build();

        Student2.StudentBuilder studentBuilder = new Student2.StudentBuilder();
        Student2 build = studentBuilder
                .withName("jan")
                .withAge(30)
                .withExam(3)
                .withSchool("hight")
                .withSurname("novak")
                .build();

        Student2 studentBest = Student2.builder()
                .withName("jan")
                .withAge(30)
                .withExam(3)
                .withSchool("hight")
                .withSurname("novak")
                .build();
    }

    static Student loadFromDb(int id){
        //student v db nebude
       return  null;
    }

    static Optional<Student> loadFromDbBetter(int id){
        boolean isInDb=false;
        if(isInDb){
            Student loadedFromDb=null;
            return Optional.of(loadedFromDb);
        }else{
            return Optional.empty();
        }
    }
}
