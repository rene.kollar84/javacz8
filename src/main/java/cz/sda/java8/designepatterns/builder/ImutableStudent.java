package cz.sda.java8.designepatterns.builder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ImutableStudent {
    private final String name;
    private final Date birthDay;

    List<String> jazykoveDovednosti=new ArrayList<>(List.of("AJ","CZ"));

    public String getName() {
        return name;
    }

    public List<String> getJazykoveDovednosti() {
        return Collections.unmodifiableList(jazykoveDovednosti);
    }

    public Date getBirthDay() {
        //vracime kopii abychom zabranili modifikaci datumu
        return new Date(birthDay.getTime());
    }

    @Override
    public String toString() {
        return "ImutableStudent{" +
                "name='" + name + '\'' +
                ", birthDay=" + birthDay +
                '}';
    }

    public ImutableStudent(String name, Date birthDay) {
        this.name = name;
        this.birthDay = birthDay;
    }

    public static void main(String[] args) {
        ImutableStudent imutableStudent = new ImutableStudent("lenka",new Date());
        System.out.println(imutableStudent);
        Date d = imutableStudent.getBirthDay();
        d.setYear(1);
        System.out.println(imutableStudent);
    }
}
