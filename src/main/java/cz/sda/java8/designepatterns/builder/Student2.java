package cz.sda.java8.designepatterns.builder;

public class Student2 {
    final private String name;
    final private String surname;
    final private String school;
    final private int old;
    final private int exam;

    private Student2(String name, String surname, String school, int old, int exam) {
        this.name = name;
        this.surname = surname;
        this.school = school;
        this.old = old;
        this.exam = exam;
    }

    public static StudentBuilder builder() {
        return new StudentBuilder();
    }

    public static class StudentBuilder {
        private String name;
        private String surname;
        private String school;
        private int age;
        private int exam;


        public StudentBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public StudentBuilder withSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public StudentBuilder withSchool(String school) {
            this.school = school;
            return this;
        }

        public StudentBuilder withAge(int age) {
            this.age = age;
            return this;
        }

        public StudentBuilder withExam(int exam) {
            this.exam = exam;
            return this;
        }

        public Student2 build() {
            return new Student2(name, surname, school, age, exam);
        }
    }
}
