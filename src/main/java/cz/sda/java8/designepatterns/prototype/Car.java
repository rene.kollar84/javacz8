package cz.sda.java8.designepatterns.prototype;

public class Car implements Cloneable{
    String type;
    Engine engine;

    String adjustments;

    public Car(String type, Engine engine, String adjustments) {
        this.type = type;
        this.engine = engine;
        this.adjustments = adjustments;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public Car clone() throws CloneNotSupportedException {
        Object ret = super.clone();
        Car ret1 = (Car) ret;
        ret1.engine= (Engine) engine.clone();
        return ret1;
    }

    public static class Engine implements Cloneable{
        double power;

        public Engine(double power) {
            this.power = power;
        }

        @Override
        public String toString() {
            return "Engine{" +
                    "power=" + power +
                    '}';
        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            return super.clone();
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "type='" + type + '\'' +
                ", engine=" + engine +
                ", adjustments='" + adjustments + '\'' +
                '}';
    }
}
