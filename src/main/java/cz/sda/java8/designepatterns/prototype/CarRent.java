package cz.sda.java8.designepatterns.prototype;

public class CarRent {
    public static void main(String[] args) throws CloneNotSupportedException {
        Car.Engine engine = new Car.Engine(130);
        Car toyota1 = new Car("Toyota", engine,"firemni logo na kapote");
        System.out.println(toyota1);


        Car clone = toyota1.clone();
        clone.adjustments="tonovane skla";
        clone.getEngine().power=100;

        System.out.println(toyota1);
        System.out.println(clone);
    }
}
