package cz.sda.java8.designepatterns.bestpractise.solid;

public class SegregationSolution {

    public static void main(String[] args) {
        GumovaKachna gumovaKachna = new GumovaKachna();
        //gumova kachna nemuze jist
        //gumovaKachna.eat();
        LiveProperties neznamyZivot = ()->{
            System.out.println("e.t. vola mnam");
        };
        neznamyZivot.eat();
    }
    public static abstract class Kachna implements AnimalProperties{

    }

    static class DivokaKachna extends Kachna implements LiveProperties{
        @Override
        public String getBarva() {
            return "hneda";
        }
        @Override
        public void eat() {
            System.out.println("mnam");
        }
    }

    static class DomaciKachna extends Kachna implements LiveProperties{
        @Override
        public String getBarva() {
            return "bila";
        }

        @Override
        public void eat() {
            System.out.println("mnam");
        }
    }

    static  class GumovaKachna extends Kachna{

        @Override
        public String getBarva() {
            return null;
        }
    }

    static class Kun implements AnimalProperties,LiveProperties{

        @Override
        public void eat() {

        }

        @Override
        public String getBarva() {
            return null;
        }
    }

    public interface AnimalProperties{

        String getBarva();
    }

    public interface LiveProperties{
        void eat();
    }
}
