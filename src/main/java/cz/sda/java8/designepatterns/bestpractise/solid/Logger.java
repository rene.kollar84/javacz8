package cz.sda.java8.designepatterns.bestpractise.solid;

import java.nio.file.Path;
import java.util.function.Consumer;

public class Logger {
    private final Consumer<String> printMethod;
    boolean alsoSendEmail;

    public void info(String msg){
        printMethod.accept(msg);
        if(alsoSendEmail){
            sendEmail(msg);
        }
    }

    public Logger(Consumer<String> printMethod) {
        this.printMethod=printMethod;
    }

    //poruseni principu single responsibility
    private void sendEmail(String msg) {
        //code for sending email
        //may be long code
    }

    private void sendEmailBetter(String msg) {
        MailSender.sendEmail(msg);
    }

    //only stub for presenting single responsibility
    private static class MailSender {
        public static void sendEmail(String msg) {
        }
    }

    public static void main(String[] args) {
        Logger logger = new Logger(System.out::println);
        Logger logger1 = new Logger(getFileConsumer());
    }

    private static Consumer<String> getFileConsumer() {
        return (msg)->{
            //some code that write msg to file
        };
    }
}
