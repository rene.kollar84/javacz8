package cz.sda.java8.designepatterns.bestpractise;

import cz.sda.java8.advancedf.ooprepeating.SojoveMaso;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main3 {
    public static void main(String[] args) {
        //find all lower case
        Stream.of("a","b","C","d").filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return Character.isLowerCase(s.charAt(0));
            }
        });

        Stream.of("a","b","C","d").forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });
        //same
        Stream.of("a","b","C","d").forEach(s -> System.out.println(s));
        //same using method reference
        Stream.of("a","b","C","d").forEach(System.out::println);
        //same using method reference
        Consumer<String> c = System.out::println;
        Stream.of("a","b","C","d").forEach(c);
    }
}
