package cz.sda.java8.designepatterns.bestpractise.solid;

public class DcI {
    public static void main(String[] args) {
        printLogMessage(new OpenClosePrincipe.ConsoleLogger());
    }
    public static void printLogMessage(OpenClosePrincipe.Logger logger){
        logger.info("any");
    }
}
