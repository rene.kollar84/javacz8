package cz.sda.java8.designepatterns.bestpractise;

import java.util.Objects;

public class Person {

    private String name;
    private int birthYear;

    public Person(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    @Override
    public boolean equals(Object obj) {
       if(! (obj instanceof Person)){
           return false;
       }else {
           Person obj1 = (Person) obj;
           return birthYear == obj1.birthYear && Objects.equals(name,obj1.name);
       }

    }
}
