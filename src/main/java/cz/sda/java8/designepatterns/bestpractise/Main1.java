package cz.sda.java8.designepatterns.bestpractise;

import lombok.Builder;

import java.util.ArrayList;
import java.util.List;

//name of class is always singular
public class Main1 {
    //snake case "_" is used for constants
    final int FINAL_STATE=0;
    public static void main(String[] args) {

    }

    public int computeCountOfUpperLetter(String inputText) {
        int upperLetterCount = 0;
        for (int i = 0; i < inputText.length(); i++) {
            if (Character.isUpperCase(inputText.charAt(i))) {
                upperLetterCount++;
            }
        }
        return upperLetterCount;
    }



    public int computeCountOfUpperLetterFromEnd(String inputText) {
        int upperLetterCount = 0;
        //joke
        for (int d = inputText.length(); d >=0; d--) {
            if (Character.isUpperCase(inputText.charAt(d))) {
                upperLetterCount++;
            }
        }
        return upperLetterCount;
    }

    public void a() {
        List<String> strings = new ArrayList<>();

        String a1 = "ahoj";
        strings.add(a1);

//necham si to na priste bude se to hodit --- tak tohle ne!!!!
//        String a2="ahoj2";
//        strings.add(a2);
    }
//sice je metoda nepouzita ale demonstruji vnorene bloky
    public void betterA() {
        List<String> strings = new ArrayList<>();


        {
            //neco
            String a1 = "ahoj";
            //....spousta kodu
            strings.add(a1);
        }

        {
            //neco
            String a1 = "ahoj2";
            //....spousta kodu
            strings.add(a1);
        }
    }
    //ukazka nahrazeni dlouheho (v tomto pripade pouze 3) poctu argumentu
    public void deleteByAttributes(String name, String town, int numberAny){

    }

    public void deleteByAttributes(InputForDeleteMethod input){

    }

    public void exampleCall(){
        ///imagine that InputForDeleteMethod has builder
//        InputForDeleteMethod input = InputForDeleteMethod.builder()
//                .numberAny(1)
//                .town("prague")
//                .name("neco").build();
//
//        deleteByAttributes(input);
    }

    private class InputForDeleteMethod{
        String name;
         String town;
        int numberAny;

    }
}
