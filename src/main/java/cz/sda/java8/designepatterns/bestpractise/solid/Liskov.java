package cz.sda.java8.designepatterns.bestpractise.solid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Liskov {

    public static void main(String[] args) {
        int[] numbers = new int[]{3,5,1,3,7,9,4};

        Sorter streamSorter = new StreamSorter();
        Sorter arraysSorter = new ArraysSorter();

        int[] sort = streamSorter.sort(numbers);
        int[] sort1 = arraysSorter.sort(numbers);
        //nepouzivana vec v dnesni dobe ale je fajn pro sdeleni, co ocekavame
        assert sort1 == sort;
    }

    interface Sorter{
        int[] sort(int[] input);
    }

    static class StreamSorter implements Sorter{

        @Override
        public int[] sort(int[] input) {
            return Arrays.stream(input).sorted().toArray();
        }
    }

    static class ArraysSorter implements Sorter{

        @Override
        public int[] sort(int[] input) {
            //create copy because Arrays sort modifing source array
            int[] ints = Arrays.copyOf(input, input.length);
           return ints;
        }
    }
}
