package cz.sda.java8.designepatterns.bestpractise.solid;

public class OpenClosePrincipe {

    public static void main(String[] args) {
        Logger consoleLogger = new ConsoleLogger();
        Logger fileLogger = new FileLogger();

        printInformation(consoleLogger,"ahoj");
        printInformation(fileLogger,"ahoj");
    }

    //liskov principe
    public static void printInformation(Logger l,String msg){
        l.info(msg);
    }
     interface Logger{
        void info(String msg);
    }


    public static class ConsoleLogger implements Logger{

        @Override
        public void info(String msg) {
            System.out.println(msg);
        }
    }

    public static class FileLogger implements Logger{

        @Override
        public void info(String msg) {
            //some code to write msg to file
        }
    }
}
