package cz.sda.java8.designepatterns.bestpractise.solid;

public class Segregation {

    public static void main(String[] args) {
        GumovaKachna gumovaKachna = new GumovaKachna();
        gumovaKachna.eat();

    }
    public static abstract class Kachna implements AnimalProperties{
        public void eat(){
            System.out.println("mnam");
        }
    }

    static class DivokaKachna extends Kachna{
        @Override
        public String getBarva() {
            return "hneda";
        }
    }

    static class DomaciKachna extends Kachna{
        @Override
        public String getBarva() {
            return "bila";
        }
    }

    static  class GumovaKachna extends Kachna{

        @Override
        public String getBarva() {
            return null;
        }
    }

    static class Kun implements AnimalProperties{

        @Override
        public void eat() {

        }

        @Override
        public String getBarva() {
            return null;
        }
    }

    public interface AnimalProperties{
        void eat();
        String getBarva();
    }
}
