package cz.sda.java8.designepatterns.bestpractise;

import java.util.function.Predicate;

public class Main2 {
    public static void main(String[] args) {
        computeCountOfUpperLetter("AhOJ");
        computeCountOfLowerLetter("AhOJ");
        computeCountOfSpecificLetter("aaahoj",'a');

        computeCountOfSpecific("AhOJ", new Predicate<Character>() {
            @Override
            public boolean test(Character testedChar) {
               return Character.isUpperCase(testedChar);
            }
        });
        //same as previous
        computeCountOfSpecific("AhOJ",testedChar->Character.isLowerCase(testedChar));
        computeCountOfSpecific("aaahoj",testedChar-> testedChar == 'a');

    }

    //best solution
    public static int computeCountOfSpecific(String inputText, Predicate<Character> predicate) {
        int charTrue = 0;
        for (int i = 0; i < inputText.length(); i++) {
           if(predicate.test(inputText.charAt(i))){
               charTrue++;
           }
        }
        return charTrue;
    }

    //better tah duplicate
    public static int computeCountOfUpperOrLowerLetter(String inputText, boolean countUpper) {
        int upperLetterCount = 0;
        for (int i = 0; i < inputText.length(); i++) {
            //ternar expresion
            //condition is expresion or boolean value
            // boolean result = (condition) ? expresion for true condition : expresion for false condition
            boolean consistCondition = countUpper ? Character.isUpperCase(inputText.charAt(i)) :
                    Character.isLowerCase(inputText.charAt(i));
            if (consistCondition) {
                upperLetterCount++;
            }
        }
        return upperLetterCount;
    }

    //duplicate
    public static int computeCountOfUpperLetter(String inputText) {
        int upperLetterCount = 0;
        for (int i = 0; i < inputText.length(); i++) {
            if (Character.isUpperCase(inputText.charAt(i))) {
                upperLetterCount++;
            }
        }
        return upperLetterCount;
    }

    public static int computeCountOfLowerLetter(String inputText) {
        int upperLetterCount = 0;
        for (int i = 0; i < inputText.length(); i++) {
            if (Character.isUpperCase(inputText.charAt(i))) {
                upperLetterCount++;
            }
        }
        return upperLetterCount;
    }

    public static int computeCountOfSpecificLetter(String inputText,char letterToFind) {
        int upperLetterCount = 0;
        for (int i = 0; i < inputText.length(); i++) {
            if (letterToFind == inputText.charAt(i)) {
                upperLetterCount++;
            }
        }
        return upperLetterCount;
    }
}
