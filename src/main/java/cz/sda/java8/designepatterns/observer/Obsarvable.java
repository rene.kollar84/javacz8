package cz.sda.java8.designepatterns.observer;

public interface Obsarvable {
     void fatherIncoming();
}
