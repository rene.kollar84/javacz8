package cz.sda.java8.designepatterns.observer;

import java.util.ArrayList;
import java.util.List;

public class House {
    List<Obsarvable> members=new ArrayList<>();

    public static void main(String[] args) {
        House house = new House();

        house.members.add(new Dog());
        house.members.add(new Wife());
        house.members.add(new Children());
        house.members.add(new Children());


        house.fatherIsAtHome();
    }

    private void fatherIsAtHome() {
        members.forEach(o->o.fatherIncoming());
    }
}
