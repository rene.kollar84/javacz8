package cz.sda.java8.designepatterns.decorator;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FilePrinter implements Printer{

    FileOutputStream fos;
    public FilePrinter(String fileName) {
        try {
            fos=new FileOutputStream(fileName);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }



    @Override
    public void printCharacter(char c) {
        try {
            fos.write(new byte[]{(byte) c},0,1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void goToNextLine() {
        printCharacter('\n');
    }
}
