package cz.sda.java8.designepatterns.decorator;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class Example {
    @Test
    public void testSimplePrinter() {
        final Printer printer = new SimplePrinter();
        printer.printCharacter('H');
        printer.printCharacter('E');
        printer.printCharacter('L');
        printer.printCharacter('L');
        printer.printCharacter('O');
        printer.goToNextLine();
        printer.printCharacter('W');
        printer.printCharacter('O');
        printer.printCharacter('R');
        printer.printCharacter('L');
        printer.printCharacter('D');
        printer.goToNextLine();
    }

    @Test
    public void testWrappingPrinter() {
        final Printer printer = new WrappingPrinter(new SimplePrinter(), 3);
        printer.printCharacter('H');
        printer.printCharacter('E');
        printer.printCharacter('L');
        printer.printCharacter('L');
        printer.printCharacter('O');
        printer.printCharacter('W');
        printer.printCharacter('O');
        printer.printCharacter('R');
        printer.printCharacter('L');
        printer.printCharacter('D');
    }

    @Test
    public void testStringPrinter() {
        final StringPrinter printer = new StringPrinter(new SimplePrinter());
        printer.printString("Hello");
        printer.printString("World");
    }

    @Test
    public void testStringPrinterToFile() {
        final StringPrinter printer = new StringPrinter(new FilePrinter("fp.txt"));
        printer.printString("Hello");
        printer.printString("World");
    }

    @Test
    public void showInputStream() {
        //InputStream
        //OutputStream
        System.out.println();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        log("ahoj",byteArrayOutputStream);
        log("svete",byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        String s = new String(bytes);
        System.out.print(s);
    }

    private void log(String msg, OutputStream s){
        PrintStream printStream = new PrintStream(s);
        printStream.println(msg);
    }
}
