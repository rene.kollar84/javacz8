package cz.sda.java8.designepatterns.decorator;

public interface Printer {
    void printCharacter(char c);

    void goToNextLine();
}
