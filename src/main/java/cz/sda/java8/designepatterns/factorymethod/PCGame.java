package cz.sda.java8.designepatterns.factorymethod;

public abstract class PCGame implements Game{

    @Override
    public String getType() {
        return "PC";
    }
}
