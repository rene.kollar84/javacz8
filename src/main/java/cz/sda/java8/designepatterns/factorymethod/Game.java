package cz.sda.java8.designepatterns.factorymethod;

public interface Game {
    String getName();
    String getType();

    void play();
}
