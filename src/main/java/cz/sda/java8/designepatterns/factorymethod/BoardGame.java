package cz.sda.java8.designepatterns.factorymethod;

public abstract class BoardGame implements Game{
    @Override
    public String getType() {
        return "DESK";
    }
}
