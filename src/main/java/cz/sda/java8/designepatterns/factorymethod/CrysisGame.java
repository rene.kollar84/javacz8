package cz.sda.java8.designepatterns.factorymethod;

public class CrysisGame extends PCGame{
    @Override
    public String getName() {
        return "CRYSIS";
    }

    @Override
    public void play() {
        System.out.println("playing "+getName());
    }
}
