package cz.sda.java8.designepatterns.factorymethod;

public class GameFactory {
    public Game createMonopoly() {
        return new MonopolyGame();
    }

    public Game createCrysisGame(){
        return new CrysisGame();
    }


}
