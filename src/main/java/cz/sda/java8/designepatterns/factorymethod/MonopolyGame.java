package cz.sda.java8.designepatterns.factorymethod;

public class MonopolyGame extends BoardGame{
    @Override
    public String getName() {
        return "MONOPOLY";
    }

    @Override
    public void play() {
        System.out.println("playing "+getName());
    }
}
