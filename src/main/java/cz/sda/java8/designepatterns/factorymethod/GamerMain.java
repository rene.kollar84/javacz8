package cz.sda.java8.designepatterns.factorymethod;

public class GamerMain {
    public static void main(String[] args) {
        GameFactory gameFactory = new GameFactory();
        Game crysisGame = gameFactory.createCrysisGame();
        Game monopoly = gameFactory.createMonopoly();

        crysisGame.play();
        monopoly.play();
    }
}
