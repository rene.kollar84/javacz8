package cz.sda.java8.designepatterns.flyweight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CharRepo {
    Map<Integer,Znak> chars=new HashMap();

    public Znak getChar(int type){
        if(!chars.containsKey(type)){
            Znak znak = new Znak((char) type);
            chars.put(type,znak);

        }else{
           // System.out.println("reusing "+(char)type);
        }
        return chars.get(type);
    }

    public static void main(String[] args) {
        String text="ahoj jak se mame, nic moc dnes";

        CharRepo charRepo = new CharRepo();

        for(int i=0;i<text.length();i++){
            char c = text.charAt(i);
            Znak znak = charRepo.getChar(c);
            znak.draw();
        }
    }
}
