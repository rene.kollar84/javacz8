package cz.sda.java8.designepatterns.flyweight;

public class Znak {
    private char value;

    public Znak(char value) {
        this.value = value;
    }

    public void draw(){
        System.out.print(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Znak znak = (Znak) o;

        return value == znak.value;
    }

    @Override
    public int hashCode() {
        return value;
    }
}
