package cz.sda.java8.fundamentalcoding;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        if (a < 0) {
            System.out.println("isn't positive");
            return;
        }
        for (int i = 1; i <= a; i++) {
            if (i % 3 == 0 && i % 7 == 0) {
                System.out.println(i + "Fizz buzz");
                continue;
            }
            if (i % 3 == 0) {
                System.out.println("Fizz ");
            }
            if (i % 7 == 0) {
                System.out.println("Buzz");
            }
        }
    }
}

