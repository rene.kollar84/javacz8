package cz.sda.java8.fundamentalcoding;

public class Task10 {
    public static void main(String[] args) {
        System.out.println("input a number");
        int number = Math.abs(InputUtil.nextInt());
        int sum = 0;

        //123 % 10 = 3 -> sum
        //123 / 10 = 12
        //12 % 10 = 2-> sum
        //12 / 10 = 1
        //1 % 10 = 1 -> sum
        //1 / 10  = 0

        sum = getSum(number);
        System.out.println(sum);
    }

    private static int getSum(int number) {
        int sum = 0;
        while (number != 0) {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }
}
