package cz.sda.java8.fundamentalcoding;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        if (a < 0) {
            System.out.println("it is not positive");
            return;

        }
        for (int i = 2; i < a; i++) {
            if (isPrime(i)) {
                System.out.println(i);
            }

        }
    }

    private static boolean isPrime(int n) {
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }

        }
        return true;
    }
}

