package cz.sda.java8.fundamentalcoding;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Task15 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] numbers = new int[10];

        for (int i = 0; i < 10; i++) {
            numbers[i] = scanner.nextInt();
        }

        solutionWithSet(numbers);
        System.out.println();

        complicatedDuplicatesSolution(numbers);
        System.out.println();

        solutionWithDuplicates(numbers);
    }

    private static void complicatedDuplicatesSolution(int[] numbers) {
        int[] duplicated = new int[numbers.length / 2];
        int indexOfDuplicated = 0;

        for (int i = 0; i < numbers.length; i++) {
            for (int k = i + 1; k < numbers.length; k++) {
                if (numbers[k] == numbers[i]) {
                    boolean foundAlready = false;

                    for (int j = 0; j < indexOfDuplicated; j++) {
                        if (numbers[i] == duplicated[j]) {
                            foundAlready = true;
                            break;
                        }
                    }

                    if (!foundAlready) {
                        duplicated[indexOfDuplicated] = numbers[i];
                        indexOfDuplicated++;
                        System.out.print(numbers[i] + " ");
                    }
                }
            }
        }
    }

    /*
    duplicatedNumbers.add(numbers[i]) tries to add value to the set.
    If present, returns false... if not, adds it and returns true.
     */
    private static void solutionWithSet(int[] numbers) {
        Set<Integer> duplicatedNumbers = new HashSet<>();
        for (int i = 0; i < numbers.length; i++) {
            for (int k = i + 1; k < numbers.length; k++) {
                if (numbers[k] == numbers[i] && duplicatedNumbers.add(numbers[i])) {
                    System.out.print(numbers[i] + " ");
                }
            }
        }
    }

    private static void solutionWithDuplicates(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            for (int k = i + 1; k < numbers.length; k++) {
                if (numbers[k] == numbers[i]) {
                    System.out.print(numbers[i] + " ");
                }
            }
        }
    }
}
