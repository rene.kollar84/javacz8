package cz.sda.java8.fundamentalcoding;

public class ArraysExample {
    public static void main(String[] args) {
        print(pyr(9));

    }

    public static char[][] pyr(int width) {
        char[][] twoDimAr = new char[width / 2 + 1][width];
        int countOf1 = 1;
        for (int i = 0; i < twoDimAr.length; i++) {
            int idexofFirst1 = (twoDimAr[i].length / 2) - (countOf1 - 1) / 2;
            for (int j = 0; j < twoDimAr[i].length; j++) {
                if (j < idexofFirst1 || j >= idexofFirst1 + countOf1) {
                    twoDimAr[i][j] = ' ';
                } else {
                    twoDimAr[i][j] = 'x';
                }
            }
            countOf1 += 2;
        }
        return twoDimAr;
    }

    public static void print(char[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
    }
}
