package cz.sda.java8.fundamentalcoding;

public class Task7b {
    public static void main(String[] args) {
        int n = InputUtil.nextInt();
        System.out.println(getFibonacci(n));
        System.out.println(getFibonacciRecursive(n));
        System.out.println(getFactorial(n));

    }

    private static int getFibonacci(int n) {
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 1;
        }
        int stepMinus2 = 1;
        int stepMinus1 = 1;
        int fib = 0;

        for (int i = 3; i <= n; i++) {
            fib = stepMinus2 + stepMinus1;
            stepMinus2 = stepMinus1;
            stepMinus1 = fib;
        }
        return fib;
    }

    private static int getFibonacciRecursive(int n) {
        return (n < 3) ? 1 : getFibonacciRecursive(n - 1) + getFibonacciRecursive(n - 2);
    }

    private static int getFactorial(int n) {
        return (n < 3) ? n : n * getFactorial(n - 1);
//        return (n==1) ? 1 : (n==2) ? 2 : n * getFactorial(n - 1);
    }
}

