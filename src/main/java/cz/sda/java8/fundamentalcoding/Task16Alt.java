package cz.sda.java8.fundamentalcoding;

import java.util.Scanner;

public class Task16Alt {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int count = 1;
        int maxCount = 1;
        int prevNumber = 0;

        for (int i = 0; i < 10; i++) {
            int current = scanner.nextInt();
            if (i > 0) {
                if (current > prevNumber) {
                    count++;
                } else {
                    if (count > maxCount) {
                        maxCount = count;
                    }
                }
            }
            prevNumber = current;
        }

        System.out.println(maxCount);
    }
}
