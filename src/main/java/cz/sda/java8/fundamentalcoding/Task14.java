package cz.sda.java8.fundamentalcoding;

public class Task14 {

    //Write an application that reads two lowercase letters of the Latin alphabet (type char)
    //and calculates how many characters is there in the alphabet between given letters.
    //Hint - use the ASCII code table and treat the characters as int numbers.

    public static void main(String[] args) {

        Character character1 = InputUtil.nextChar();
        Character character2 = InputUtil.nextChar();
        if (character1 == null || character2 == null) {
            System.out.println("Nebyly zadany dva znaky!");
            return;

        }
        if (character1.charValue() < 'a' || character1.charValue() > 'z' || Character.isUpperCase(character2)) {
            return;

        }
        System.out.println("Vzdalenost mezi zadanymi pismy je: " + (Math.abs((int) (character1.charValue() - character2.charValue())) - 1));


    }
}
