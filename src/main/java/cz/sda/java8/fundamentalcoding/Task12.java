package cz.sda.java8.fundamentalcoding;

public class Task12 {
    public static void main(String[] args) {
        String text = InputUtil.nextLine();
        float percentage = getPercentage(text, (char) 20); // 20 = mezera
        System.out.println(percentage);
    }

    public static float getPercentage(String text, char test) {
        int count = 0;
        for (int i = 0; i < text.length(); i++) {
            char actualChar = text.charAt(i);
            if (actualChar == test) {
                count++;
            }
        }
        return (float) (100.0 * count / text.length());
    }
}
