package cz.sda.java8.fundamentalcoding;

public class Author {
    private String surName;
    private String nationality;
    private String name;

    public Author(String surName, String nationality, String name) {
        this.surName = surName;
        this.nationality = nationality;
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public String getNationality() {
        return nationality;
    }

    public String getName() {
        return name;
    }
}


