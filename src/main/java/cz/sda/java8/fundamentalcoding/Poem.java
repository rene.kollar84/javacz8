package cz.sda.java8.fundamentalcoding;


public class Poem {
    private int stropheNumbers;
    private Author creator;

    public Poem(int stropheNumbers, Author creator) {
        this.stropheNumbers = stropheNumbers;
        this.creator = creator;
    }

    public int getStropheNumbers() {
        return stropheNumbers;
    }

    public Author getCreator() {
        return creator;
    }
}

