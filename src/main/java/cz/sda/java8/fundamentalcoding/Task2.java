package cz.sda.java8.fundamentalcoding;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Zadaj vahu v Kg");
        float h = scanner.nextFloat();
        System.out.println("zadaj vysku");
        float v = scanner.nextFloat();
        if (v > 2.5) {
            v = v / 100;
        }
        float bmi = h / (v * v);
        if (bmi <= 18.5f || bmi >= 24.9f) {
            System.out.println("BMI not optimal");
        } else {
            System.out.println("BMI optimal");
        }
        System.out.printf("bmi: %.2f", bmi);

    }
}
