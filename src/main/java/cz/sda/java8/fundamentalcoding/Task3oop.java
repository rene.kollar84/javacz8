package cz.sda.java8.fundamentalcoding;

import cz.sda.java8.fundamentalcoding.oop.QuadraticEquation;

import java.util.Scanner;

public class Task3oop {
    public static void main(String[] args) {
        QuadraticEquation quadraticEquation = new QuadraticEquation();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Zadej kvadratickou rovnici : ");
        System.out.print(" \n a = ");
        float a = scanner.nextFloat();
        System.out.print(" \n b = ");
        float b = scanner.nextFloat();
        System.out.print("\n c = ");
        float c = scanner.nextFloat();
        System.out.printf("Kvadratická rovnice je tedy: %sx^2 + %sx + %s = 0 \n", a, b, c);
        quadraticEquation.setA(a);
        quadraticEquation.setB(b);
        quadraticEquation.setC(c);
        System.out.println(quadraticEquation);
    }

}
