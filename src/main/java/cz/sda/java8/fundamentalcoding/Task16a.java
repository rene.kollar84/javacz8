package cz.sda.java8.fundamentalcoding;

import java.util.Scanner;

public class Task16a {

    /*Write an application that takes 10 numbers from the user (type int) and write the length
of the longest such subsequence of these numbers, which is increasing.
For example, for the numbers: "1, 3, 8, 4, 2, 5, 6, 11, 13, 7" the program should write "5"
as the length of the longest increasing subsequence (underlined in the example).*/


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] numbers = new int[10];

        for (int i = 0; i < 10; i++) {
            numbers[i] = scanner.nextInt();
        }

        int count = 0;
        int maxCount = 0;
        for (int i = 0; i < numbers.length - 1; i++) {

            if (numbers[i] < numbers[i + 1]) {
                if (count == 0) {
                    count = 1;
                }
                count = count + 1;
            } else {
                if (count > maxCount) {
                    maxCount = count;
                }
                count = 0;
            }
        }
        System.out.println(maxCount);
    }
}
