package cz.sda.java8.fundamentalcoding;

public class Task9 {
    public static void main(String[] args) {
        System.out.print("Enter length of wave:");
        int length = InputUtil.nextInt();
        System.out.print("Enter height of wave:");
        int height = InputUtil.nextInt();

        int countOfFirstSpaceAmount = 0;
        int countOfDecreasingSpaceAmount = 2 * height - 3;
        int countOfIncreasingSpaceAmount = 0;

        for (int i = 0; i < height; i++) {
            boolean increasingSpace = i == height - 1;
            int nextStar = countOfFirstSpaceAmount;
            for (int j = 0; j < length; j++) {
                if (j < nextStar) {
                    System.out.print(" ");

                } else {
                    System.out.print('*');
                    if (increasingSpace) {
                        nextStar += countOfIncreasingSpaceAmount + 1;
                        if (i < height - 1) {
                            increasingSpace = false;
                        }
                    } else {
                        nextStar += countOfDecreasingSpaceAmount + 1;
                        if (i > 0) {
                            increasingSpace = true;
                        }
                    }
                }
            }
            System.out.println();
            countOfFirstSpaceAmount++;
            if (countOfDecreasingSpaceAmount - 2 < 0) {
                countOfDecreasingSpaceAmount = 0;
            } else {
                countOfDecreasingSpaceAmount -= 2;
            }
            countOfIncreasingSpaceAmount = 2 * i + 1;
        }
    }
}