package cz.sda.java8.fundamentalcoding.oop;

public class QuadraticEquation {
    private float a;
    private float b;
    private float c;

    public float getA() {
        return a;
    }

    public void setA(float a) {
        this.a = a;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }

    public float getC() {
        return c;
    }

    public void setC(float c) {
        this.c = c;
    }

    public double getDiscriminant() {
        return b * b - 4 * a * c;
    }

    private boolean canEvaluate() {
        return getDiscriminant() >= 0;
    }

    public double getRoot1() {
        return (-b + Math.sqrt(getDiscriminant())) / (2 * a);
    }

    public double getRoot2() {
        return (-b - Math.sqrt(getDiscriminant())) / (2 * a);
    }

    @Override
    public String toString() {
        if (canEvaluate()) {
            return String.format("Koren 1 = %.2f", getRoot1()) + String.format(" \n Koren 2 = %.2f", getRoot2());
        } else {
            return "Rovnice nemá řešení";
        }
    }
}
