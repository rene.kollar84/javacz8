package cz.sda.java8.fundamentalcoding.oop;

import java.util.Date;

public class Person {
    private String name;
    private Date brirthDate;

    private static int personCount = 0;

    public Person(Date brirthDate) {
        this.brirthDate = brirthDate;
        personCount++;
    }

    public static int getPersonCount() {
        return personCount;
    }

    public Date getBrirthDate() {
        return brirthDate;
    }

    @Deprecated
    public void setBirthDate(Date date) {
        this.brirthDate = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
