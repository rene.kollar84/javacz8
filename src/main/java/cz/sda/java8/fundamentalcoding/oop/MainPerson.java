package cz.sda.java8.fundamentalcoding.oop;

import java.util.Date;

public class MainPerson {
    public static void main(String[] args) {
        Person person1 = new Person(new Date(1984, 5, 28));
        person1.setName("Adam");
        Person person2 = new Person(new Date());
        person2.setName("jana");


        System.out.println(person1.getName() + " " + person1.getBrirthDate());
        System.out.println(person2.getName() + " " + person2.getBrirthDate());

        System.out.println(Person.getPersonCount());
    }
}
