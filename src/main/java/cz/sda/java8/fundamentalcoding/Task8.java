package cz.sda.java8.fundamentalcoding;

public class Task8 {

    /*Write an application that implements a simple calculator. The application should:
a. read first number (type float)
b. read one of following symbols: + - / *
c. read second number (type float)
d. return a result of given mathematical operation
If the user provides a symbol other than supported, the application should print "Invalid
symbol". If the entered action cannot be implemented (i.e. it is inconsistent with the
principles of mathematics), the application should print "Cannot calculate".*/

    public static void main(String[] args) {
        System.out.println("zadaj prve cislo");
        float first = InputUtil.nextFloat();
        System.out.println("zadaj operator");
        String operator = InputUtil.nextString();
        switch (operator) {
            case "+":
                break;
            case "-":
                break;
            case "*":
                break;
            case "/":
                break;
            default: {
                System.out.println("zly operator");
                return;
            }

        }
        System.out.println("zadaj druhe cislo");
        float second = InputUtil.nextFloat();
        if (second == 0 && operator.equals("/")) {
            System.out.println("Delenie nulou");
            return;
        }


        float vysl = switch (operator) {
            case "+" -> vysl = first + second;
            case "-" -> vysl = first - second;
            case "*" -> vysl = first * second;
            case "/" -> vysl = first / second;
            default -> 0;
        };
        System.out.println("vysledok je:" + vysl);
    }
}
