package cz.sda.java8.fundamentalcoding;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task18 {
    /*Write an application that reads a text from the user (type String) and checks whether the
user sneezed, i.e. whether the text equals Ͽachooo!Ѐ with one or more letter "o" at the end
of the expression (so both 'acho!' and 'achooooooo!Ѐ are correct). Hint: use a regular
expression with the appropriate quantifier.*/

    public static void main(String[] args) {
        String userInput = InputUtil.nextLine();

        Pattern pattern = Pattern.compile("acho+!");
        Matcher matcher = pattern.matcher(userInput);

        if (matcher.matches()) {
            System.out.println("Matched!");
        } else {
            System.out.println("No match found.");
        }
    }
}

