package cz.sda.java8.fundamentalcoding;

public class Task11 {
    public static void main(String[] args) {
        String text = "";
        String input = "";

        while (!input.equals("Enough!")) {
            input = InputUtil.nextLine();
            if (input.equals("")) {
                System.out.println("no text provided");
            } else if (input.length() > text.length()) {
                text = input;
            }

        }
        System.out.println(text);
    }
}
