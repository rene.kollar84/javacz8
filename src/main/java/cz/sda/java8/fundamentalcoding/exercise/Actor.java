package cz.sda.java8.fundamentalcoding.exercise;

import java.time.LocalDate;

public class Actor {

    private String name;
    private LocalDate dateOfBirth;

    public Actor(String name) {
        this.name = name;
    }

    public Actor(String name, LocalDate dateOfBirth) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }
}
