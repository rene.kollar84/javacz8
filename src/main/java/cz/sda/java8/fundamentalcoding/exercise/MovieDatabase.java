package cz.sda.java8.fundamentalcoding.exercise;

public class MovieDatabase {

    private Movie[] movies;//pole zvane movies zlozene z typu movie

    public MovieDatabase(Movie[] movies) {
        this.movies = movies;

    }

    public void setMovies(Movie[] movies) {
        this.movies = movies;
    }

    public Movie findMovieByName(String name) {
        for (Movie movie : movies) {
            if (name.equals(movie.getName())) {
                return movie;
            }

        }
        return null;//null je uplne nic mozes vratit miesto objektu ale v navrate musis osetrit
    }

    public void printMoviesForKids() {

        for (Movie movie : movies) {
            if (movie.getForKids()) {

                System.out.println(movie.getName());
            }

        }

    }

    public Movie mostMoneyPerMinute() {

        Movie maxMPM = null;

          for (Movie movie : movies) {

            if (maxMPM == null) {
                maxMPM = movie;
            } else if (movie.getGrosses() / movie.getDuration().toMinutes() > maxMPM.getGrosses() / maxMPM.getDuration().toMinutes()) {
                maxMPM = movie;
            }
        }
            return maxMPM;
    }
}
