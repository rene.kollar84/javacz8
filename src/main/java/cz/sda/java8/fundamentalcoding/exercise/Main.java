package cz.sda.java8.fundamentalcoding.exercise;

import java.time.Duration;
import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Director jiriMenzel = new Director("Jiri Menzel");
        Director stevenSpielberg = new Director("Steven Spielberg");

        Movie ostreSledovaneVlaky = new Movie("Ostre sledovane vlaky", 1966, Duration.ofHours(1).ofMinutes(32), 1500000, jiriMenzel,false);
        Movie slavnostiSnezenek = new Movie("Slavnosti snezenek", 1983, Duration.ofHours(1).ofMinutes(23), 2500000, jiriMenzel,false);
        Movie jurskyPark = new Movie("Jursky park", 1993, Duration.ofHours(2).ofMinutes(7), 65000000, stevenSpielberg,true);

        Actor vaclavNeckar = new Actor("Vaclav Neckar", LocalDate.of(1943, 10, 23));
        Actor josefSomr = new Actor("Josef Somr", LocalDate.of(1934, 4, 15));
        ostreSledovaneVlaky.addActor(vaclavNeckar);
        ostreSledovaneVlaky.addActor(josefSomr);

        Movie[] movies = new Movie[]{ostreSledovaneVlaky, slavnostiSnezenek, jurskyPark};
        MovieDatabase moviesDB = new MovieDatabase(movies);
        moviesDB.printMoviesForKids();
        System.out.println(moviesDB.mostMoneyPerMinute().getName());
        System.out.println(moviesDB.mostMoneyPerMinute().toString());
    }
}

