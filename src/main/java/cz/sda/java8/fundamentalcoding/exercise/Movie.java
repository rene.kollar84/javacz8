package cz.sda.java8.fundamentalcoding.exercise;

import java.time.Duration;
import java.util.HashSet;
import java.util.Set;

public class Movie {
    private String name;
    private int year;
    private Duration duration;
    private long grosses;
    private Director director; // director je nami vytvorena trieda Director
    private boolean forKids;
    private Set<Actor> actors; //datova struktura

    public Movie(String name, int year, Duration duration, long grosses, Director director, boolean forKids) {
        this.name = name;
        this.year = year;
        this.duration = duration;
        this.grosses = grosses;
        this.director = director;
        this.forKids = forKids;
        this.actors = new HashSet<>();//vytvor novy prazdny set

    }

    public void addActor(Actor actor) {
        actors.add(actor);

    }

    public String getName() {
        return this.name;
    }

    public boolean getForKids() {
        return this.forKids;


    }

    public Duration getDuration() {
        return duration;
    }

    public long getGrosses() {
        return grosses;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", director=" + director +
                '}';
    }
}
