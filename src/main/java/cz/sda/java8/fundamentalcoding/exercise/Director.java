package cz.sda.java8.fundamentalcoding.exercise;

public class Director {
    private String name;

    public Director(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
