package cz.sda.java8.fundamentalcoding;

public class Task6 {
    public static void main(String[] args) {

        int i = InputUtil.nextInt();
        float sum = 0;
        for (int j = 1; j <= i; j++) {
            sum += 1f / j;
        }
        System.out.println(sum);
    }
}
