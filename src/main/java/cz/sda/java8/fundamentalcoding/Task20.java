package cz.sda.java8.fundamentalcoding;

import java.util.Random;

public class Task20 {
    public static void main(String[] args) {
        Random random = new Random();
        int randomNumber = random.nextInt(100);

        int number = -1;
        while (randomNumber != number) {
            System.out.println("Enter number: ");
            number = InputUtil.nextInt();

            if (number > randomNumber) {
                System.out.println("too much");
            } else if (number < randomNumber) {
                System.out.println("not enough");
            } else {
                System.out.println("Congratulations!");
            }
        }
    }
}
