package cz.sda.java8.fundamentalcoding;

public class Task13 {

    //Write an application that "stutters", that is, reads the user's text (type String), and prints
    //the given text, in which each word is printed twice.
    //For example, for the input: "This is my test" the application should print "This This is is
    //my my test test"

    public static void main(String[] args) {

        String text = "";
        System.out.println("Enter your text here: ");
        String input = InputUtil.nextLine();

        String[] myArray = input.split(" ");
        for (String item : myArray) {
            System.out.print(item + " " + item + " ");


        }
        System.out.print("\b"); //Odeslani backspace na konzoli


    }
}

