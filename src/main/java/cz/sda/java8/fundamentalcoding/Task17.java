package cz.sda.java8.fundamentalcoding;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Task17 {
    public static void main(String[] args) {
        String userInput = InputUtil.nextLine();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.MM.yyyy");
        LocalDate date = LocalDate.parse(userInput, formatter);
        LocalDate now = LocalDate.now();

        Period period = Period.between(now, date);
        System.out.println("Days until next course is: " + period.getDays() + " days.");
    }
}
