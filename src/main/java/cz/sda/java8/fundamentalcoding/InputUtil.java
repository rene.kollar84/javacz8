package cz.sda.java8.fundamentalcoding;

import java.util.Scanner;

public class InputUtil {
    private static Scanner scanner = new Scanner(System.in);

    public static int nextInt() {
        return scanner.nextInt();
    }

    public static float nextFloat() {
        return scanner.nextFloat();
    }

    public static String nextString() {
        return scanner.next();
    }


    public static String nextLine() {
        return scanner.nextLine();
    }

    public static Character nextChar() {
        //return text.length() == 1? text.charAt(0) : null; TERNALNI VYRAZ
        String text = nextLine();
        if (text.length() == 1) {
            return text.charAt(0);
        } else {
            return null;
        }
    }


}


