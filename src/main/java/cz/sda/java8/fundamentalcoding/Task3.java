package cz.sda.java8.fundamentalcoding;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Zadej kvadratickou rovnici : ");
        System.out.print(" \n a = ");
        float a = scanner.nextFloat();
        System.out.print(" \n b = ");
        float b = scanner.nextFloat();
        System.out.print("\n c = ");
        float c = scanner.nextFloat();
//        System.out.println("Kvadratická rovnice je tedy: " + a + "x^2 + " + b + "x + " + c + " = 0");
        System.out.printf("Kvadratická rovnice je tedy: %sx^2 + %sx + %s = 0 \n", a, b, c);
        double diskriminant = getDiskriminant(a, b, c);
        if (diskriminant < 0) {
            System.out.println("Rovnice nemá řešení");
            return;
        }
        diskriminant = Math.sqrt(diskriminant);
        double koren1 = (-b + diskriminant) / (2 * a);
        double koren2 = (-b - diskriminant) / (2 * a);
        System.out.println(String.format("Koren 1 = %.2f", koren1));
        System.out.println(String.format("Koren 2 = %.2f", koren2));

        boolean z1 = (a * koren1 * koren1 + b * koren1 + c) < 0.00000000001;
        boolean z2 = (a * koren2 * koren2 + b * koren2 + c) < 0.00000000001;
        if (z1 && z2) {
            System.out.println("Správně vypočteno");
        } else {
            System.out.println("Vloudila se chybička");
        }
    }

    private static float getDiskriminant(float a, float b, float c) {
        return b * b - 4 * a * c;
    }
}
