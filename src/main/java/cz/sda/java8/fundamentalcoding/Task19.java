package cz.sda.java8.fundamentalcoding;

public class Task19 {
    public static void main(String[] args) {
        Author author = new Author("Machu", "Czech", "Jakub");
        Poem poem1 = new Poem(10, author);
        Poem poem2 = new Poem(20, author);

        Author author1 = new Author("Novak", "Slovakia", "Pavel");
        Poem poem3 = new Poem(23, author1);

//        Poem[] poems = new Poem[3];
//        poems[0] = poem1;
//        poems[1] = poem2;
//        poems[2] = poem3;

        Poem[] poems = new Poem[]{poem1, poem2, poem3};

        Poem longestPoem = poems[0];
        for (Poem poem : poems) {
            if (longestPoem.getStropheNumbers() < poem.getStropheNumbers()) {
                longestPoem = poem;
            }
        }

        Author authorOfLongestPoem = longestPoem.getCreator();
        System.out.println("The author of the longest poem is: " + authorOfLongestPoem.getSurName());
    }
}
