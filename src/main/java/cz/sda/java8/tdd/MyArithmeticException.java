package cz.sda.java8.tdd;

public class MyArithmeticException extends RuntimeException{
    public MyArithmeticException(String message) {
        super(message);
    }
}
