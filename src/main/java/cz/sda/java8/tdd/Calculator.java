package cz.sda.java8.tdd;

public class Calculator {
    public int add(int a,int b){
        return a+b;
    }

    public int sub(int a,int b){
        return a-b;
    }

    public int max(int a,int b){
        return Math.max(a,b);
    }

    public float div(int a,int b){
        //return (float)a/b;
        if(b==0){
            throw new MyArithmeticException("Deleni 0");
        }
        return 1F*a/b;
    }
}
