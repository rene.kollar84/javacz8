package cz.sda.java8.excercises;

public class ConditionalStatements {
    public static void main(String[] args) {
        int var = 12;
        if (var % 2 == 0) {
            System.out.println("It is an even number!");
        }
        if (var + 1 % 2 != 0) {
            System.out.println("it is an odd number!");
        }
        if (var < 0) {
            System.out.println("Value is less than zero!");
        } else if (var - var == 0) {
            System.out.println("Value is equal to zero!");
        } else {
            System.out.println("Value is greater than zero!");

        }
    }
}
