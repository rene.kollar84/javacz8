package cz.sda.java8.excercises;

public class DivisionByFiveExample {
    public static void main(String[] args) {
        for (int i = 100; i >0; i = i - 5){
            System.out.println(i);
        }
    }
}
