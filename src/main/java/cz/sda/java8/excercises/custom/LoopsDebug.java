package cz.sda.java8.excercises.custom;

public class LoopsDebug {
    public static void main(String[] args) {
        for (int outerForIndex = 0; outerForIndex < 10; outerForIndex++) {
            for (int innerForIndex = 0; innerForIndex < 10; innerForIndex++) {
                System.out.print(outerForIndex);
                System.out.print(innerForIndex);
                System.out.println();
            }
        }
    }
}
