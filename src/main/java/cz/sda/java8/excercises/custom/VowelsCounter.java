package cz.sda.java8.excercises.custom;

import java.util.Scanner;

public class VowelsCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String sentence = scanner.nextLine();

        char[] chars = sentence.toLowerCase().toCharArray();

        int a = 0;
        int e = 0;
        int i = 0;
        int o = 0;
        int u = 0;
        int y = 0;

        for (char c : chars) {
            switch (c) {
                case 'a':
                    a++;
                    break;
                case 'e':
                    e++;
                    break;
                case 'i':
                    i++;
                    break;
                case 'o':
                    o++;
                    break;
                case 'u':
                    u++;
                    break;
                case 'y':
                    y++;
                    break;
                default:
                    break;
            }
        }

        System.out.println("a: " + a);
        System.out.println("e: " + e);
        System.out.println("i: " + i);
        System.out.println("o: " + o);
        System.out.println("u: " + u);
        System.out.println("y: " + y);
    }
}
