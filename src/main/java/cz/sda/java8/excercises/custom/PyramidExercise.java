package cz.sda.java8.excercises.custom;

import java.util.Scanner;

public class PyramidExercise {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        customPyramid(scanner);
        repeatPrinting(scanner);
    }

    private static void repeatPrinting(Scanner scanner) {
        while (true) {
            System.out.println("Do you wish to do it again? (y/n)");
            String userInput = scanner.next();

            if (userInput.equals("y") || userInput.equals("Y")) {
                customPyramid(scanner);
            } else if (userInput.equals("n") || userInput.equals("N")) {
                break;
            } else {
                System.out.println("Incorrect input!");
            }
        }
    }


    private static void customPyramid(Scanner scanner) {
        System.out.println("Input pyramid's height:");
        int pyramidHeight = scanner.nextInt();

        System.out.println("Input sky character:");
        String skyMaterial = scanner.next();

        System.out.println("Input pyramid character:");
        String pyramidMaterial = scanner.next();

        //Takes care of lines
        for (int i = 0; i < pyramidHeight; i++) {
            int sky = pyramidHeight - 1 - i;
            int pyramid = i + 1;

            //Left Side
            //Generates sky
            printCharNTime(skyMaterial, sky);
            //Generates pyramid
            printCharNTime(pyramidMaterial, pyramid);

            //Right Side
            //Generates sky
            printCharNTime(pyramidMaterial, pyramid);
            //Generates pyramid
            printCharNTime(skyMaterial, sky);

            //Newline
            System.out.println();
        }
    }

    static void printCharNTime(String c, int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(c);
        }
    }

    //    Simple pyramid without user's custom characters and with ** top
//    private static void printSimplePyramid(Scanner scanner) {
//        System.out.println("Input pyramid's height:");
//        int pyramidHeight = scanner.nextInt();
//
//        for (int i = 0; i < pyramidHeight; i++) {
//            int sky = pyramidHeight - 1 - i;
//            int blocks = i+1;
//
//            //Left Side
//            for (int j = 0; j < sky; j++) {
//                System.out.print("-");
//            }
//            for (int j = 0; j < blocks; j++) {
//                System.out.print("*");
//            }
//
//            //Right Side
//            for (int j = 0; j < blocks; j++) {
//                System.out.print("*");
//            }
//            for (int j = 0; j < sky; j++) {
//                System.out.print("-");
//            }
//
//            //Newline
//            System.out.println();
//        }
//    }


}
