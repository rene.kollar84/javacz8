package cz.sda.java8.excercises.custom;

public class ArraysExercises {
    public static void main(String[] args) {
        int[] integers = {1, 3, 5, 2, 5, 6, 7, 4, 9, 7};
        allNumbers(integers);
        prvnich6(integers);
    }

    private static void allNumbers(int[] numbers) {
        for (int number : numbers) {
            System.out.print(number + " ");
        }
        System.out.println();
    }

    private static void prvnich6( int[] integers) {
        for (int i = 0; i < 6; i++) {
            System.out.print (integers[i] + " ");
        }
        System.out.println();
    }
}
