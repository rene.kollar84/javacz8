package cz.sda.java8.excercises;

public class JavaFundamentalsAdditionalExcercises {
public static void main(String[] args) {
//// Task 2
    final int finalInt = 5;
    final String finalString = "This is the test!";
    final boolean finalBoolean = true;
    final long finalLong = 12L;
    final float finalFloat = 5F;
    System.out.println(finalInt);
    System.out.println(finalString);
    System.out.println(finalBoolean);
    System.out.println(finalLong);
    System.out.println(finalFloat);

    /*finalInt = 6; nefunguje/spravi error*/



}
}
