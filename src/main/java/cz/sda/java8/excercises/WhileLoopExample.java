package cz.sda.java8.excercises;

public class WhileLoopExample {
    public static void main(String[] args) {
        int i = 5;
        while (i <= 100) {
            System.out.println(i++);

        }
    }
}
