package cz.sda.java8.excercises;

public class CastExample {
    public static void main(String[] args) {
        int intVar1 = 12;
        int intVar2 = 15;
        short shortSum = (short) (intVar1 + intVar2);
        System.out.println(shortSum);
        System.out.println(shortSum++);
        byte byteSum = (byte) (intVar1 + intVar2);
        System.out.println(byteSum);
        System.out.println(++byteSum);
        int a = 1;
        int b = 2;
        if (a == 1 || ++b == 3) {
            System.out.println(b);
        } // zastaví hned po první podmínce a další už nečekuje tedy ani neprovádí předepsané změmy (nezvětší b o 1)
        if (a == 1 | ++b == 100) {
            System.out.println(b);
        } // logická hodnota zůstane kvůli první podmínce true ale projede a vykoná i druhou podmínku tedy b zvětší ze 2 na 3
        int c = 0;
        if (a == 1 || a / c > 0) {
            System.out.println("true");
        }
        String g = null;
        if (g != null && g.isEmpty()) {
            System.out.println("g is empty");
        }
    }
}
