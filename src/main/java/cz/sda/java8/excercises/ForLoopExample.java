package cz.sda.java8.excercises;

public class ForLoopExample {
    public static void main(String[] args) {
        for (int i = 5; i <= 100; i++) {
            System.out.println(i);
        }
    }
}
