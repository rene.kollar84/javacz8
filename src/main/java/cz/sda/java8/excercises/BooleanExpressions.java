package cz.sda.java8.excercises;

public class BooleanExpressions {
    public static void main(String[] args) {
        System.out.println(2 * 2 >= 3 && 1 == 1);//true
        System.out.println((12 / 4) != 3);//false
        System.out.println(12 % 4 != 0);//false
        System.out.println(3 != 4 || (2 + 3 > 5));//true

        float a = 2;
        int b = 5;
        float v = b / a;//2.5
        int c = 2;
        int i = b / c;//2
        int i1 = b % c;//1
        //14/5=2
        //(float)14/5=2.8
        //14%5=4;
    }
}
