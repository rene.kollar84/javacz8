package cz.sda.java8.excercises;

public class Calculator {
    public static void main(String[] args) {
        int a = 5;
        int b = 10;
        int c = 8;
        int result1 = a - b - c;
        System.out.println("a - b - c = " + result1);

        long d = 85L;
        long e = 18L;
        long f = 9L;
        long result2 = d * e / f;
        System.out.println("d * e / f =" + result2);
    }
}
