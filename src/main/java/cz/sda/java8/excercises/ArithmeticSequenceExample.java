package cz.sda.java8.excercises;

public class ArithmeticSequenceExample {
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 5; i <= 460 * 2 + 1; i = i + 2){
            sum = sum + i;
        }
        System.out.println("Sum =" + sum);
    }
}
