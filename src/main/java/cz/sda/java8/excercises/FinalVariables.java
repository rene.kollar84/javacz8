package cz.sda.java8.excercises;

public class FinalVariables {
    public static void main(String[] args) {
        final int finalInt= 5;
        final String finalString = "This is the test!";
        final boolean finalBolean = true;
        final long finalLong = 12L;
        final float finalFloat = 5F;
        System.out.println(finalInt);
        System.out.println(finalString);
        System.out.println(finalBolean);
        System.out.println(finalLong);
        System.out.println(finalFloat);

       /*finalInt = 6; není možné */

    }
}
