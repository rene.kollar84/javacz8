package cz.sda.java8.advancedf.inheriance;

public class Cylinder extends Circle {

    private float h;

    public float getH() {
        return h;
    }

    public void setH(float h) {
        this.h = h;
    }

    @Override
    public double getArea() {
        return super.getArea() * 2 + Math.PI * getR() * 2 * getH();
    }

    public double getVolume() {
        return super.getArea() * getH();
    }
}
