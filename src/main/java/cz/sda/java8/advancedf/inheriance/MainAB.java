package cz.sda.java8.advancedf.inheriance;

public class MainAB {
    public static void main(String[] args) {
        B b = new B();
        b.setI(10);
        System.out.println(b);
        System.out.println(new C());
    }
}
