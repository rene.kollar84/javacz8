package cz.sda.java8.advancedf.inheriance;

public class A {
    private int i;

    public void setI(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }

    @Override
    public String toString() {
        return "I am A";
    }
}
