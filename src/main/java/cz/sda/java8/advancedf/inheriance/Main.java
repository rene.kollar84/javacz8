package cz.sda.java8.advancedf.inheriance;

public class Main {
    public static void main(String[] args) {
        Circle c= new Circle();
        c.setR(1);

        Cylinder cy = new Cylinder();
        cy.setR(1);
        cy.setH(10);

        System.out.println(c.getArea());

        System.out.println(cy.getArea());
        System.out.println(cy.getVolume());
    }
}
