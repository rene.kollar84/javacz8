package cz.sda.java8.advancedf.threads;

import java.util.concurrent.*;

public class MainExecutorService {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newScheduledThreadPool(1);
        executorService.submit(() -> {
            System.out.println(Thread.currentThread().getName());
            for (int i = 0; i < 100; i++) {
                System.out.print(i + ",");
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        executorService.submit(() -> {
            System.out.println(Thread.currentThread().getName());
            for (int i = 1000; i < 1100; i++) {
                System.out.print(i + ",");
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        Future<Integer> submit = executorService.submit(() -> {
            int sum = 0;
            for (int i = 0; i < 100; i++) {
                sum += i;
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            return sum;
        });
        while (!submit.isDone()) {
            System.out.println("Cekam na vysledek");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println(submit.get());
        executorService.shutdown();
    }
}
