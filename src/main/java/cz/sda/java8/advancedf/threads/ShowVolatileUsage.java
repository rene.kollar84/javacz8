package cz.sda.java8.advancedf.threads;

import java.util.Scanner;

public class ShowVolatileUsage {
     volatile int i=0;//vzdy aktualni hodnota ve vsech vlaknech, jinak neni zaroceno, vlakna maji kopii promenne

    public static void main(String[] args) {

        ShowVolatileUsage main = new ShowVolatileUsage();
        main.init();
        new Scanner(System.in).nextLine();
        System.exit(0);
    }
    void init(){
        Thread thread = new Thread(() -> {
            while(true) {
                i++;
                if(i%2==0){
                    System.out.println("i is now:"+i);
                }
                sleep();
            }
        });
        thread.start();

        Thread thread1 = new Thread(() -> {
            while (true) {
                if (i % 2 == 0) {
                    System.out.println(i);
                }
                sleep();
            }
        });
        thread1.start();
    }
    void sleep(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {

        }
    }
}
