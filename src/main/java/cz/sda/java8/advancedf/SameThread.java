package cz.sda.java8.advancedf;

public class SameThread {
    public static void main(String[] args) {
        Runnable p = () -> System.out.println(Thread.currentThread().getName());
        new Thread(p).start();
        new Thread(p).start();
        new Thread(p).start();
        new Thread(p).start();
        A a = new A(1) {

            @Override
            void aa() {

            }
        };
    }


}

abstract class A {
    int i;

    public A(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
    //nemusi obsahovat
    abstract void aa();
}
