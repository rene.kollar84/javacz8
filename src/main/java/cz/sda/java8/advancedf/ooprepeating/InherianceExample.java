package cz.sda.java8.advancedf.ooprepeating;

public class InherianceExample {
    public static void main(String[] args) {
        B b = new B(10);
    }
}

class A{
    private int i;

    public A(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }
}

class B extends A{
    public B(int i) {
        super(i);
        System.out.println(getI());
    }
}
