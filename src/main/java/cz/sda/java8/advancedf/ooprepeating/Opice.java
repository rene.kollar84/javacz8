package cz.sda.java8.advancedf.ooprepeating;

public class Opice extends Zvire{
    public Opice(int vaha) {
        super(vaha);
    }

    @Override
    public boolean isMasozravec() {
        return false;
    }
}
