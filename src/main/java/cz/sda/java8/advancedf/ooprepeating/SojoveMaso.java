package cz.sda.java8.advancedf.ooprepeating;

public class SojoveMaso extends Maso{
    public SojoveMaso(int kJ, String zeme, int hmotnost) {
        super(kJ, zeme, hmotnost);
    }

    @Override
    public final String getType() {
        return "V";
    }
}
