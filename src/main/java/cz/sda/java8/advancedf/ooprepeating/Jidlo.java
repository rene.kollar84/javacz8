package cz.sda.java8.advancedf.ooprepeating;

public abstract class Jidlo implements Vazitelny{
    private final int hmotnost;
    private int kJ;
    private String zeme;

    public Jidlo(int kJ, String zeme,int hmotnost) {
        this.kJ = kJ;
        this.zeme = zeme;
        this.hmotnost=hmotnost;
    }

    @Override
    public int getHmotnost() {
        return hmotnost;
    }

    private static int count=0;

    public int getkJ() {
        return kJ;
    }

    public void setkJ(int kJ) {
        this.kJ = kJ;
    }

    public static int getCount(){
        return count;
    }
    //chceme aby metoda vratila typ jidla, napr vegetarianska, masozrava
    public abstract String getType();

    public void printPotentionalAnimals(){
        switch (getType()){
            case "V" ->{
                System.out.println("Opice,Jelen");
            }
            case "M" ->{
                System.out.println("Lev, Tulen");
            }
        }
    }
}
