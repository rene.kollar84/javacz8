package cz.sda.java8.advancedf.ooprepeating;

import java.io.Serializable;

public abstract class Zvire implements Vazitelny, Serializable {//serializable je nejake rozhrani, tady jenom
    //ukazujeme ze trida muze implementovat vice rozhrani
    private String nazev;
    private String vek;
    private int vaha;
    public abstract boolean isMasozravec();

    public Zvire(int vaha) {
        this.vaha = vaha;
    }

    @Override
    public int getHmotnost() {
        return vaha;
    }
}
