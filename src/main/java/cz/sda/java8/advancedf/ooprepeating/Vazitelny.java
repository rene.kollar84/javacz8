package cz.sda.java8.advancedf.ooprepeating;

public interface Vazitelny {
    int getHmotnost();

    default void printHmotnost(){
        System.out.println("Hmotnost je "+getHmotnost());
    }
}
