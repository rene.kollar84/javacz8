package cz.sda.java8.advancedf.ooprepeating;

public class Krajta extends Zvire{
    public Krajta(int vaha) {
        super(vaha);
    }

    @Override
    public boolean isMasozravec() {
        return true;
    }
}
