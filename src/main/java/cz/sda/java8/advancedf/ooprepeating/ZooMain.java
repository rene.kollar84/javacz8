package cz.sda.java8.advancedf.ooprepeating;

import java.util.ArrayList;
import java.util.List;

public class ZooMain {
    public static void main(String[] args) {

        List<Jidlo> potrava = new ArrayList<>();
        potrava.add(new MrazenyTunak(300, "SK", 30));
        potrava.add(new Ovoce(10, "CR",20));
        potrava.add(new Maso(300, "SK",30));
        potrava.add(new MrazenaRyba(300, "SK",44));

        //nemuzeme vytvorit instanci abstraktni tridy
        // potrava.add(new Jidlo(200,"SK"));

        potrava.forEach(a -> {
            if (a instanceof Maso) {
                Maso m = (Maso) a;
                System.out.println("Totot jidlo je tucne? " + m.jeTucne());
            }
            System.out.println(a.getType());
            a.printPotentionalAnimals();
            print(a);
            System.out.println();
        });

        List<Vazitelny> nakladniAuto = new ArrayList<>();
        nakladniAuto.add(new Maso(10,"CZ",30));
        nakladniAuto.add(new Opice(60));
        nakladniAuto.add(new Ovoce(30,"SK",50));
        System.out.println("Celkova vaha nakladu je "+getHmotnost(nakladniAuto));
    }

    public static int getHmotnost(List<Vazitelny> polozky) {
        int celkovaHmotnost = 0;
        for (Vazitelny polozka : polozky) {
            polozka.printHmotnost();
            celkovaHmotnost += polozka.getHmotnost();
        }
        return celkovaHmotnost;
    }

    public static void print(Jidlo j) {
        System.out.println(j.getType());

    }
    //pretizeni je napsani metody se stejnym nazvem ale jinymi argumety
    public static long add(int a,int b){
        return a+b;
    }

    public static long add(int a,long b){
        return a+b;
    }
}
