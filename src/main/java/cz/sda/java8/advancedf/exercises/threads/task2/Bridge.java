package cz.sda.java8.advancedf.exercises.threads.task2;

public class Bridge {
    public synchronized void driveThrough(Car car){
        System.out.println("Car:"+car+" is on bridge");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {

        }
        System.out.println("Car:"+car+" leaving bridge");
    }

    public void driveThrough2(Car car){
        synchronized (this) {
            System.out.println("Car:" + car + " is on bridge");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {

            }
            System.out.println("Car:" + car + " leaving bridge");
        }
    }
}
