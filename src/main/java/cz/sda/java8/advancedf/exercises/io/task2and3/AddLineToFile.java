package cz.sda.java8.advancedf.exercises.io.task2and3;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class AddLineToFile {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Zadej nazev");
        Path path = Path.of(scanner.nextLine());
        File file = path.toFile();
        if(!file.exists()){
            file.createNewFile();
        }
        System.out.println(" Co chces pridat?");
        String line = scanner.nextLine();
        Files.write(path,line.getBytes(), StandardOpenOption.APPEND);
        Files.write(path,new byte[]{'\n'}, StandardOpenOption.APPEND);
    }
}
