package cz.sda.java8.advancedf.exercises.generic.task4;

import cz.sda.java8.advancedf.inheriance.B;

public class Main {
    public static void main(String[] args) {
        Library<Book> bookLibrary = new Library<>();
        Library<Movie> movieLibrary = new Library<>();

        movieLibrary.addItem(new Movie());
        bookLibrary.addItem(new Book());
    }


}

class Book{

}

class Movie{

}
