package cz.sda.java8.advancedf.exercises.enumtask2;

public enum PackageSize {
    SMALL(10,100),
    MEDIUM(100,1000),
    LARGE(1000,10000);

    private int minSize;
    private int maxSize;

    PackageSize(int minSize, int maxSize) {
        this.minSize = minSize;
        this.maxSize = maxSize;
    }
    public static PackageSize getPackageSize(int size ){

        if (size<=SMALL.maxSize){
            return SMALL;
        }
        if (size> MEDIUM.minSize && size<=MEDIUM.maxSize){
            return MEDIUM;
        } else return LARGE;
    };

}
