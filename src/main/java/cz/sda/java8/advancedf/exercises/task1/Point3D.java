package cz.sda.java8.advancedf.exercises.task1;

public class Point3D extends Point2D {
    public static void main(String[] args) {

        Point2D p = new Point3D(1, 1, 1);
        Point3D p1 = new Point3D(2, 1, 1);
        Object p3 = new Point3D(3, 1, 1);

        System.out.println(p);
        System.out.println(p1);
        System.out.println(p3);

        Point2D[] pArr=new Point2D[5];

        for(int i=0;i<pArr.length;i++){
            System.out.println(pArr[i]);
        }

        for(int i=0;i<pArr.length;i++){
            if (i % 2 == 0) {
                pArr[i]=new Point3D(i,i,i);
            }else{
                pArr[i]=new Point2D(i,i);
            }
        }

        for(int i=0;i<pArr.length;i++){
            System.out.println(pArr[i]);
        }
    }

    private float z;

    public Point3D(float x, float y, float z) {
        super(x, y);//volanie rodicovskeho konstruktora vzdy ked dedim
        this.z = z;
    }

    public float getZ() {
        return z;
    }

    public float[] getXYZ() {

        return new float[]{getX(), getY(), z};
    }

    public void setZ(float z) {
        this.z = z;
    }

    public void setXYZ(float x, float y, float z) {
        this.z = z;
        setX(x);
        setY(y);
    }
    @Override
    public String toString() {
        return "("+getX()+","+getY()+","+z+")";
    }
}
/*Point3D class
Using the Point2D class implement the Point3D class. It should extend the
Point2D class. It should contain:
private 􀂧oat 􀂦eld: z
three-arguments constructor: float x , float y , float z
getter method which will be responsible for returning the z 􀂦eld value
getXYZ method which will return x , y , z values as three-element array
setter method which will be responsible for setting the z 􀂦eld value
setXYZ method which will be responsible for setting x , y , z
toString method which should return string in the following format: (x,
y, z)
Please provide an example usage of above implementation.*/