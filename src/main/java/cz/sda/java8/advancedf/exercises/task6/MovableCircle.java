package cz.sda.java8.advancedf.exercises.task6;

public class MovableCircle implements Movable{
    int polomer;
    int x;
    int y;
    int xSpeed;
    int ySpeed;

    public MovableCircle(int polomer, int x, int y, int xSpeed, int ySpeed) {
        this.polomer = polomer;
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    public MovableCircle(int polomer, int x, int y) {
        this.polomer = polomer;
        this.x = x;
        this.y = y;
        this.xSpeed = 1;
        this.ySpeed = 1;
    }

    public int getPolomer() {
        return polomer;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getxSpeed() {
        return xSpeed;
    }

    public int getySpeed() {
        return ySpeed;
    }

    @Override
    public void moveUp() {
        y -= ySpeed;

    }

    @Override
    public void moveDown() {
        y += ySpeed;

    }

    @Override
    public void moveLeft() {
        x -= xSpeed;

    }

    @Override
    public void moveRight() throws OutOfScreenException {
        if (x + xSpeed + polomer > MAX_X) {
            throw new OutOfScreenException("Kruh se nevleze");
        }
        x += xSpeed;

    }

    @Override
    public String getActualPosition() {
        return String.format("Kruh x = %s, y = %s", x, y);
    }
}
