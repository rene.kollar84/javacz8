package cz.sda.java8.advancedf.exercises.generic.task2and3;

import java.util.function.Predicate;

public class Main {
//    Design the countIf generic method wich, based on an array of elements of any
//    type will count the number of elements meeting the condition using an
//    functional interface. Any interface implemented anonymously can be a function.

    public static void main(String[] args) {
        Integer[] integers = {1, 2, 3, 4, 5, 6};

        countIf(integers, new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return integer % 2 ==0;
            }
        });

        //stejne jako na radku 13-18 ale kratsi
        int i = countIf(integers, p -> p % 2 == 0);
        System.out.println(i);

        String[] strings = {"a", "aa", "bbb", "a"};
        countIf(strings, p -> p.length() == 1);
        countIf(strings, p -> p.startsWith("b"));

        swap(1,2,strings);
    }

    static <T> int countIf(T[] pole, Predicate<T> p) {
        int count = 0;
        for (int i = 0; i < pole.length; i++) {
            if (p.test(pole[i])) {
                count++;
            }
        }
        return count;
    }

    //    Design the generic swap method, which will be responsible for swapping the
//    position of the selected elements of the array.
    static <T> void swap(int first, int second, T[] arr) {
        T t = arr[first];
        arr[first] = arr[second];
        arr[second] = t;

    }

//    static int countIf(String[] pole, Predicate<String> p){
//        int count=0;
//        for(int i=0;i<pole.length;i++){
//            if(p.test(pole[i])){
//                count++;
//            }
//        }
//        return count;
//    }
}
