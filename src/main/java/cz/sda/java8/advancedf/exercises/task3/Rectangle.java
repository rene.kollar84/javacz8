package cz.sda.java8.advancedf.exercises.task3;

public class Rectangle extends Shape {
    private double width;
    private double lenght;


    public Rectangle() {
        super(Colour.UNKNOWN, false);
        width = 1;
        lenght = 1;

    }

    public Rectangle(Colour colour, boolean shouldFill, double width, double lenght) {
        super(colour, shouldFill);
        this.width = width;
        this.lenght = lenght;
    }

    public double getWidth() {
        return width;
    }

    public double getLenght() {
        return lenght;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    @Override
    public double getArea() {
        return lenght * width;
    }

    @Override
    public double getPerimetr() {
        return 2 * (lenght + width);
    }

    @Override
    public String toString() {
        return String.format("Rectangle with width %s and length %s which is a subclass off %s",
                width, lenght, super.toString());
    }
}
