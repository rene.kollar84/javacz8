package cz.sda.java8.advancedf.exercises.coltask2and4;

public class Author {
    private String name;
    private String surname;
    private String gender;

    public Author(String name, String surname, String gender) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "Author{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (!name.equals(author.name)) return false;
        if (!surname.equals(author.surname)) return false;
        return gender != null ? gender.equals(author.gender) : author.gender == null;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        return result;
    }

    //    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Author author = (Author) o;
//
//        if (name != null ? !name.equals(author.name) : author.name != null) return false;
//        if (surname != null ? !surname.equals(author.surname) : author.surname != null) return false;
//        return gender.equals(author.gender);
//    }
//
//    @Override
//    public int hashCode() {
//        int result = name != null ? name.hashCode() : 0;
//        result = 31 * result + (surname != null ? surname.hashCode() : 0);
//        result = 31 * result + gender.hashCode();
//        return result;
//    }
}
