package cz.sda.java8.advancedf.exercises.task2;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class LectureWithLombok {
    @ToString.Exclude
    String specialization;

    //@Setter(AccessLevel.NONE)
    double sallary;



}
