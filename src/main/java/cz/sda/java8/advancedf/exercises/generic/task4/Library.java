package cz.sda.java8.advancedf.exercises.generic.task4;

import java.util.ArrayList;
import java.util.List;

//Create a class that will behave like a library for the following types of media:
//        books
//        newspapers
//        movies
//        Please provide a solution for generic types. For data collection, use any array or
//        Collection API class.
public class Library <E>{
    List<E> inList=new ArrayList<>();

    void addItem(E item){
        inList.add(item);
    }
}
