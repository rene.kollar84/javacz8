package cz.sda.java8.advancedf.exercises.io.task6;

import java.io.Serializable;

public class Movie implements Serializable {

        private String title;
        private String genre;
        private String director;
        private int yearOfRelease;

    public Movie(String title, String genre, String director, int yearOfRelease) {
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.yearOfRelease = yearOfRelease;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getDirector() {
        return director;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", genre='" + genre + '\'' +
                ", director='" + director + '\'' +
                ", yearOfRelease=" + yearOfRelease +
                '}';
    }
}
