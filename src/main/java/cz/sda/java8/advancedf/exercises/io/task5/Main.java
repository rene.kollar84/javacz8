package cz.sda.java8.advancedf.exercises.io.task5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {

        Path of = Path.of("users.csv");
        List<String> lines = Files.readAllLines(of);

        List<User> collect = lines.stream().map(line -> {
            String[] split = line.split(",");
            User user = new User(split[0], split[1], Integer.parseInt(split[2].trim()));
            return user;
        }).collect(Collectors.toList());

        collect.forEach(System.out::println);

        //alternativa
        List<User> collect1 = lines.parallelStream().map(User::new).collect(Collectors.toList());
        collect1.forEach(System.out::println);
    }

    public static class User{
        String name;
        String surname;
        int old;

        public User(String name, String surname, int old) {
            this.name = name;
            this.surname = surname;
            this.old = old;
        }

        public User(String line){
            String[] split = line.split(",");
            name=split[0];
            surname=split[1];
            old=Integer.parseInt(split[2].trim());

        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    ", old=" + old +
                    '}';
        }
    }
}
