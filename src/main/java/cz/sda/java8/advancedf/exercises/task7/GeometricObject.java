package cz.sda.java8.advancedf.exercises.task7;

public interface GeometricObject {
    double getPerimeter();
    double getArea();

}
