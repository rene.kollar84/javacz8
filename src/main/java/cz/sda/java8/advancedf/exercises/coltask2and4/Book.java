package cz.sda.java8.advancedf.exercises.coltask2and4;

import java.util.List;

public class Book {

    private String title;
    private double price;
    private int yearOfRelease;
    private List<Author> authorLists;
    private GenreEnum genre;

    public Book(String title, double price, int yearOfRelease, List<Author> authorLists, GenreEnum genre) {
        this.title = title;
        this.price = price;
        this.yearOfRelease = yearOfRelease;
        this.authorLists = authorLists;
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public List<Author> getAuthorLists() {
        return authorLists;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", price=" + price +
                ", yearOfRelease=" + yearOfRelease +
                ", authorLists=" + authorLists +
                ", genre=" + genre +
                '}';
    }

    public GenreEnum getGenre() {
        return genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (Double.compare(book.price, price) != 0) return false;
        if (yearOfRelease != book.yearOfRelease) return false;
        if (title != null ? !title.equals(book.title) : book.title != null) return false;
        if (authorLists != null ? !authorLists.equals(book.authorLists) : book.authorLists != null) return false;
        return genre == book.genre;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = title != null ? title.hashCode() : 0;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + yearOfRelease;
        result = 31 * result + (authorLists != null ? authorLists.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        return result;
    }

    public enum GenreEnum {
        SCIFI,
        FANTASY,
        BIOGRAFY
    }
}
