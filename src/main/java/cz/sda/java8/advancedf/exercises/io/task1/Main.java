package cz.sda.java8.advancedf.exercises.io.task1;

import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Zadej adresar");
        String dir = scanner.nextLine();
        File root = new File(dir);
        File[] list = root.listFiles();
        for (File l:list) {

            System.out.println((l.isDirectory()?"dir-":"file-")+l.getName());
        }
    }
}
