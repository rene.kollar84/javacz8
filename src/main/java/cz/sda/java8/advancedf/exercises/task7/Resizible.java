package cz.sda.java8.advancedf.exercises.task7;

public interface Resizible {
    void resize (double percentage);
}
