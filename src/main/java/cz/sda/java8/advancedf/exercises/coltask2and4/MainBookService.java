package cz.sda.java8.advancedf.exercises.coltask2and4;

import java.util.*;

public class MainBookService {

    public static void main(String[] args) {
        BookService service = new BookService();
        List<Author> authorsOfABook = new ArrayList<>();
        authorsOfABook.add(new Author("Robert", "Galbraith", null));
        Book book1 = new Book("The Heart Black Ink", 407d, 2022, authorsOfABook, Book.GenreEnum.FANTASY);
        service.addBook(book1);
        List<Author> authorsOfABook2 = new ArrayList<>();
        authorsOfABook2.add(new Author("Karel Jaromír", "Erben", "male"));
        Book book2 = new Book("Kytice", 65.30d, 1926, authorsOfABook2, Book.GenreEnum.SCIFI);
        service.addBook(book2);
        List<Book> allOfGenre = service.getAllOfGenre(Book.GenreEnum.FANTASY);
        allOfGenre.forEach(book -> System.out.println(book));
        service.getAllBefore(1999).forEach(book -> System.out.println(book));

        System.out.println(service.findByPrice(true));
        System.out.println(service.findByPrice(false));

        //started task 4
        //generate one duplicity
        book1 = new Book("The Heart Black Ink", 407d, 2022, authorsOfABook, Book.GenreEnum.FANTASY);
        service.addBook(book1);
        List<Book> all = service.getAll();
        Set<KeyValue> unique = getUnique(all);
        System.out.println(all.size()> unique.size());


    }

    public static Set<KeyValue> getUnique(List<Book> all){
        Set<KeyValue> ret = new HashSet<>();
        for (Book b:all) {
            ret.add(new KeyValue(b.getGenre(),b.getTitle()));
        }
        return ret;
    }
    /*
        /*
        Based on Task 2 please implement a method which will be responsible for
    returing unique key-value pairs. The key should be represented as a book genre,
    value need to contain a title.
         */
    public static class KeyValue {
        Book.GenreEnum key;
        String value;

        public KeyValue(Book.GenreEnum key, String value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            KeyValue keyValue = (KeyValue) o;

            if (key != keyValue.key) return false;
            return value.equals(keyValue.value);
        }

        @Override
        public int hashCode() {
            int result = key.hashCode();
            result = 31 * result + value.hashCode();
            return result;
        }
    }
}
