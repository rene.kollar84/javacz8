package cz.sda.java8.advancedf.exercises.task2cLombock.task2c;

public class MovieBuilderMain {
    public static void main(String[] args) {
        //nebezpeci dlouheho konstruktoru je v prohozeni poradi argumentu
        Movie movie = new Movie("Spilberg","Matrix",2000,"scifi","imax");

        Movie movie2=new Movie();
        movie2.setDistributor("imax");
        movie2.setDirector("ja");
        movie2.setTitle("Slunce seno");
        movie2.setGenre("scifi");
        movie2.setYearOfRelease(2000);


        //Movie.builder() jsme nepsali je vygenerovan lombockem pomoci anotace @Builder nad tridou Movie
        Movie movie3 = Movie.builder().title("a").genre("b").distributor("c").build();
    }
}
