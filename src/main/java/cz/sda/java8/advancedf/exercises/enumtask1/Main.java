package cz.sda.java8.advancedf.exercises.enumtask1;

public class Main {
    public static void main(String[] args) {
        WeekDay monday = WeekDay.MONDAY;
        WeekDay otherDay = WeekDay.FRIDAY;
        System.out.println(monday == otherDay);
        System.out.println(monday == WeekDay.MONDAY);

        for (WeekDay actual:WeekDay.values()) {
            System.out.println(actual + " isHoliday="+actual.isHoliday()+" isWeekDay="+actual.isWeekDay());
        }

        System.out.println(WeekDay.MONDAY.whichIsGreater(true));
        System.out.println(WeekDay.MONDAY.whichIsGreater(false));
    }
}
