package cz.sda.java8.advancedf.exercises.task7;

public class ResizableCircle extends Circle implements Resizible{


    public ResizableCircle(double radius) {
        super(radius);
    }

    @Override
    public void resize(double percentage) {
        if(percentage<0){
            throw new IllegalArgumentException("Procento nesmí být nižší než nula.");
        }
       setRadius(getRadius()*percentage/100);
    }
}
