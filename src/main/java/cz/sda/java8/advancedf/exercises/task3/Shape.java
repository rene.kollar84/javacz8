package cz.sda.java8.advancedf.exercises.task3;

public abstract class Shape {
    /*
    Abstraktni trida znamena ze nemuzu vytvorit instanci (s vyjimkou anonymni tridy),
     pouze instanci potomka
     */

    /*
    To ye je trida abstraktni neznamena ze nemuze definovat promenne a konkretni metody
    ty pak muzou byt vyuyity v potomcich
     */
    protected Colour colour;
    protected boolean shouldFill;

    public Shape() {
        this.colour = Colour.UNKNOWN;
        this.shouldFill = false;
    }

    public Shape(Colour colour, boolean shouldFill) {
        this.colour = colour;
        this.shouldFill = shouldFill;
    }

    public Colour getColour() {
        return colour;
    }

    public boolean isShouldFill() {
        return shouldFill;
    }

    public void setColour(Colour colour) {
        this.colour = colour;
    }

    public void setShouldFill(boolean shouldFill) {
        this.shouldFill = shouldFill;
    }

    @Override
    public String toString() {
        String filledString = shouldFill ? "filled" : "notfilled";
        return String.format("Shape " +
                "with color of %s and %s", colour,filledString);
    }

    /*
    To ze oznacim metodu jako abstraktni znamena, ze neznam implementaci, tu ponecham na potomka
     */
    public abstract double getArea ();
    public abstract double getPerimetr();

}

