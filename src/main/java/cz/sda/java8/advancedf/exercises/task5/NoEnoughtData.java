package cz.sda.java8.advancedf.exercises.task5;

public class NoEnoughtData extends Exception{
    public NoEnoughtData(String msg){
        super(msg);
    }
}
