package cz.sda.java8.advancedf.exercises.task2;

public class Student extends Person {
    int yearOfStudy;
    double priceOfStudy;
    TypeOfStudy typeOfStudy = TypeOfStudy.UNDEFINED;

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public double getPriceOfStudy() {
        return priceOfStudy;
    }

    public TypeOfStudy getTypeOfStudy() {
        return typeOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public void setPriceOfStudy(double priceOfStudy) {
        this.priceOfStudy = priceOfStudy;
    }

    public void setTypeOfStudy(TypeOfStudy typeOfStudy) {
        this.typeOfStudy = typeOfStudy;
    }

    @Override
    public String toString() {
        return super.toString() + "\n Student{" +
                "yearOfStudy=" + yearOfStudy +
                ", priceOfStudy=" + priceOfStudy +
                ", typeOfStudy=" + typeOfStudy +
                '}';
    }

    @Override
    public String getType() {
        return "student";
    }

    public enum TypeOfStudy {
        PRESENT,
        COMBINED,
        UNDEFINED;
    }
}
