package cz.sda.java8.advancedf.exercises.io.task6;

import java.io.*;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Movie movie = new Movie("Matrix", "Scifi", "nevim", 1999);
        Movie movie2 = new Movie("Matrix2", "Scifi", "nevim", 2001);

        ObjectOutputStream objOutput = new ObjectOutputStream(new FileOutputStream("movies.db"));
        objOutput.writeObject(movie);
        objOutput.writeObject(movie2);

        List<Movie> movies = List.of(movie, movie2);
        objOutput.writeObject(movies);
        objOutput.close();

        ObjectInputStream objIn = new ObjectInputStream(new FileInputStream("movies.db"));
        Object readedObject = null;
        do {
            try {
                readedObject = objIn.readObject();
                if (readedObject instanceof Movie m) {//since java 17 we may use m
                    // Movie o1 = (Movie) o; before java 17
                    //some actions with movie
                }
                System.out.println(readedObject);
            } catch (Exception e) {
                break;
            }
        } while (true);

    }

}
