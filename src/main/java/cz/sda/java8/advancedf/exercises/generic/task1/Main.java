package cz.sda.java8.advancedf.exercises.generic.task1;

public class Main {
    //    Create the Pair class which, based on generic types, will allow to store any pair
//            of objects.
    public static void main(String[] args) {
        System.out.println(new Pair<String, String>("ahoj", "b"));
        System.out.println(new Pair<>(2,"aa"));
        Pair<String, Object> objectObjectPair = new Pair<>("2",2);
    }
}

class Pair<K, V> {
    K key;
    V value;

    public Pair() {
    }

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "key=" + key +
                ", value=" + value +
                '}';
    }
}
