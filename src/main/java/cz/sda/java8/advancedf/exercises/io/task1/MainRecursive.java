package cz.sda.java8.advancedf.exercises.io.task1;

import java.io.File;
import java.util.Scanner;

public class MainRecursive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Zadej adresar");
        String dir = scanner.nextLine();
        printDir(new File(dir), 0);
    }

    static void printDir(File root, int pM) {
        File[] files = root.listFiles();
        for (File f : files) {
            System.out.println(calculatorPM(pM)+(f.isDirectory() ? "dir-" : "file-") + f.getName());
            if (f.isDirectory()) {
                printDir(f,pM+2);
            }
        }
    }

    static String calculatorPM(int pM) {
        String res = "";
        for (int i = 0; i < pM; i++) {
            res += " ";
        } return res;
    }
}
