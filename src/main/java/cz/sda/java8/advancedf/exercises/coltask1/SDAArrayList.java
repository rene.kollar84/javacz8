package cz.sda.java8.advancedf.exercises.coltask1;

public class SDAArrayList<T>{
    private T[] ia;

    private int pos=0;

    public SDAArrayList() {
        ia= (T[]) new Object[16];
    }

    public void add(T value){
        if(pos==ia.length-1){
            Object[] objects = new Object[ia.length + 16];
            System.arraycopy(ia,0,objects,0,ia.length);
            ia= (T[]) objects;
        }
        ia[pos++]=value;
    }
    public T get(int index){
        return ia[index];
    }

    public void remove(T value){
        for(int i=0;i<ia.length;i++){
            if(value.equals(ia[i])){
                ia[i]=null;
                pos--;
            }
        }
    }

    public void display(){
        for (T t:ia) {
            if(t!=null){
                System.out.println(t);
            }
        }
    }
}
