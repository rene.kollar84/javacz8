package cz.sda.java8.advancedf.exercises.enumtask1;

public enum WeekDay {
    MONDAY(true),
    TUESDAY(true),
    WEDNESDAY(true),
    THURSDAY(true),
    FRIDAY(true),
    SATURDAY(false),
    SUNDAY(false);

    WeekDay(boolean weekDay) {
        this.weekDay = weekDay;
    }

    private boolean weekDay;

    boolean isWeekDay() {
        return this.weekDay;
    }

    boolean isHoliday() {
        return !weekDay;
    }

    //vrati den, ktrey je pred(accesor=true) anebo za(accesor=false) aktualnim dnem
    public WeekDay whichIsGreater(boolean accessor) {
        return switch (this) {

            case MONDAY -> accessor ? SUNDAY : TUESDAY;
            case TUESDAY -> accessor ? MONDAY : WEDNESDAY;
            case WEDNESDAY -> accessor ? TUESDAY : THURSDAY;
            case THURSDAY -> accessor ? WEDNESDAY : FRIDAY;
            case FRIDAY -> accessor ? THURSDAY : SATURDAY;
            case SATURDAY -> accessor ? FRIDAY : SUNDAY;
            case SUNDAY -> accessor ? SATURDAY : MONDAY;
        };
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
