package cz.sda.java8.advancedf.exercises.task6;

public class OutOfScreenException extends Exception {
    public  OutOfScreenException(String message){
        super(message);
    }
}
