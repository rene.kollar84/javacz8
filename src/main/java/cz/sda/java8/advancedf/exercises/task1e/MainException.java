package cz.sda.java8.advancedf.exercises.task1e;

public class MainException {
    public static double divide(double a, double b) throws CannotDivideBy0Exception {
        if(b == 0){
            throw new CannotDivideBy0Exception();
        }
        return a / b;
    }

    public static void main(String[] args) {
        try {
            System.out.println(divide(10, 0));
        } catch (CannotDivideBy0Exception e) {
            System.out.println("Nelze delit nulou!");
        }
    }
}

