package cz.sda.java8.advancedf.exercises.task1c;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidator {

    public String validateEmail(String input) {

        class Email {
            private String email;

            public Email(String email) {
                this.email = email;
            }

            public String getEmail() {

                return validate() ? email : "unknown";
            }

            public boolean validate (){
                Pattern pattern = Pattern.compile("[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(email);
                return matcher.find();
            }
        }
        return new Email(input).getEmail();
    }


}
