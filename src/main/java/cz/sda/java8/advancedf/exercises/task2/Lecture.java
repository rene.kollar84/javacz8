package cz.sda.java8.advancedf.exercises.task2;

public class Lecture extends Person{

    {
        System.out.println("Object lecture created");
    }
    Specialization specialization;
    double salary;

    public Lecture(Specialization specialization, double salary) {
        this.specialization = specialization;
        salary = salary;
    }

    public Lecture(String name, String address, Specialization specialization, double salary) {
        super(name, address);
        this.specialization = specialization;
        salary = salary;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public double getSalary() {
        return salary;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return super.toString() + "\n Lecture{" +
                "specialization=" + specialization +
                ", salary=" + salary +
                '}';
    }

    @Override
    public String getType() {
        return "lecture";
    }

    public enum Specialization {
        MATH,
        LANGUAGE,
        OTHER;
    }
}
