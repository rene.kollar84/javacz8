package cz.sda.java8.advancedf.exercises.task1composie;

public class Point3D {

    public static void main(String[] args) {
        Point2D p = new Point2D();
        //not possible point3d isnt point2d
        //Point2D point2D = new Point3D(p, 1);
    }

    private float z;
    private Point2D point;

    public Point3D(Point2D point2d, float z) {
        point = point2d;
        this.z = z;
    }

    void printPoints() {
        System.out.println(point.getX() + point.getY() + z);
    }

}
