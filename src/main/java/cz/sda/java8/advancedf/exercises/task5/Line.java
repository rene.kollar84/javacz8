package cz.sda.java8.advancedf.exercises.task5;

import cz.sda.java8.advancedf.exercises.task1.Point2D;

public class Line {
    private Point2D startPoint;
    private Point2D endPoint;

    public Point2D getStartPoint() {
        return startPoint;
    }

    public Point2D getEndPoint() {
        return endPoint;
    }

    public void setStartPoint(Point2D startPoint) {
        this.startPoint = startPoint;
    }

    public void setEndPoint(Point2D endPoint) {
        this.endPoint = endPoint;
    }

    public Line(Point2D startPoint, Point2D endPoint) {
        if(startPoint==null || endPoint ==null){
            throw new BadArguments();
        }
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }
    public Line(float startX, float startY, float endX, float endY){
        startPoint = new Point2D(startX, startY);
        endPoint = new Point2D(endX, endY);
    }
    public double getLenght(){
        return Math.sqrt(Math.pow(startPoint.getX()-endPoint.getX(),2)-Math.pow(startPoint.getY() - endPoint.getY(),2 ));
    }
    public Point2D getMiddlePoint() throws NoEnoughtData {

        if(startPoint==null || endPoint==null){
            throw new NoEnoughtData("Nejaky null object");
        }

        double x = Math.abs(startPoint.getX() - endPoint.getX()) / 2
                + Math.min(startPoint.getX() , endPoint.getX());

        double y = Math.abs(startPoint.getY() - endPoint.getY()) / 2
                + Math.min(startPoint.getY() , endPoint.getY());
        return new Point2D((float) x, (float) y);
    }
}

