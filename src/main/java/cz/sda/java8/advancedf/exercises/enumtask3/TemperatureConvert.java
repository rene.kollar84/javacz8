package cz.sda.java8.advancedf.exercises.enumtask3;

public enum TemperatureConvert {

    C_K("C", "K", new CelsiusToKelvinConverter()),//konkretni implementace interface Convertor
    K_C("K", "C", new Converter() {//anonymni trida implementujici interface Convertor
        @Override
        public double convert(double tempIn) {
            return tempIn-273.15;
        }
    }),
    K_F("K","F",tempIn -> 1.8*(tempIn - 273) + 32);//lambda vyraz reprezentujici Convertor


    private String inputUnit;
    private String outputUnit;
    private Converter converter;

    TemperatureConvert(String inputUnit, String outputUnit, Converter converter) {
        this.inputUnit = inputUnit;
        this.outputUnit = outputUnit;
        this.converter = converter;
    }

    public static double convertTemperature(String inputUnit, String outputUnit, double value) {
        //kazdy enum ma statickou metodu valueOf, ktera vrati enum podle vstupniho retezce
        TemperatureConvert temperatureConvert = TemperatureConvert.valueOf(inputUnit + "_" + outputUnit);
        return temperatureConvert.converter.convert(value);
    }

    interface Converter {
        double convert(double tempIn);
    }

    static class CelsiusToKelvinConverter implements Converter{

        @Override
        public double convert(double tempIn) {
            return tempIn+273.15;
        }
    }
}
