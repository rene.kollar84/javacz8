package cz.sda.java8.advancedf.exercises.threads.task1;

public class Main {
    public static void main(String[] args) {
        Thread t1 = new Thread() {
            @Override
            public void run() {
                //1000-
                //2000
                for (int i = 1000; i <= 2000; i++) {
                    if (i % 2 == 0) {
                        System.out.println(i);
                    }
                }
            }
        };
        t1.start();
        Runnable r=new Runnable() {//vytvoreni anonymni tridy (beze jmena) ktera implementuje rozhrani Runnable
            @Override
            public void run() {
                //14300-17800
                for (int i = 14300; i <= 17800; i++) {
                    if (i % 2 == 0) {
                        System.out.println(i);
                    }
                }
            }
        };
        Thread thread = new Thread(r);
        thread.start();
//dalsi zpusob
        Runnable rr= () -> {
            //14300-17800
            for (int i = 14300; i <= 17800; i++) {
                if (i % 2 == 0) {
                    System.out.println(i);
                }
            }
        };
        Thread thread3 = new Thread(rr);
        thread3.start();
    }
}
