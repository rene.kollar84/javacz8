package cz.sda.java8.advancedf.exercises.generic.task5;

public class Main {
    public static void main(String[] args) {
        PetHouse<Animal> animalPetHouse = new PetHouse<>();
        animalPetHouse.addAnimal(new Cat());
        animalPetHouse.addAnimal(new Dog());
    }
}
