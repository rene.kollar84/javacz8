package cz.sda.java8.advancedf.exercises.io;

import java.io.*;
import java.util.Scanner;

public class IoExample {
    public static void main(String[] args) throws IOException {
        Scanner in=new Scanner(System.in);
       // Scanner scanner = new Scanner(new FileInputStream("a.txt"));

        BufferedInputStream bi=new BufferedInputStream(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
//      while (true){
//          int read = System.in.read();
//          System.out.print((char) read);
//      }
    }
}
