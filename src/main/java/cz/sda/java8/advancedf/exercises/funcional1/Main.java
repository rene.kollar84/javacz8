package cz.sda.java8.advancedf.exercises.funcional1;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        Video video = new Video("GOT1", "got1.com",
                VideoType.CLIP);
        Video video1 = new Video("GOT2", "got2.com",
                VideoType.EPISODE);
        Video video2 = new Video("GOT3", "got3.com",
                VideoType.PREVIEW);
        Video video3 = new Video("GOT4", "got4.com",
                VideoType.PREVIEW);
        Video video4 = new Video("GOT5", "got5.com",
                VideoType.CLIP);
        Video video5 = new Video("GOT6", "got6.com",
                VideoType.EPISODE);

        Episode episode = new Episode("got1", 1,
                Arrays.asList(video, video1));
        Episode episode1 = new Episode("got2", 2,
                Arrays.asList(video2, video3));
        Episode episode2 = new Episode("got3", 1,
                Arrays.asList(video4, video5));

        Season season = new Season("GOTS1", 1,
                Arrays.asList(episode, episode1));
        Season season1 = new Season("GOTS1", 2,
                Arrays.asList(episode2));


        List<Season> seasons = Arrays.asList(season, season1);

//list of episodes
        List<Episode> episodes = seasons.stream()
                .flatMap(s -> season.episodes.stream())
                .collect(Collectors.toList());
        System.out.println(episodes);

        //list of alll videos
        printAllVideos(seasons);

        //a list of all season numbers
        printAllSeasonNumbers(seasons);

//        a list of all episode names
        List<String> collect = seasons.stream()
                .flatMap(season2 -> season2.episodes.stream())
                .map(episode3 -> episode3.episodeName).collect(Collectors.toList());
        System.out.println(collect);

//        a list of all episode numbers
        seasons.stream()
                .flatMap(season2 -> season2.episodes.stream())
                .map(episode3 -> episode3.episodeNumber).collect(Collectors.toList());

//        a list of all video names
        Stream<String> allVideoNames = seasons.stream()
                .flatMap(season2 -> season2.episodes.stream())
                .flatMap(episode3 -> episode3.videos.stream()).map(v -> v.title);

//        a list of all url addresses for each video
        Stream<String> allVideoAddress = seasons.stream()
                .flatMap(season2 -> season2.episodes.stream())
                .flatMap(ee -> ee.videos.stream()).map(v -> v.url);

//        only episodes from even seasons
        List<Episode> episFromEvenSeas = seasons.stream()
                .filter(s -> s.seasonNumber % 2 == 0)
                .flatMap(s -> s.episodes.stream())
                .collect(Collectors.toList());

//        only videos from even seasons
        seasons.stream().filter(s -> s.seasonNumber % 2 == 0).flatMap(s -> s.episodes.stream())
                .flatMap(e -> e.videos.stream())
                .collect(Collectors.toList());

//        only videos from even episodes and seasons
        List<Video> collect1 = seasons.stream().filter(s -> s.seasonNumber % 2 == 0)
                .flatMap(s -> s.episodes.stream())
                .filter(e -> e.episodeNumber % 2 == 0)
                .flatMap(e -> e.videos.stream())
                .collect(Collectors.toList());

//        only Clip videos from even episodes and odd seasons
        Stream<Video> videoStream = seasons.stream().filter(s -> s.seasonNumber % 2 != 0)
                .flatMap(s -> s.episodes.stream())
                .filter(e -> e.episodeNumber % 2 == 0)
                .flatMap(e -> e.videos.stream())
                .filter(v -> v.videoType == VideoType.CLIP);
        
//        only Preview videos from odd episodes and even seasons
        seasons.stream()
                .filter(s -> s.seasonNumber % 2 == 0)
                .flatMap(s -> s.episodes.stream())
                .filter(e -> e.episodeNumber % 2 != 0)
                .flatMap(e -> e.videos.stream())
                .filter(v -> v.videoType == VideoType.PREVIEW).collect(Collectors.toList());
    }

    private static void printAllSeasonNumbers(List<Season> seasons) {
        List<Integer> collect = seasons.stream()
                .map(season -> season.seasonNumber)
                .collect(Collectors.toList());
        System.out.println(collect);
    }

    private static void printAllVideos(List<Season> seasons) {
        List<Video> collect = seasons.stream().flatMap(season -> season.episodes.stream())
                .flatMap(episode -> episode.videos.stream())
                .collect(Collectors.toList());
        System.out.println(collect);
    }
}
