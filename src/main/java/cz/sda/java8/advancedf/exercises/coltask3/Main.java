package cz.sda.java8.advancedf.exercises.coltask3;

import java.util.*;

/*
Based on 100 element array with randomly selected elements from the range
0-50 please implement following features:
returning a list of unique elements
returning a list of elements that have been repeated in the generated array at
least once
 */
public class Main {
    public static void main(String[] args) {
        Random r = new Random();
        int[] pole = new int[100];
        for (int i = 0; i < pole.length; i++) {
            pole[i] = r.nextInt(51);
        }

        List<Integer> unique = getUnique(pole);
        List<Integer> noUnique = getNoUnique(pole);
        System.out.println(unique.size()+" "+noUnique.size());
    }

    public static List<Integer> getUnique(int[] pole) {
        int[] p;
        //List<int>
       // Integer ii=1;
        //int iii=ii;
        Set<Integer> intSet = new HashSet<>();
        for (int value : pole) {
            intSet.add(value);
        }
        return new ArrayList<>(intSet);
    }

    public static List<Integer> getNoUnique(int[] pole) {
        Set<Integer> intSet = new HashSet<>();
        ArrayList<Integer> ret = new ArrayList<>();
        for (int value : pole) {
            boolean add = intSet.add(value);
            if (add == false) {
                ret.add(value);
            }
        }
        return ret;
    }

}
