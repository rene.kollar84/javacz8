package cz.sda.java8.advancedf.exercises.interface_example;

public class Obleceni implements ColorInfo,WeightInfo{
    @Override
    public String getColor() {
        return "black";
    }

    @Override
    public int getWeight() {
        return 3;
    }
}
