package cz.sda.java8.advancedf.exercises.interface_example;

public class Main {
    public static void main(String[] args) {
        Auto a = new Auto();
        Obleceni o = new Obleceni();
        Ovoce ov = new Ovoce();

        ColorInfo[] ci = new ColorInfo[]{a, o, ov};
        WeightInfo[] wi = new WeightInfo[]{a, o, ov};

        for (ColorInfo c : ci) {
            System.out.println(c.getColor());
            if(c instanceof WeightInfo){
                WeightInfo c1 = (WeightInfo) c;
                System.out.println(c1.getWeight());
            }
        }

        for (WeightInfo c : wi) {
            System.out.println(c.getWeight());

        }
    }
}
