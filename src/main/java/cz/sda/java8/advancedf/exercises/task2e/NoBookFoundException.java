package cz.sda.java8.advancedf.exercises.task2e;

public class NoBookFoundException extends RuntimeException {
    public NoBookFoundException(String message) {
        super(message);
    }
}
