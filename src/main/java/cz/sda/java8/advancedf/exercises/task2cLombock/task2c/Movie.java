package cz.sda.java8.advancedf.exercises.task2cLombock.task2c;

import lombok.*;

/*
Trida ukazuje pouziti lombock oproti Movie z baliku task2c
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"title","genre"})
@Builder
public class Movie {
    private String title;
    private String director;
    private int yearOfRelease;
    private String genre;
    private String distributor;


}
