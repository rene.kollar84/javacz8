package cz.sda.java8.advancedf.exercises.funcional1;

import java.util.List;

class Season {
    public String seasonName;
    public int seasonNumber;
    List<Episode> episodes;
    public Season(String seasonName, int seasonNumber,
                  List<Episode> episodes) {
        this.seasonName = seasonName;
        this.seasonNumber = seasonNumber;
        this.episodes = episodes;
    }
}