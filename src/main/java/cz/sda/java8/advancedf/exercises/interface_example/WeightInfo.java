package cz.sda.java8.advancedf.exercises.interface_example;

public interface WeightInfo {
    int getWeight();
}
