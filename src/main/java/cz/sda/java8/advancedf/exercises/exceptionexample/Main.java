package cz.sda.java8.advancedf.exercises.exceptionexample;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        {
            int a;
        }

        {
            int a;
        }
        int a;
        Scanner scanner = new Scanner(System.in);
        String s=null;
        String s2=null;
        do {
            try {
                s = scanner.nextLine();
                s2 = scanner.nextLine();

                int i = Integer.parseInt(s);
                int i1 = Integer.parseInt(s2);
                System.out.println(i / i1);
            } catch (ArithmeticException e) {
                System.out.println("Div by 0");
            }catch (NumberFormatException e){

            }
            finally {
                if ("exit".equalsIgnoreCase(s) || "exit".equalsIgnoreCase(s2)) {
                    break;
                }
            }
        } while (true);
    }
}
