package cz.sda.java8.advancedf.exercises.task1composie;

public class Point2D {


    public static void main(String[] args) {
        Point2D point=new Point2D(2.5f,3.7f);
        System.out.println(point.toString());
    }
    private float x;
    private float y;

    public Point2D() {
        x = 0;
        y = 0;
    }

    public Point2D(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float[] getXY(){
        return new float[]{x,y};
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setXY(float x, float y){
        this.x = x;
        this.y = y;
    }

     @Override
    public String toString() {
        return "("+x+","+y+")" ;
    }

}
//non-arguments constructor which will set x , y 􀂦elds to 0
//two-arguments constructor: float x , float y
//getter methods which will be responsible for returning x , y 􀂦elds values
//getXY method which will return x , y values as two-element array
//setter methods which will be responsible for setting x , y 􀂦elds values
//setXY method which will be responsible for settting x and y
//toString method which should return string in the following format: (x,
//y)