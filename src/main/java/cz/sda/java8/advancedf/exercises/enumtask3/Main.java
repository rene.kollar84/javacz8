package cz.sda.java8.advancedf.exercises.enumtask3;

public class Main {
    public static void main(String[] args) {
        double v = TemperatureConvert.convertTemperature("C", "K", 0);
        double v1 = TemperatureConvert.convertTemperature("K", "C", v);
        System.out.println(v+ " "+v1);
    }
}
