package cz.sda.java8.advancedf.exercises.task3c;

import lombok.NonNull;
import lombok.ToString;

@ToString
public class Car {
    private int yearCreation;
    private String name;
    private Engine engine;
    private CarType type;

    public void setYearCreation(@NonNull Integer yearCreation) {
        this.yearCreation = yearCreation;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(CarType type) {
        this.type = type;
        engine = switch (type) {
            case OTHER -> new Engine("petrol");
            case LUXURY -> new Engine("electric");
            case ECONOMIC -> new Engine("diesel");
        };
    }

    @ToString
    public class Engine {
        private String type;//electric|disel|petrol

        public Engine(String type) {
            this.type = type;
        }
    }

    public enum CarType {
        ECONOMIC,
        LUXURY,
        OTHER
    }
}
