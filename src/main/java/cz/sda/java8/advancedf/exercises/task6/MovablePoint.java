package cz.sda.java8.advancedf.exercises.task6;

public class MovablePoint implements Movable {
    private int x;
    private int y;
    private int xSpeed;
    private int ySpeed;

    public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    public MovablePoint(int x, int y) {
        this.x = x;
        this.y = y;
        this.xSpeed = 1;
        this.ySpeed = 1;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public void moveUp() throws OutOfScreenException {
        if (y - ySpeed < 0){
            throw new OutOfScreenException("Objekt by byl mimo obrazovku osy y.");
        }
        y -= ySpeed;

    }

    @Override
    public void moveDown() {
        y += ySpeed;

    }

    @Override
    public void moveLeft() {
        x -= xSpeed;

    }

    @Override
    public void moveRight() throws OutOfScreenException {
        if (x + xSpeed > MAX_X){
            throw new OutOfScreenException("Objekt by byl mimo obrazovku osy x.");
        }
        x += xSpeed;

    }

    @Override
    public String getActualPosition() {
        return String.format("Point x = %s, y = %s", x, y);
    }
}
