package cz.sda.java8.advancedf.exercises.task6;

public interface Movable {
    void moveUp() throws OutOfScreenException;

    void moveDown();

    void moveLeft();

    void moveRight() throws OutOfScreenException;

    String getActualPosition();

    int MAX_X = 1024;
    int MAX_Y = 768;

}

