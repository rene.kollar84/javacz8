package cz.sda.java8.advancedf.exercises.funcional1;

enum VideoType {
    CLIP, PREVIEW, EPISODE
}