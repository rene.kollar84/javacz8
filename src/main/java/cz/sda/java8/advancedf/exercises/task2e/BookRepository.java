package cz.sda.java8.advancedf.exercises.task2e;

public class BookRepository {
    private Book[] books = new Book[10];
    private int numberOfBooks = 0;

    public void addBook(Book book) {
        books[numberOfBooks] = book;
        numberOfBooks++;

    }

    public void removeBook(Book book) {
        boolean removed = false;
        for (int i = 0; i < books.length; i++) {
            if (book.equals(books[i])) {
                books[i] = null;
                numberOfBooks--;
                removed = true;
            }

        }
        if (!removed) {
            throw new NoBookFoundException("Kniha nenalezena!" + book);
        }


    }

    public Book findByName(String name) {
        for (int i = 0; i < books.length; i++) {
            if (books[i] != null && name.equals(books[i].getTitle())) {
                return books[i];
            }

        }
        throw new NoBookFoundException("Kniha nenalezena!" + name);
    }
}
