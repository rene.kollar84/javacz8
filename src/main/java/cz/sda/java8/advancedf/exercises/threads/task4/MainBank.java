package cz.sda.java8.advancedf.exercises.threads.task4;

import java.util.Random;
import java.util.Scanner;

public class MainBank {
    //    Write a program which will synchronize access to a bank account. If any cyclical
//    Internet service wants to charge the account with a higher amount than currently
//    available, then the thread should be suspended. When additional money will be
//    transfered to the account, the thread should be raised.
    public static void main(String[] args) {
        BankAccount ucet = new BankAccount(3000);


        Thread nakupyNaInternetu = new Thread(() ->
        {
            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("Nakup c." + i);
                ucet.withdraw(generateRandom());
            }
            System.out.println("Nakupovani konci");
        });
        nakupyNaInternetu.setName("Nakupujici manzelka");
        Thread dobitiUctu = new Thread(() -> {
            while (true) {
                ucet.waitForNeedMoney();

            }
        });
        nakupyNaInternetu.start();
        dobitiUctu.setDaemon(true);
        dobitiUctu.setName("Manzel");
        dobitiUctu.start();
    }

    private static int generateRandom() {
        Random r = new Random();
        return (int) (r.nextFloat() * 2000);
    }
}

class BankAccount {
    double amount;

    volatile boolean needMoney = false;

    public BankAccount(double i) {
        amount = i;
    }

    synchronized void addMoney(double how) {
        amount += how;
        System.out.println("Vlozeno " + how);
        notify();
    }


    synchronized void withdraw(double how) {
        System.out.println("Chci nakoupit za " + how);
        while (how > amount) {

            try {
                notify();
                wait();

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        amount -= how;
        System.out.println("Nakoupeno za " + how + " Zustatek:" + amount);
    }

    synchronized void waitForNeedMoney() {

        try {
            wait();
        } catch (InterruptedException e) {

        }
        System.out.println("Kolik nabit");
        Scanner s = new Scanner(System.in);
        int how = s.nextInt();
        amount += how;
        notify();
        System.out.println("********************************************************nabiti karty*******************");
    }
}