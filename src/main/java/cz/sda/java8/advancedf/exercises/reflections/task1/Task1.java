package cz.sda.java8.advancedf.exercises.reflections.task1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public  class Task1 {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        Student student = new Student();
        System.out.println("Methods: ");

        Class<? extends Student> clazz = student.getClass();

        Method[] methods =
                clazz.getDeclaredMethods();

        System.out.println(Arrays.toString(methods));
        System.out.println("Fields: ");

        Field[] fields = student.getClass().getDeclaredFields();
        System.out.println(Arrays.toString(fields));
        System.out.println("Constructors: ");

        Constructor[] constructors =
                student.getClass().getConstructors();
        System.out.println(Arrays.asList(constructors));

        student.setName("karel");

        Method getName = clazz.getMethod("getName");
        Object name = getName.invoke(student);

//        Field name1 = clazz.getField("name");
//        name1.setAccessible(true);
//        Object o = name1.get(student);

        System.out.println(name+" ");
        MyAnotation annotation = clazz.getAnnotation(MyAnotation.class);
        String verze = annotation.verze();
        System.out.println(verze);
    }
}