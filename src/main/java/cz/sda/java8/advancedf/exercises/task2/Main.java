package cz.sda.java8.advancedf.exercises.task2;

public class Main {
    public static void main(String[] args) {
        Person[] all = new Person[]{
                new Lecture("Karolina", "Kolin",Lecture.Specialization.LANGUAGE, 42053),
                new Student(){
                    {
                        setTypeOfStudy(TypeOfStudy.PRESENT);
                        setYearOfStudy(10);
                        setAddress("Orlova");
                    }
                    @Override
                    public String getName() {
                        return "Jane Doe";
                    }
                },
                new Student(){
                    {
                        setAddress("Praha");
                        setName("Novak");
                    }

                },
//                new Person("Josef", "Ostrava") - nelze vytvorit protoze nechci a dala jsem Person abstract
        };

        //pretypovani
        ((Student)all[2]).setTypeOfStudy(Student.TypeOfStudy.PRESENT);
        for (Person human : all) {

            System.out.println(human.getType());
            System.out.println(human.getName());
            System.out.println(human.getAddress());
        }


    }
}
