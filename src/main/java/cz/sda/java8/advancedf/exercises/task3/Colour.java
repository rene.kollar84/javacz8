package cz.sda.java8.advancedf.exercises.task3;

public enum Colour {
    WHITE, BLACK, RED, BLUE, UNKNOWN
}
