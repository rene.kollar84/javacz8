package cz.sda.java8.advancedf.exercises.threads.task2;

public class Main {
    public static void main(String[] args) {
        Car c1=new Car("toyota");
        Car volvo = new Car("Volvo");

        Bridge bridge = new Bridge();

        Thread thread = new Thread(() -> bridge.driveThrough(c1));
        Thread thread1 = new Thread(() -> bridge.driveThrough(volvo));

        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        thread.setPriority(Thread.MIN_PRIORITY);
        thread1.setPriority(Thread.MAX_PRIORITY);
        thread.start();
        thread1.start();
    }
}
