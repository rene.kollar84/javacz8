package cz.sda.java8.advancedf.exercises.coltask2and4;

import java.util.ArrayList;
import java.util.List;

public class BookService {

    private List<Book> books = new ArrayList<>();

    public void addBook(Book newOne) {
        books.add(newOne);
    }

    public void removeBook(Book book) {
        books.remove(book);
    }

    public List<Book> getAll() {
        return books;
    }

    public List<Book> getAllOfGenre(Book.GenreEnum genre) {
        List<Book> selected = new ArrayList<>();
        for (Book b : books) {
            if (b.getGenre().equals(genre)) {
                selected.add(b);
            }
        }
        return selected;
    }

    public List<Book> getAllBefore(int year) {
        List<Book> selected = new ArrayList<>();
        for (Book b : books) {
            if (b.getYearOfRelease() < year) {
                selected.add(b);
            }
        }
        return selected;
    }

    public Book findByPrice(boolean max) { //true = highest price, false = lowest price
        double extreme = max ? 0:Double.MAX_VALUE;
        Book ret = null;
        for (Book b : books) {
            if (max){
                if (b.getPrice()>extreme){
                    ret = b;
                    extreme = b.getPrice();
                }
            } else {
                if (b.getPrice()<extreme){
                    ret = b;
                    extreme = b.getPrice();
                }
            }
        } return ret;
    }

}
