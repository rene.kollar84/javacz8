package cz.sda.java8.advancedf.exercises.task6;

public class MainMovable {
    public static void main(String[] args) {
        Movable[] movables = loadFromFile();
        for (Movable m : movables) {
            System.out.println("Before move " + m.getActualPosition());
            try {
                m.moveRight();
            } catch (OutOfScreenException e) {
                System.out.println(e.getMessage());
            }
            System.out.println("After move " + m.getActualPosition());
        }
    }

    public static Movable[] loadFromFile() {
        return new Movable[]{new MovablePoint(10, 5), new MovableCircle(15,20,30), new MovablePoint(1024,800)};

    }
}
