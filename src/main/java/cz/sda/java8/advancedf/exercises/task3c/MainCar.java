package cz.sda.java8.advancedf.exercises.task3c;

public class MainCar {
    public static void main(String[] args) {
        Car car1 = new Car();
        car1.setName("toyota");
        car1.setType(Car.CarType.LUXURY);
        car1.setYearCreation(2020);
        System.out.println(car1);
    }
}
