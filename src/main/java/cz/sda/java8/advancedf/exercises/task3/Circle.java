package cz.sda.java8.advancedf.exercises.task3;

public class Circle extends Shape {
    /*
    dedim z abstraktni tridy, kompilator me donuti impolementovat abstraktni metody
     */
    private double radius;

    public Circle() {
        super(Colour.UNKNOWN, false);
        radius = 1;

    }

    public Circle(Colour colour, boolean shouldFill, double radius) {
        super(colour, shouldFill);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


    //implementace abstraktnich metod z predka
    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimetr() {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return String.format("Circle " +
                "with radius %s which is a subclass off %s ",radius,super.toString());
    }
}
