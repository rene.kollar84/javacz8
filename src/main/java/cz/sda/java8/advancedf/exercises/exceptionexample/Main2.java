package cz.sda.java8.advancedf.exercises.exceptionexample;

public class Main2 {
    public static void main(String[] args) {
        int a = a();
        System.out.println(a);
    }

    public static int a(){
        int i=0;
        try{
          return i;
        }finally {
            i++;
           System.out.println("Method a return "+i);
        }
    }
}
