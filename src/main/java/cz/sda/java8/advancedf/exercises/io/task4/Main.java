package cz.sda.java8.advancedf.exercises.io.task4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
      //  Prepare a solution which will return the longest word from the provided  le.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Zadej nazev");
        Path path = Path.of(scanner.nextLine());
        List<String> lines = Files.readAllLines(path);
        String s1 = lines.stream()
                .flatMap(l -> Arrays.stream(l.split(" ")))
                .max((s, s2) -> s.length() - s2.length()).get();
        System.out.println(s1);
    }
}
