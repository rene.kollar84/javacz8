package cz.sda.java8.advancedf.exercises.generic.task5;

import java.util.ArrayList;
import java.util.List;

public class PetHouse <T extends Animal>{

    List<T> animals = new ArrayList<>();

    public void addAnimal(T animal){
        animals.add(animal);
    }
}
