package cz.sda.java8.advancedf.exercises.io.task2and3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Scanner;

public class Main {
    //    Prepare a solution which will read and display a line by line.
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Zadej nazev");
        String soubor = scanner.nextLine();
        List<String> strings = Files.readAllLines(Path.of(soubor));
        strings.forEach(System.out::println);
    }
}
