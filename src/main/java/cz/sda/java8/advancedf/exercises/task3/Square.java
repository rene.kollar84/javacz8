package cz.sda.java8.advancedf.exercises.task3;


public class Square extends Rectangle {
    /*
    predkem neni primym Shape ale obdelnik, protoze ctverec je specialnim typem obdelniku
    specialitou je ze ctverec ma vsechny strany stejne, takze prekryjeme metody pro nastaveni delky
    a sirky
     */
    @Override
    public void setLenght(double lenght) {
        super.setLenght(lenght);
        super.setWidth(lenght);
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
        super.setLenght(width);
    }

    @Override
    public String toString() {
        return super.toString().replace("Rectangle","Squere")
                .replace("Shape","Rectangle");
    }

    public static Square getFromArea(double area){
        double a = Math.sqrt(area);
        Square square = new Square();
        square.setWidth(a);
        return square;
    }
}
