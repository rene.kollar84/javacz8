package cz.sda.java8.advancedf.exercises.interface_example;

public class Auto implements ColorInfo,WeightInfo{
    @Override
    public String getColor() {
        return "red";
    }

    @Override
    public int getWeight() {
        return 100;
    }
}
