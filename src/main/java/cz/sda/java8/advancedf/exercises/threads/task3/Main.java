package cz.sda.java8.advancedf.exercises.threads.task3;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class Main {
    //    Write a program which will execute two independent sorting algorithms on two
//    separate threads. The main goal of the implementation is to return information
//    about the algorithm that has completed faster.
    public static void main(String[] args) {
        Main main = new Main();
        int[] random = main.createRandom(10000000);
        int[] random1 = main.createRandom(10000000);

        Thread thread = new Thread(() -> main.sort(random));
        thread.setName("Vlakno");
        Thread thread1 = new Thread(() -> main.sort(random1));
        thread1.setName("Vlakno1");

        thread1.setPriority(Thread.MIN_PRIORITY);
        thread.setPriority(Thread.MAX_PRIORITY);

        thread1.start();

        thread.start();
    }

    void sort(int[] pole){
        long start = System.currentTimeMillis();
        Arrays.sort(pole);
        long end = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName()+" Trideni trvalo "+(end-start)+"ms");
    }

    int[] createRandom(int limit){
        Random random = new Random();
        return IntStream.generate(()-> random.nextInt(1000)).limit(limit).toArray();
    }
}
