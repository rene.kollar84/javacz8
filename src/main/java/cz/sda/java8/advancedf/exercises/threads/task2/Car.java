package cz.sda.java8.advancedf.exercises.threads.task2;

public class Car {
    private String name;
    private String type;

    public Car(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
