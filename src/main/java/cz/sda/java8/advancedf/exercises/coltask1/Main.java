package cz.sda.java8.advancedf.exercises.coltask1;

public class Main {
    public static void main(String[] args) {
        SDAArrayList<String> stringSDAArrayList = new SDAArrayList<>();
        stringSDAArrayList.add("a");
        stringSDAArrayList.add("b");
        stringSDAArrayList.display();
        System.out.println(stringSDAArrayList.get(1));
        stringSDAArrayList.remove("b");
        stringSDAArrayList.display();

        SDAArrayList<Integer> integers=new SDAArrayList<>();
        for(int i=0;i<20;i++){
            integers.add(i);
        }
        integers.display();
    }
}
