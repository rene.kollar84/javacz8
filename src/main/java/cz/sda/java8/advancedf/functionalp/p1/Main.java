package cz.sda.java8.advancedf.functionalp.p1;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();

        integers.forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer pomeranc) {
                System.out.println(pomeranc);
            }
        });

        //stejny zapis ale kratsi
        integers.forEach(pomeranc -> System.out.println(pomeranc));

        //stejny zapis ale kratsi 2.
        integers.forEach(integer -> {
            System.out.println(integer);
        });

        Consumer<Integer> ref = System.out::println;

        ref.accept(1);

        //stejny zapis s methosREference
        //stejny zapis ale kratsi
        integers.forEach(System.out::println);



    }
}
