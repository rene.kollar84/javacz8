package cz.sda.java8.advancedf.annonymousc;

public class Main {
    public static void main(String[] args) {

        Thread thread = new Thread() {
            @Override
            public void run() {
                System.out.println("Ahoj");
            }
        };
        B b = new B();
        B b1 = new B(1);
        thread.start();
    }

    static class B {
        B(int i) {

        }

        public B() {
        }
    }
}
