package cz.sda.java8.advancedf.innerc;

public class Body {
    private double weight;
    private double height;

    Leg left;
    Leg right;

    Arm leftArm;
    Arm rightArm;

    CardioStim stimulator = null;

    public Body(double weight, double height) {
        this.weight = weight;
        this.height = height;

        left = new Leg();
        right = new Leg();
        leftArm = new Arm();
        rightArm = new Arm();

    }

    public CardioStim getStimulator() {
        return stimulator;
    }

    public void setStimulator(CardioStim stimulator) {
        this.stimulator = stimulator;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Leg getLeft() {
        return left;
    }

    public void setLeft(Leg left) {
        this.left = left;
    }

    public Leg getRight() {
        return right;
    }

    public void setRight(Leg right) {
        this.right = right;
    }

    public Arm getLeftArm() {
        return leftArm;
    }

    public void setLeftArm(Arm leftArm) {
        this.leftArm = leftArm;
    }

    public Arm getRightArm() {
        return rightArm;
    }

    public void setRightArm(Arm rightArm) {
        this.rightArm = rightArm;
    }

    public class Leg {
        double lenght;
        double sizeFoot;

        public void prinntBodyLength() {
            System.out.println(height);
        }
    }

    public class Arm {
        double lenght;
        Palm palm;
    }

    public class Palm {
        int fingerCount;
    }

    public static class CardioStim {
        //**********Inner classes **********
        public void print() {
            // no possible System.out.println(height);

            //Local class
            class Inner{
                int a;
                void increase(){
                    a++;
                }
            }

            Inner inner=new Inner();
            inner.increase();
            System.out.println(inner.a);
        }
    }
}
