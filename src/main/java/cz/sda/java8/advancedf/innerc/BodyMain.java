package cz.sda.java8.advancedf.innerc;

public class BodyMain {
    public static void main(String[] args) {
        Body me = new Body(80,180);
        Body you = new Body(30,100);

        Body.CardioStim cardioStim = orderStim();
        Body.Leg leg = me.new Leg();
        you.setLeft(leg);

        me.setStimulator(cardioStim);
    }

    public static Body.CardioStim orderStim(){
        return new Body.CardioStim();
    }
}
