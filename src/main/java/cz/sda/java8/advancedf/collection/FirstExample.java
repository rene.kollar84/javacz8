package cz.sda.java8.advancedf.collection;

import java.util.*;

public class FirstExample {
    public static void main(String[] args) {
        List<String> l1 = new ArrayList<>();
        l1.add("a");
        l1.add("b");
        l1.add("a");
        l1.forEach(prvek -> {
            System.out.println(prvek);
        });

        Set<String> s1=new HashSet<>();
        boolean add1 = s1.add("1");//true
        boolean add = s1.add("1");//false
        s1.forEach(prvek->{
            System.out.println(prvek);
        });

        Set<Animal> animals=new HashSet<>();
        animals.add(new Animal("had"));
        animals.add(new Animal("had"));
        animals.forEach(p-> System.out.println(p));

        String a="";
        Float f= 23F;//Float f = new Float(23F);Float.valueOf(23F);
        Map<String,Float> salaryes = new HashMap<>();

        salaryes.put("Ja",300F);
        salaryes.put("Ty",350F);

        Float jaValue = salaryes.get("Ja");

        for(String key:salaryes.keySet()){
            System.out.println(key);
        }
        Float ff= Float.valueOf(10F);
        float fp = ff;//float fp = ff.floatValue()
        for(float value:salaryes.values()){
            System.out.println(value);
        }

        salaryes.forEach((k,v)->{
            System.out.println(k+":"+v);
        });

    }

    static class Animal{
        String name;

        public Animal(String name) {
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Animal animal = (Animal) o;

            return Objects.equals(name, animal.name);
        }

        @Override
        public int hashCode() {
            return name != null ? name.hashCode() : 0;
        }

        @Override
        public String toString() {
            return "Animal{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

}
