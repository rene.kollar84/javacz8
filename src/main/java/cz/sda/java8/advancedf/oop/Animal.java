package cz.sda.java8.advancedf.oop;

public abstract class Animal {
    abstract String getName();

    abstract boolean eatMeat();

    abstract boolean eatVegetable();

    public void printName(){
        System.out.println(getName());
    }

    //type represent type of meal 1 is meat 2 s vegetable
    public void eat(int type){
       switch (type){
           case 1:
           case 2:
           {
               boolean canEatThisType = eatMeat() | eatVegetable();
               if(canEatThisType){
                   System.out.println("Yummy");
               }else{
                   System.out.println("Blee");
               }
           }

           default :System.out.println("Unknown meal type");break;
       }
    }


}
