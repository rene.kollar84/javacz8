package cz.sda.java8.advancedf.oop;

public interface Iterator {
    boolean hasNext();
    Object getNext();
}
