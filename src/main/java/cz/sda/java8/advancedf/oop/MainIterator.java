package cz.sda.java8.advancedf.oop;

public class MainIterator {

    public static void main(String[] args) {

    }

    class CarIterator implements Iterator {
        int curPos = 0;
        Car[] cars = new Car[]{
                new Truck("1"),
                new Truck("2"),
                new Truck("3")
        };

        @Override
        public boolean hasNext() {
            return curPos < cars.length;
        }

        @Override
        public Object getNext() {
            if (hasNext()) {
                return cars[++curPos];
            }
            return null;
        }
    }
}
