package cz.sda.java8.advancedf.oop;

public class Truck extends Car{
    public Truck(String vin) {
        super(vin);
    }

    @Override
    void runEngine() {

    }
}
